﻿namespace Recurse.MonoGame.Components
{
    //public class ZoomViewer : SingleItemContainer<Widget>
    //{
    //    private float _mapScale;

    //    private Vector2 _mapOffset;

    //    private Point _lastTouchPosition;

    //    public Widget? Widget
    //    {
    //        get => InternalChild;
    //        set => InternalChild = value;
    //    }

    //    public override void OnTouchDown()
    //    {
    //        base.OnTouchDown();

    //        _lastTouchPosition = Mouse.GetState().Position;
    //    }

    //    public override void OnTouchMoved()
    //    {
    //        base.OnTouchMoved();

    //        var touchPosition = Mouse.GetState().Position;
    //        var touchDelta = touchPosition - _lastTouchPosition;
    //        _lastTouchPosition = touchPosition;

    //        _mapOffset += touchDelta.ToVector2();
    //    }

    //    public override void OnMouseWheel(float delta)
    //    {
    //        base.OnMouseWheel(delta);

    //        var scrollSign = delta > 0 ? 1 : -1;
    //        var scrollAmount = Math.Ceiling(Math.Abs(delta / 300));
    //        var zoomFactor = Math.Pow(2, scrollSign * scrollAmount);
    //        var newScale = (float)Math.Max(0.125, Math.Min(_mapScale * zoomFactor, 2));
    //        //var actualZoomFactor = newScale / Scale;

    //        Zoom(Mouse.GetState().Position.ToVector2(), newScale);
    //    }

    //    public override void InternalArrange()
    //    {
    //        base.InternalArrange();

    //        if (Widget != null)
    //        {
    //            Widget.Left = (int)_mapOffset.X;
    //            Widget.Top = (int)_mapOffset.Y;
    //            Widget.HorizontalAlignment = HorizontalAlignment.Left;
    //            Widget.VerticalAlignment = VerticalAlignment.Top;
    //        }
    //    }

    //    public override void InternalRender(RenderContext context)
    //    {


    //        base.InternalRender(context);

    //        if (Widget != null)
    //        {
    //            var previousContextTransform = context.Transform;
    //            try
    //            {
    //                context.Transform =
    //                    = Matrix.CreateScale(_mapScale, _mapScale, 1)
    //                    * Matrix.CreateTranslation(_mapOffset.X, _mapOffset.Y, 0);
    //                MapRendering.DrawTiles(context);
    //            }
    //            finally
    //            {
    //                context.Transform = previousContextTransform;
    //            }
    //        }
    //    }

    //    private void OnMapRenderingChanged()
    //    {
    //        _mapScale = 1;

    //        SetMapOffset(default, default);
    //    }

    //    private void Zoom(Vector2 viewerPoint, float scale)
    //    {
    //        var mapPoint = GetMapPoint(viewerPoint);

    //        _mapScale = scale;

    //        SetMapOffset(viewerPoint, mapPoint);
    //    }

    //    private void SetMapOffset(Vector2 viewerPoint, Vector2 mapPoint)
    //    {
    //        var unadjustedViewerPoint = GetViewerPoint(mapPoint);

    //        _mapOffset += viewerPoint - unadjustedViewerPoint;
    //    }

    //    private Vector2 GetMapPoint(Vector2 viewerPoint)
    //    {
    //        return (viewerPoint - _mapOffset) / _mapScale;
    //    }

    //    private Vector2 GetViewerPoint(Vector2 mapPoint)
    //    {
    //        return mapPoint * _mapScale + _mapOffset;
    //    }
    //}
}
