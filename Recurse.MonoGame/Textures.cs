﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Recurse.Core.Collections.Planes;

namespace Recurse.MonoGame
{
    public static class Textures
    {
        public static Array2D<Color> GetPixels(Texture2D texture)
        {
            var result = new Array2D<Color>(texture.Width, texture.Height);
            texture.GetData(result.Elements);
            return result;
        }

        public static Array2D<Color> GetPixels(Texture2D texture, Rectangle bounds)
        {
            var result = new Array2D<Color>(bounds.Width, bounds.Height);
            texture.GetData(0, bounds, result.Elements, 0, bounds.Width * bounds.Height);
            return result;
        }

        public static void SetPixels(Texture2D texture, Array2D<Color> pixels)
        {
            texture.SetData(pixels.Elements);
        }

        public static void ReplacePixels(Texture2D texture, Color from, Color to)
        {
            var pixels = GetPixels(texture);
            for (var i = 0; i < pixels.Elements.Length; i++)
            {
                if (pixels.Elements[i] == from)
                {
                    pixels.Elements[i] = to;
                }
            }
            SetPixels(texture, pixels);
        }
    }
}
