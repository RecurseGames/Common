﻿using Microsoft.Xna.Framework;
using System;

namespace Recurse.MonoGame.Colors
{
    public struct HSV
    {
        public float Hue { get; }

        public float Saturation { get; }

        public float Value { get; }

        public HSV(float hue, float saturation, float value) : this()
        {
            if (hue < 0 || hue > 360)
            {
                throw new ArgumentOutOfRangeException(nameof(hue), "Hue must be between 0 and 360 (inclusive).");
            }
            if (value < 0 || value > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(saturation), "Saturation must be between 0 and 1 (inclusive).");
            }
            if (value < 0 || value > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "Value must be between 0 and 1 (inclusive).");
            }

            Hue = hue;
            Saturation = saturation;
            Value = value;
        }

        public static implicit operator Color(HSV hsv)
        {
            float H = hsv.Hue;
            while (H < 0) { H += 360; };
            while (H >= 360) { H -= 360; };
            float R, G, B;
            if (hsv.Value <= 0)
            { R = G = B = 0; }
            else if (hsv.Saturation <= 0)
            {
                R = G = B = hsv.Value;
            }
            else
            {
                float hf = H / 60.0f;
                int i = (int)Math.Floor(hf);
                float f = hf - i;
                float pv = hsv.Value * (1 - hsv.Saturation);
                float qv = hsv.Value * (1 - hsv.Saturation * f);
                float tv = hsv.Value * (1 - hsv.Saturation * (1 - f));
                switch (i)
                {

                    // Red is the dominant color

                    case 0:
                        R = hsv.Value;
                        G = tv;
                        B = pv;
                        break;

                    // Green is the dominant color

                    case 1:
                        R = qv;
                        G = hsv.Value;
                        B = pv;
                        break;
                    case 2:
                        R = pv;
                        G = hsv.Value;
                        B = tv;
                        break;

                    // Blue is the dominant color

                    case 3:
                        R = pv;
                        G = qv;
                        B = hsv.Value;
                        break;
                    case 4:
                        R = tv;
                        G = pv;
                        B = hsv.Value;
                        break;

                    // Red is the dominant color

                    case 5:
                        R = hsv.Value;
                        G = pv;
                        B = qv;
                        break;

                    // Just in case we overshoot on our math by a little, we put these here. Since its a switch it won't slow us down at all to put these here.

                    case 6:
                        R = hsv.Value;
                        G = tv;
                        B = pv;
                        break;
                    case -1:
                        R = hsv.Value;
                        G = pv;
                        B = qv;
                        break;

                    // The color is not defined, we should throw an error.

                    default:
                        //LFATAL("i Value error in Pixel conversion, Value is %d", i);
                        R = G = B = hsv.Value; // Just pretend its black/white
                        break;
                }
            }
            var r = Math.Clamp((int)(R * 255.0), 0, 255);
            var g = Math.Clamp((int)(G * 255.0), 0, 255);
            var b = Math.Clamp((int)(B * 255.0), 0, 255);

            return new Color(r, g, b);
        }
    }
}
