﻿using Microsoft.Xna.Framework;
using System;

namespace Recurse.MonoGame.Colors
{
    public struct LCH
    {
        public float Luma { get; }

        public float Chroma { get; }

        public float Hue { get; }

        public LCH(float luma, float chroma, float hue) : this()
        {
            if (luma < 0 || luma > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(luma), "Luma must be between 0 and 1 (inclusive).");
            }
            if (hue < 0 || hue > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(chroma), "Chroma must be between 0 and 1 (inclusive).");
            }
            if (hue < 0 || hue > 360)
            {
                throw new ArgumentOutOfRangeException(nameof(hue), "Hue must be between 0 and 360 (inclusive).");
            }

            Luma = luma;
            Chroma = chroma;
            Hue = hue;
        }

        public static implicit operator Color(LCH lch)
        {
            var h = lch.Hue % 360 / 60;
            var x = lch.Chroma * (1 - Math.Abs(h % 2 - 1));
            double r, g, b;
            if (0 <= h && h <= 1)
            {
                r = lch.Chroma;
                g = x;
                b = 0;
            }
            else if (h <= 2)
            {
                r = x;
                g = lch.Chroma;
                b = 0;
            }
            else if (h <= 3)
            {
                r = 0;
                g = lch.Chroma;
                b = x;
            }
            else if (h <= 4)
            {
                r = 0;
                g = x;
                b = lch.Chroma;
            }
            else if (h <= 5)
            {
                r = x;
                g = 0;
                b = lch.Chroma;
            }
            else
            {
                r = lch.Chroma;
                g = 0;
                b = x;
            }

            var m = lch.Luma - (.3 * r + .59 * g + .11 * b);

            return new Color(
                (int)(255 * (r + m)),
                (int)(255 * (g + m)),
                (int)(255 * (b + m)));
        }
    }
}
