﻿using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Recurse.MonoGame.Extensions
{
    public static class ContentManagerExtensions
    {
        public static IReadOnlyList<string> LoadFileNames(this ContentManager content, string contentFolder)
        {
            var directory = new DirectoryInfo(content.RootDirectory + "/" + contentFolder);
            if (!directory.Exists)
            {
                throw new DirectoryNotFoundException();
            }

            return directory.GetFiles("*.*").Select(file => Path.GetFileNameWithoutExtension(file.Name)).ToArray();
        }

        public static List<T> LoadContents<T>(this ContentManager content, string contentFolder)
        {
            var result = new List<T>();
            foreach (var fileName in content.LoadFileNames(contentFolder))
            {
                result.Add(content.Load<T>(contentFolder + "/" + fileName));
            }
            return result;
        }
    }
}
