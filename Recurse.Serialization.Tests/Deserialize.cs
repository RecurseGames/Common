﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Persist.V2.Defaults;

namespace Recurse.Persist.Test
{
    public static class Deserialize
    {
        public static T Execute<T>(T @object)
        {
            var model = DefaultSerializationModel.Instance;
            var writer = new SerializationContext(model);
            writer.GetOrCreateId(@object);
            while (writer.UninitializedEntries > 0)
            {
                writer.InitializeEntry();
            }

            var reader = new DeserializationContext(model);
            reader.AddEntries(writer.Entries);
            reader.GetOrCreateRoot<T>();
            while (reader.UninitializedEntries > 0)
            {
                reader.InitializeEntry();
            }

            return reader.GetOrCreateRoot<T>();
        }
    }
}
