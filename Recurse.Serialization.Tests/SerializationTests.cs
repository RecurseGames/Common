﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Recurse.Persist.Test
{
    public static class SerializationTests
    {
        [Test]
        public static void CannotSerializeActions()
        {
            Assert.Throws<InvalidOperationException>(() => Deserialize.Execute<Action>(() => 1.ToString()));
        }

        [Test]
        public static void CannotSerializeFunctions()
        {
            Assert.Throws<InvalidOperationException>(() => Deserialize.Execute<Func<int>>(() => 1));
        }

        [TestCase(null)]
        [TestCase(20)]
        public static void CanSerializeNullables(int? value)
        {
            var result = Deserialize.Execute(value);

            Assert.AreEqual(value, result);
        }

        [TestCase(0)]
        [TestCase(69)]
        [TestCase(1)]
        [TestCase(-1)]
        [TestCase(-2)]
        [TestCase(int.MaxValue)]
        [TestCase(int.MinValue)]
        public static void CanSerializeIntegers(int value)
        {
            Assert.AreEqual(value, Deserialize.Execute(value));
        }

        [Test]
        public static void CanSerializeEmptyArrays()
        {
            Assert.AreEqual(0, Deserialize.Execute(new int[0]).Length);
            Assert.AreEqual(0, Deserialize.Execute(new object[0]).Length);
        }

        [Test]
        public static void CanSerializeOneDimensionalArrays()
        {
            Assert.AreEqual(55, Deserialize.Execute(new[] { 44, 55 })[1]);
        }

        [Test]
        public static void CanSerializeArrayOfArrays()
        {
            Assert.AreEqual(44, Deserialize.Execute(new[] { new[] { 44 } })[0][0]);
        }

        [Test]
        public static void CanSerializeArrayOfObjects()
        {
            Assert.IsNotNull(Deserialize.Execute(new[] { new object() })[0]);
        }

        [Test]
        public static void CanSerializeIntArray()
        {
            var array = new[] { 1, 2 };

            var result = Deserialize.Execute(array);

            Assert.AreEqual(array, result);
        }

        [Test]
        public static void CanSerializeArrayOfCustomObjects()
        {
            Assert.IsNotNull(Deserialize.Execute(new[] { new CustomClass("Hey") })[0]);
        }

        [Test]
        public static void CanSerializeTwoDimensionalArrays()
        {
            var array = new int[,] { { 44, 55 }, { 44, 88 } };

            var result = Deserialize.Execute(array);

            Assert.AreEqual(array, result);
        }

        [Test]
        public static void CanSerializeJaggedArrays()
        {
            var array = new int[][] { new[] { 44, 55 }, new[] { 88 } };

            var result = Deserialize.Execute(array);

            Assert.AreEqual(array, result);
        }

        [Test]
        public static void CanSerializeObjects()
        {
            Assert.IsNotNull(Deserialize.Execute(new object()));
        }

        [Test]
        public static void CanSerializeNulls()
        {
            Assert.IsNull(Deserialize.Execute<object>(null));
        }

        [Test]
        public static void CanSerializeWeakReferences()
        {
            var o1 = new object();
            var r1 = new WeakReference(o1);
            var r2 = new WeakReference(new object());

            var result = Deserialize.Execute(new[] { o1, r1, r2 });

            GC.Collect();

            Assert.IsNotNull(((WeakReference)result[1]).Target);
            Assert.IsNull(((WeakReference)result[2]).Target);
        }

        [Test]
        public static void CanSerializeStringObjects()
        {
            String hello = "Hello";
            Assert.AreEqual("Hello", hello);
        }

        [Test]
        public static void CanSerializeStringValues()
        {
            string hello = "Hello";
            Assert.AreEqual("Hello", hello);
        }

        [Test]
        public static void CanSerializeLists()
        {
            var result = Deserialize.Execute(new List<int> { 0, 1, 2 });
            Assert.AreEqual(0, result[0]);
            Assert.AreEqual(1, result[1]);
            Assert.AreEqual(2, result[2]);

            var result2 = Deserialize.Execute(new List<int> { 1, 2, 3 });
            Assert.AreEqual(1, result2[0]);
            Assert.AreEqual(2, result2[1]);
            Assert.AreEqual(3, result2[2]);
        }

        [Test]
        public static void CanSerializeEmptyLists()
        {
            var result = Deserialize.Execute(new List<int>());
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public static void CanSerializeHashSets()
        {
            var value = new HashSet<object>
            {
                1,
                new CustomClass("p")
            };

            var result = Deserialize.Execute(value);

            Assert.True(result.Contains(1));
            Assert.AreEqual(1, result.OfType<CustomClass>().Count(e => e.ReadOnlyProperty == "p"));
        }

        [Test]
        public static void CanSerializeDictionaries()
        {
            var value = new Dictionary<int, object>
            {
                { 1, 5 },
                { 2, new CustomClass("p") }
            };

            var result = Deserialize.Execute(value);

            Assert.AreEqual(value[1], result[1]);
            Assert.AreEqual(((CustomClass)value[2]).ReadOnlyProperty, ((CustomClass)result[2]).ReadOnlyProperty);
        }

        [Test]
        public static void CanSerializeCustomClass()
        {
            var o = new CustomClass("Hi!") { String = "Hey" };
            var result = Deserialize.Execute(o);

            Assert.AreEqual(o.String, result.String);
            Assert.AreEqual(o.ReadOnlyProperty, result.ReadOnlyProperty);
            Assert.AreEqual(o.PrivateField, result.PrivateField);
        }

        [Test]
        public static void ClassAndBaseClassMembersAreSerialized()
        {
            var o = new CustomSubClass("Hi!", "Yay!");
            var result = Deserialize.Execute(o);

            Assert.AreEqual("Hi!", result.ReadOnlyProperty);
            Assert.AreEqual("Yay!", ((CustomClass)result).ReadOnlyProperty);
        }

        [Test]
        public static void RecurrentReferencesSerialized()
        {
            var a = new CustomClass("");
            var b = new CustomClass("");
            var c = new CustomClass("");
            a.ObjectProperty = b;
            b.ObjectProperty = c;
            c.ObjectProperty = a;
            
            var result = Deserialize.Execute(a);

            Assert.AreEqual(result, ((CustomClass)((CustomClass)result.ObjectProperty).ObjectProperty).ObjectProperty);
        }

        [Test]
        public static void ObjectsNotClonedByMultipleReferences()
        {
            var obj = new object();
            var refs = new[] { obj, obj };
            var result = Deserialize.Execute(refs);

            Assert.IsTrue(result[0] == result[1]);
        }

        [Test]
        public static void CanSerializeListsOfCustomObjects()
        {
            var result = Deserialize.Execute(new List<CustomClass> { new CustomClass("Um"), new CustomClass("Ah") });

            Assert.AreEqual("Um", result[0].ReadOnlyProperty);
            Assert.AreEqual("Ah", result[1].ReadOnlyProperty);
        }

        [Test]
        public static void DoesNotSerializeNotSerializedMembers()
        {
            var obj = new ClassWithNotSerializedValue { Value = 55 };
            var result = Deserialize.Execute(obj);

            Assert.AreEqual(0, result.Value);
            Assert.AreNotEqual(obj.Value, result.Value);
        }

        [Serializable]
        private class ClassWithNotSerializedValue
        {
            [NonSerialized]
            public int Value;
        }

        [Serializable]
        private class CustomClass
        {
            public int PrivateField { get; } = 2;

            public string String { get; set; }

            public string ReadOnlyProperty { get; }

            public object ObjectProperty { get; set; }

            public CustomClass(string readOnlyProperty)
            {
                ReadOnlyProperty = readOnlyProperty;
            }
        }

        [Serializable]
        private class CustomSubClass : CustomClass
        {
            public CustomSubClass(string readOnlyProperty, string baseReadOnlyProperty) : base(baseReadOnlyProperty)
            {
                ReadOnlyProperty = readOnlyProperty;
            }

            public new string ReadOnlyProperty { get; }
        }
    }
}
