﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Recurse.Persist.Test
{
    public static class ValueTypeSerializationTests
    {
        [Test]
        public static void CanSerializeNullableMembers()
        {
            var a = new ClassWithValue<int?>(null);
            var b = new ClassWithValue<int?>(999);

            var result = Deserialize.Execute(new[] { a, b });

            Assert.AreEqual(null, result[0].Value);
            Assert.AreEqual(999, result[1].Value);
        }

        [Test]
        public static void CanSerializeCustomEnums()
        {
            var a = new ClassWithValue<CustomEnum>(CustomEnum.Value1);
            var b = new ClassWithValue<CustomEnum>(CustomEnum.Value2);

            var result = Deserialize.Execute(new[] { a, b });

            Assert.AreEqual(CustomEnum.Value1, result[0].Value);
            Assert.AreEqual(CustomEnum.Value1, result[0].Value);
        }

        [Test]
        public static void CanSerializeCustomStructs()
        {
            var result = Deserialize.Execute(new CustomStruct(33));

            Assert.AreEqual(33, result.Value);
        }

        [Test]
        public static void CanSerializeListsOfCustomStructs()
        {
            var list = new List<CustomStruct> { new CustomStruct(33), new CustomStruct(77) };
            var result = Deserialize.Execute(list);

            Assert.AreEqual(33, result[0].Value);
            Assert.AreEqual(77, result[1].Value);
        }

        private struct CustomStruct
        {
            public int Value { get; }

            public CustomStruct(int value) : this()
            {
                Value = value;
            }
        }

        [Serializable]
        private class ClassWithValue<T>
        {
            public T Value { get; }

            public ClassWithValue(T value)
            {
                Value = value;
            }
        }

        private enum CustomEnum
        {
            Value1, Value2
        }
    }
}
