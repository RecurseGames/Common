﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using NUnit.Framework;

namespace Recurse.Persist.Test
{
    public static class SerializableActionTests
    {
        [Test]
        public static void CanSerializeDelegates()
        {
            var container = new DelegateContainer();

            var result = Deserialize.Execute(container);

            Assert.AreEqual(container.Delegate(0), result.Delegate(0));
        }

        [Test]
        public static void CannotSerializeAnonymousDelegates()
        {
            string anonymousAction() => typeof(InstanceClass).ToString();

            Assert.Throws<InvalidOperationException>(() => Deserialize.Execute<Func<string>>(anonymousAction));
        }

        [Test]
        public static void CanSerializeStaticClassSerializableDelegates()
        {
            Action<int> action = InstanceClass.StaticReceiveValue;
            var result = Deserialize.Execute(action);
            result.Invoke(1);

            Assert.AreEqual(1, InstanceClass.StaticReceivedValue);
        }

        [Test]
        public static void CanSerializeSerializableDelegates()
        {
            Action<int> action = new InstanceClass().ReceiveValue;
            var result = Deserialize.Execute(action);
            var resultTarget = (InstanceClass)result.Target;

            result.Invoke(1);

            Assert.AreEqual(1, resultTarget.ReceivedValue);
        }

        [Test]
        public static void CanSerializePrivateAction()
        {
            var action = new InstanceClass().CreatePrivateAction;
            var result = Deserialize.Execute(action);
            var resultTarget = (InstanceClass)result.Target;

            result.Invoke(1);

            Assert.AreEqual(1, resultTarget.ReceivedValue);
        }

        [Test]
        public static void CanSerializeExtensionMethod()
        {
            Func<int> action = new InstanceClass().CallExtensionMethod;
            var result = Deserialize.Execute(action);
            var resultTarget = (InstanceClass)result.Target;

            var resultResult = result.Invoke();

            Assert.AreEqual(resultTarget.CallExtensionMethod(), resultResult);
        }

        [Test]
        public static void CanSerializeStaticSerializableDelegates()
        {
            Action<int> action = StaticClass.StaticReceiveValue;
            var result = Deserialize.Execute(action);
            result.Invoke(1);

            Assert.AreEqual(1, StaticClass.ReceivedValue);
        }

        [Test]
        public static void CanSerializePrivateStaticAction()
        {
            var action = StaticClass.CreatePrivateAction;
            var result = Deserialize.Execute(action);
            result.Invoke(1);

            Assert.AreEqual(1, StaticClass.ReceivedValue);
        }

        private static class StaticClass
        {
            public static int FuncWithoutArguments() => 0;

            public static int FuncWithArguments(int x) => 0;

            public static int ReceivedValue { get; private set; }

            public static Action<int> CreatePrivateAction => PrivateReceiveValue;

            public static void StaticReceiveValue(int value)
            {
                ReceivedValue = value;
            }

            private static void PrivateReceiveValue(int value)
            {
                ReceivedValue = value;
            }
        }

        [Serializable]
        public class InstanceClass
        {
            public Action<int> CreatePrivateAction => PrivateReceiveValue;

            public int ReceivedValue { get; private set; }

            public static int StaticReceivedValue { get; private set; }

            public static void StaticReceiveValue(int value)
            {
                StaticReceivedValue = value;
            }

            public void ReceiveValue(int value)
            {
                ReceivedValue = value;
            }

            private void PrivateReceiveValue(int value)
            {
                ReceivedValue = value;
            }
        }

        [Serializable]
        private class DelegateContainer
        {
            public delegate int DelegateDefinition(int x);

            public DelegateDefinition Delegate { get; set; }

            public DelegateContainer()
            {
                Delegate = ExecuteDefinition;
            }

            public int ExecuteDefinition(int x) => x + 2;
        }
    }

    public static class InstanceClassExtensions
    {
        public static int CallExtensionMethod(this SerializableActionTests.InstanceClass instanceClass)
        {
            return 777;
        }
    }
}
