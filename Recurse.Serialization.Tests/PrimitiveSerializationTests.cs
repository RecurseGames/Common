﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;

namespace Recurse.Persist.Test
{
    public static class PrimitiveSerializationTests
    {
        [TestCase(1)]
        [TestCase(2L)]
        [TestCase(3U)]
        [TestCase(4UL)]
        [TestCase(5D)]
        [TestCase(6F)]
        [TestCase(7F)]
        [TestCase(true)]
        [TestCase("")]
        [TestCase("Zumtrel")]
        public static void CanSerializePrimitives(object o)
        {
            var result = Deserialize.Execute(o);

            Assert.AreEqual(o, result);
        }

        [Test]
        public static void CanSerializeIntArray()
        {
            var o = new[] { 1, 2, 3 };
            var result = Deserialize.Execute(o);

            Assert.AreEqual(o, result);
        }
    }
}
