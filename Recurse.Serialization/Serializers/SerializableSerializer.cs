﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Recurse.Persist.Serializers
{
    public class SerializableSerializer : ISerializer
    {
        private static readonly Dictionary<Type, ConstructorInfo> Constructors = new Dictionary<Type, ConstructorInfo>();

        private readonly Type _compatibleType;

        public SerializableSerializer(Type compatibleType)
        {
            if (!typeof(ISerializable).IsAssignableFrom(compatibleType))
                throw new ArgumentException($"Compatible type must implement {nameof(ISerializable)}.", nameof(compatibleType));

            _compatibleType = compatibleType;
        }

        public bool CanSerializeType(Type type)
        {
            return _compatibleType.IsGenericTypeDefinition
                ? type.IsConstructedGenericType && _compatibleType.IsAssignableFrom(type.GetGenericTypeDefinition())
                : _compatibleType.IsAssignableFrom(type);
        }

        public string CreateData(ISerializationContext context, object obj)
        {
            var serializable = obj as ISerializable;
            var info = new SerializationInfo(obj.GetType(), new FormatterConverter());
            var streamingContext = new StreamingContext();
            serializable.GetObjectData(info, streamingContext);

            var data = new EncodedStringArray();
            data.Elements.Add(context.Encode(typeof(Type), obj.GetType()));
            foreach (var entry in info)
            {
                var entryData = EncodedStringArray.Encode(
                    entry.Name,
                    context.Encode(typeof(Type), entry.ObjectType),
                    context.Encode(entry.ObjectType, entry.Value));
                data.Elements.Add(entryData);
            }

            return data.EncodedElements;
        }

        public object CreateObject(IDeserializationContext context, string data)
        {
            var delimitedData = EncodedStringArray.Decode(data);
            var type = (Type)context.Decode(typeof(Type), delimitedData[0]);

            return FormatterServices.GetUninitializedObject(type);
        }

        public void InitializeObject(IDeserializationContext context, object obj, string data)
        {
            var delimitedData = EncodedStringArray.Decode(data);
            var type = obj.GetType();
            var info = new SerializationInfo(type, new FormatterConverter());
            var streamingContext = new StreamingContext();

            foreach (var entry in delimitedData.Skip(1).Select(e => EncodedStringArray.Decode(e)))
            {
                var name = entry[0];
                var entryType = context.Decode<Type>(entry[1]);
                var value = context.Decode(entryType, entry[2]);

                info.AddValue(name, value);
            }

            ConstructorInfo constructor;
            if (!Constructors.TryGetValue(type, out constructor))
            {
                constructor = type.GetConstructor(
                    BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance,
                    null,
                    new[] { typeof(SerializationInfo), typeof(StreamingContext) },
                    new ParameterModifier[0]);
                Constructors[type] = constructor;
            }
            constructor.Invoke(obj, new object[] { info, streamingContext });
        }
    }
}
