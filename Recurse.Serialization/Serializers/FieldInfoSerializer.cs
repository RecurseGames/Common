﻿using Recurse.Persist.V2.Encodings;
using System;
using System.Reflection;

namespace Recurse.Persist.Serializers
{
    public class FieldInfoSerializer : Serializer<FieldInfo>
    {
        public override string OnCreateData(ISerializationContext context, FieldInfo obj)
        {
            var declaringTypeId = context.GetOrCreateId(obj.DeclaringType);

            return EncodedStringArray.Encode(DefaultEncoding.Encode(declaringTypeId), obj.Name);
        }

        public override object CreateObject(IDeserializationContext context, string data)
        {
            var delimitedData = EncodedStringArray.Decode(data);
            var declaringTypeId = DefaultEncoding.Decode<int>(delimitedData[0]);
            var declaringType = (Type)context.GetOrCreateObject(declaringTypeId);
            var name = delimitedData[1];

            return declaringType.GetField(name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        }

        public override void InitializeObject(IDeserializationContext context, object obj, string data)
        {
        }
    }
}
