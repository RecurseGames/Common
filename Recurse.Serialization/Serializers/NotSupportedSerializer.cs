﻿using System;

namespace Recurse.Persist.Serializers
{
    public class NotSupportedSerializer<T> : Serializer<T>
    {
        public override object CreateObject(IDeserializationContext context, string data)
        {
            throw CreateNotSupportedException();
        }

        public override void InitializeObject(IDeserializationContext context, object obj, string data)
        {
            throw CreateNotSupportedException();
        }

        public override string OnCreateData(ISerializationContext context, T obj)
        {
            throw CreateNotSupportedException();
        }

        private Exception CreateNotSupportedException()
        {
            return new NotSupportedException($"Asynchronous {typeof(T).Name} object serialization not supported.");
        }
    }
}
