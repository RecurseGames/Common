﻿using Recurse.Persist.V2.Encodings;
using System;

namespace Recurse.Persist.Serializers
{
    public class ValueSerializer : ISerializer
    {
        public bool CanSerializeType(Type type)
        {
            return DefaultEncoding.TryGet(type) != null;
        }

        public string CreateData(ISerializationContext context, object obj)
        {
            var type = obj.GetType();
            var typeId = context.GetOrCreateId(type);
            var encoding = DefaultEncoding.Get(type);

            return EncodedStringArray.Encode(DefaultEncoding.Encode(typeId), encoding.EncodeObject(obj));
        }

        public object CreateObject(IDeserializationContext context, string data)
        {
            var delimitedData = EncodedStringArray.Decode(data);
            var typeId = DefaultEncoding.Decode<int>(delimitedData[0]);
            var type = (Type)context.GetOrCreateObject(typeId);
            var encoding = DefaultEncoding.Get(type);

            return encoding.DecodeObject(delimitedData[1]);
        }

        public void InitializeObject(IDeserializationContext context, object obj, string data)
        {
        }
    }
}
