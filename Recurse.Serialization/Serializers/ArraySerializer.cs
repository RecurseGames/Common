﻿using Recurse.Persist.V2.Encodings;
using System;
using System.Linq;

namespace Recurse.Persist.Serializers
{
    public class ArraySerializer : Serializer<Array>
    {
        public override object CreateObject(IDeserializationContext context, string data)
        {
            var delimitedData = EncodedStringArray.Decode(data);
            var elementTypeId = DefaultEncoding.Decode<int>(delimitedData[0]);
            var elementType = (Type)context.GetOrCreateObject(elementTypeId);
            var rank = DefaultEncoding.Decode<int>(delimitedData[1]);
            int[] lengths = Enumerable
                .Range(0, rank)
                .Select(dimension => DefaultEncoding.Decode<int>(delimitedData[dimension + 2]))
                .ToArray();

            return Array.CreateInstance(elementType, lengths);
        }

        public override void InitializeObject(IDeserializationContext context, object obj, string data)
        {
            var array = (Array)obj;
            var delimitedData = EncodedStringArray.Decode(data);
            var elementType = array.GetType().GetElementType();
            var encoding = DefaultEncoding.TryGet(elementType);

            var index = 2 + array.Rank;
            foreach (var indices in new ArrayIndexEnumerable(array))
            {
                var elementData = delimitedData[index];
                var element = encoding != null
                    ? encoding.DecodeObject(elementData)
                    : context.GetOrCreateObject(DefaultEncoding.Decode<int>(elementData));

                array.SetValue(element, indices);

                index++;
            }
        }

        public override string OnCreateData(ISerializationContext context, Array obj)
        {
            var elementType = obj.GetType().GetElementType();
            var elementTypeId = context.GetOrCreateId(elementType);
            var data = new EncodedStringArray();

            data.Elements.Add(DefaultEncoding.Encode(elementTypeId));
            data.Elements.Add(DefaultEncoding.Encode(obj.Rank));
            for (var dimension = 0; dimension < obj.Rank; dimension++)
                data.Elements.Add(DefaultEncoding.Encode(obj.GetLength(dimension)));

            var encoding = DefaultEncoding.TryGet(elementType);
            foreach (var element in obj)
            {
                if (encoding != null)
                {
                    data.Elements.Add(encoding.EncodeObject(element));
                }
                else
                {
                    var elementId = context.GetOrCreateId(element);

                    data.Elements.Add(DefaultEncoding.Encode(elementId));
                }
            }

            return data.EncodedElements;
        }
    }
}
