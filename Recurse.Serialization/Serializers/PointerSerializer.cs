﻿using System;

namespace Recurse.Persist.Serializers
{
    public class PointerSerializer : ISerializer
    {
        public bool CanSerializeType(Type type)
        {
            return type.IsPointer || Equals(type, typeof(IntPtr)) || Equals(type, typeof(UIntPtr));
        }

        public string CreateData(ISerializationContext context, object obj)
        {
            throw CreateNotSupportedException();
        }

        public object CreateObject(IDeserializationContext context, string data)
        {
            throw CreateNotSupportedException();
        }

        public void InitializeObject(IDeserializationContext context, object obj, string data)
        {
            throw CreateNotSupportedException();
        }

        private Exception CreateNotSupportedException()
        {
            return new NotSupportedException($"Pointer serialization not supported.");
        }
    }
}
