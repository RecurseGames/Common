﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Recurse.Persist.Serializers
{
    public class ObjectMetadata
    {
        private static readonly Dictionary<Type, ObjectMetadata> Cache = new Dictionary<Type, ObjectMetadata>();

        public Type Type { get; }

        public List<FieldInfo> Fields { get; } = new List<FieldInfo>();

        public ObjectMetadata(Type type)
        {
            Type = type;
        }

        public static ObjectMetadata GetOrCreate(Type type)
        {
            ObjectMetadata metadata;
            if (!Cache.TryGetValue(type, out metadata))
            {
                metadata = new ObjectMetadata(type);

                var current = type;
                while (current != null)
                {
                    var fields = current
                        .GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                        .Where(f => (f.Attributes & FieldAttributes.NotSerialized) == 0);
                    foreach (var field in fields)
                    {
                        if ((field.Attributes & FieldAttributes.NotSerialized) == 0)
                        {
                            metadata.Fields.Add(field);
                        }
                    }
                    current = current.BaseType;
                }

                Cache[type] = metadata;
            }
            return metadata;
        }
    }
}
