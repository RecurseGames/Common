﻿using Recurse.Persist.V2.Encodings;
using System;
using System.Linq;
using System.Reflection;

namespace Recurse.Persist.Serializers
{
    public class ObjectMetadataSerializer : Serializer<ObjectMetadata>
    {
        public override object CreateObject(IDeserializationContext context, string data)
        {
            var delimitedData = EncodedStringArray.Decode(data);
            var type = (Type)context.GetOrCreateObject(DefaultEncoding.Decode<int>(delimitedData[0]));
            var metadata = new ObjectMetadata(type);

            foreach (var fieldElement in delimitedData.Skip(1))
            {
                var field = (FieldInfo)context.GetOrCreateObject(DefaultEncoding.Decode<int>(fieldElement));

                metadata.Fields.Add(field);
            }

            return metadata;
        }

        public override void InitializeObject(IDeserializationContext context, object obj, string data)
        {

        }

        public override string OnCreateData(ISerializationContext context, ObjectMetadata obj)
        {
            var data = new EncodedStringArray
            {
                Elements = { DefaultEncoding.Encode(context.GetOrCreateId(obj.Type)) }
            };
            foreach (var field in obj.Fields)
            {
                data.Elements.Add(DefaultEncoding.Encode(context.GetOrCreateId(field)));
            }
            return data.EncodedElements;
        }
    }
}
