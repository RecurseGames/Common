﻿using Recurse.Persist.V2.Encodings;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace Recurse.Persist.Serializers
{
    public class ObjectSerializer : ISerializer
    {
        public bool CanSerializeType(Type type)
        {
            return
                IsStruct(type)
                || type.GetCustomAttributes(false).OfType<SerializableAttribute>().Any();
        }

        public string CreateData(ISerializationContext context, object obj)
        {
            var type = obj.GetType();
            var metadata = ObjectMetadata.GetOrCreate(type);
            var metadataId = context.GetOrCreateId(metadata);
            var data = new EncodedStringArray
            {
                Elements = 
                {
                    DefaultEncoding.Encode(metadataId)
                }
            };

            foreach (var field in metadata.Fields)
            {
                var value = field.GetValue(obj);

                data.Elements.Add(context.Encode(field.FieldType, value));
            }

            return data.EncodedElements;
        }

        public object CreateObject(IDeserializationContext context, string data)
        {
            var delimitedData = EncodedStringArray.Decode(data);
            var metadataId = DefaultEncoding.Decode<int>(delimitedData[0]);
            var metadata = (ObjectMetadata)context.GetOrCreateObject(metadataId);

            var obj = FormatterServices.GetUninitializedObject(metadata.Type);
            if (IsStruct(metadata.Type))
            {
                SetFieldValues(context, obj, delimitedData);
            }
            return obj;
        }

        public void InitializeObject(IDeserializationContext context, object obj, string data)
        {
            var delimitedData = EncodedStringArray.Decode(data);
            var metadataId = DefaultEncoding.Decode<int>(delimitedData[0]);
            var metadata = (ObjectMetadata)context.GetOrCreateObject(metadataId);
            if (!IsStruct(metadata.Type))
            {
                SetFieldValues(context, obj, delimitedData);
            }
        }

        private void SetFieldValues(IDeserializationContext context, object obj, string[] delimitedData)
        {
            var metadataId = DefaultEncoding.Decode<int>(delimitedData[0]);
            var metadata = (ObjectMetadata)context.GetOrCreateObject(metadataId);

            for (var i = 0; i < metadata.Fields.Count; i++)
            {
                var field = metadata.Fields[i];
                var encodedValue = delimitedData[i + 1];
                var value = context.Decode(field.FieldType, encodedValue);

                field.SetValue(obj, value);
            }
        }

        private static bool IsStruct(Type type)
        {
            return type.IsValueType && !type.IsPrimitive && !type.IsEnum;
        }
    }
}
