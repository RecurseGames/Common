﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Persist.Serializers
{
    public class TypeSerializer : Serializer<Type>
    {
        public override string OnCreateData(ISerializationContext context, Type obj)
        {
            return obj.AssemblyQualifiedName;
        }

        public override object CreateObject(IDeserializationContext context, string assemblyQualifiedName)
        {
            return Type.GetType(assemblyQualifiedName) ?? FindType(assemblyQualifiedName);
        }

        public override void InitializeObject(IDeserializationContext context, object obj, string data)
        {
        }

        private static Type FindType(string assemblyQualifiedName)
        {
            var startArray = assemblyQualifiedName.IndexOf("[[");
            if (startArray != -1)
            {
                var endArray = assemblyQualifiedName.LastIndexOf(']');
                var array = assemblyQualifiedName.Substring(startArray, endArray - startArray + 1);
                var typeArgumentNames = GetAssemblyQualifiedNames(array).ToArray();
                var typeArguments = typeArgumentNames.Select(FindType).ToArray();

                var genericTypeName = assemblyQualifiedName.Substring(0, startArray);
                var genericType = GetTypeByName(genericTypeName);
                return genericType.MakeGenericType(typeArguments);
            }
            else
            {
                var assemblyIndex = assemblyQualifiedName.IndexOf(',', Math.Max(0, assemblyQualifiedName.LastIndexOf(']')));
                var typeName = assemblyQualifiedName.Substring(0, assemblyIndex);
                return GetTypeByName(typeName);
            }
        }

        private static Type GetTypeByName(string name)
        {
            return AppDomain.CurrentDomain
                .GetAssemblies()
                .Select(a => a.GetType(name))
                .FirstOrDefault(type => type != null);
        }

        private static IEnumerable<string> GetAssemblyQualifiedNames(string array)
        {
            var arrayContent = array.Substring(2, array.Length - 3);
            var brackets = 1;
            var elementIndex = 0;
            for (var i = 0; i < arrayContent.Length; i++)
            {
                if (arrayContent[i] == '[' && ++brackets == 1)
                {
                    elementIndex = i + 1;
                }
                else if (arrayContent[i] == ']' && --brackets == 0)
                {
                    var elementLength = i - elementIndex;
                    yield return arrayContent.Substring(elementIndex, elementLength);
                }
            }
        }
    }
}
