﻿using System;
using System.Linq;
using System.Reflection;

namespace Recurse.Persist.Serializers
{
    public class DelegateSerializer : Serializer<Delegate>
    {
        private const BindingFlags GetMethodBindingFlags
            = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static;

        public override object CreateObject(IDeserializationContext context, string data)
        {
            var delimitedData = EncodedStringArray.Decode(data);
            var name = delimitedData[1];
            var types = delimitedData.Skip(2).Select(e => (Type)context.Decode(typeof(Type), e)).ToArray();
            var reflectedType = types[0];
            var delegateType = types[1];
            var argumentTypes = types.Skip(2).ToArray();
            var target = context.Decode(reflectedType, delimitedData[0]);

            return Delegate.CreateDelegate(
                delegateType,
                target,
                reflectedType.GetMethod(name, GetMethodBindingFlags, null, argumentTypes, null));
        }

        public override string OnCreateData(ISerializationContext context, Delegate obj)
        {
            if (obj.Method.Name.Contains("<"))
            {
                throw new InvalidOperationException("Cannot create serializable anonymous delegate.");
            }

            var data = new EncodedStringArray();

            data.Elements.Add(context.Encode(obj.Method.ReflectedType, obj.Target));
            data.Elements.Add(obj.Method.Name);
            data.Elements.Add(context.Encode(typeof(Type), obj.Method.ReflectedType));
            data.Elements.Add(context.Encode(typeof(Type), obj.GetType()));
            foreach (var parameter in obj.Method.GetParameters())
            {
                data.Elements.Add(context.Encode(typeof(Type), parameter.ParameterType));
            }

            return data.EncodedElements;
        }

        public override void InitializeObject(IDeserializationContext context, object obj, string data)
        {

        }
    }
}
