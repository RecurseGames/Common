﻿using System;

namespace Recurse.Persist
{
    public abstract class Serializer<T> : ISerializer
    {
        string ISerializer.CreateData(ISerializationContext context, object obj)
        {
            return OnCreateData(context, (T)obj);
        }

        public bool CanSerializeType(Type type)
        {
            return typeof(T).IsAssignableFrom(type);
        }

        public abstract object CreateObject(IDeserializationContext context, string data);

        public abstract void InitializeObject(IDeserializationContext context, object obj, string data);

        public abstract string OnCreateData(ISerializationContext context, T obj);
    }
}
