﻿using System;

namespace Recurse.Persist
{
    public interface ISerializationModel
    {
        ISerializer GetSerializer(int id);

        int GetSerializerId(Type type);
    }
}
