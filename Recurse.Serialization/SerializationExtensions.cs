﻿using Recurse.Persist;
using Recurse.Persist.V2.Encodings;
using System;

// ReSharper disable once CheckNamespace
namespace Recurse
{
    public static class SerializationExtensions
    {
        public static string Encode(this ISerializationContext context, Type type, object value)
        {
            var encoding = DefaultEncoding.TryGet(type);
            return encoding != null
                ? encoding.EncodeObject(value)
                : DefaultEncoding.Encode(context.GetOrCreateId(value));
        }

        public static object Decode(this IDeserializationContext context, Type type, string value)
        {
            var encoding = DefaultEncoding.TryGet(type);
            return encoding != null
                ? encoding.DecodeObject(value)
                : context.GetOrCreateObject(DefaultEncoding.Decode<int>(value));
        }

        public static T Decode<T>(this IDeserializationContext context, string value)
        {
            var encoding = DefaultEncoding.TryGet(typeof(T));
            return encoding != null
                ? (T)encoding.DecodeObject(value)
                : (T)context.GetOrCreateObject(DefaultEncoding.Decode<int>(value));
        }
    }
}
