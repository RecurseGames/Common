﻿using Recurse.Persist.Serializers;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Recurse.Persist.V2.Defaults
{
    public static class DefaultSerializationModel
    {
        public static ISerializationModel Instance { get; }

        static DefaultSerializationModel()
        {
            Instance = Create();
        }

        public static SerializationModel Create()
        {
            return new SerializationModel
            {
                Serializers =
                {
                    new ValueSerializer(),
                    new TypeSerializer(),
                    new FieldInfoSerializer(),
                    new ArraySerializer(),
                    new DelegateSerializer(),

                    new SerializableSerializer(typeof(Dictionary<,>)),
                    new SerializableSerializer(typeof(HashSet<>)),
                    new SerializableSerializer(typeof(WeakReference)),
                    new SerializableSerializer(typeof(WeakReference<>)),

                    new NotSupportedSerializer<ISerializable>(),
                    new PointerSerializer(),

                    new ObjectSerializer(),
                    new ObjectMetadataSerializer()
                }
            };
        }
    }
}
