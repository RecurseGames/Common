﻿using System;
using System.Collections.Generic;

namespace Recurse.Persist.V2.Defaults
{
    public class SerializationModel : ISerializationModel
    {
        private readonly List<ISerializer> _serializers = new List<ISerializer>();

        public IList<ISerializer> Serializers => _serializers;

        public ISerializer GetSerializer(int id) => _serializers[id];

        public int GetSerializerId(Type type)
        {
            var serializerId = _serializers.FindIndex(s => s != null && s.CanSerializeType(type));
            if (serializerId == -1)
            {
                throw new InvalidOperationException($"Could not find serializer for type {type}");
            }
            return serializerId;
        }
    }
}
