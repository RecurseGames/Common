﻿namespace Recurse.Persist
{
    public interface ISerializationContext
    {
        int GetOrCreateId(object obj);
    }
}
