﻿namespace Recurse.Persist
{
    public interface ISerializerContext
    {
        int UninitializedEntries { get; }

        void InitializeEntry();
    }
}
