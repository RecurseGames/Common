﻿namespace Recurse.Persist.V2.Encodings
{
    public class CharEncoding : IEncoding<char>
    {
        public char Decode(string str)
        {
            return str[0];
        }

        public object DecodeObject(string str)
        {
            return str[0];
        }

        public string Encode(char value)
        {
            return value.ToString();
        }

        public string EncodeObject(object value)
        {
            return value.ToString();
        }
    }
}
