﻿namespace Recurse.Persist.V2.Encodings
{
    public class StringEncoding : IEncoding<string>
    {
        public string Decode(string str)
        {
            return str;
        }

        public object DecodeObject(string str)
        {
            return str;
        }

        public string Encode(string value)
        {
            return value;
        }

        public string EncodeObject(object value)
        {
            return (string)value;
        }
    }
}
