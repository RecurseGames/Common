﻿namespace Recurse.Persist.V2.Encodings
{
    public class BooleanEncoding : IEncoding<bool>
    {
        public bool Decode(string str)
        {
            return Equals(str, "1");
        }

        public object DecodeObject(string str)
        {
            return Equals(str, "1");
        }

        public string Encode(bool value)
        {
            return value ? "1" : "0";
        }

        public string EncodeObject(object value)
        {
            return (bool)value ? "1" : "0";
        }
    }
}
