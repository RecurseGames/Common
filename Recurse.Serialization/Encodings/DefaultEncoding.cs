﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Recurse.Persist.V2.Encodings
{
    public static class DefaultEncoding
    {
        private static readonly Dictionary<Type, PropertyInfo> PrimaryCache = new Dictionary<Type, PropertyInfo>();

        private static readonly Dictionary<Type, IEncoding> SecondaryCache = new Dictionary<Type, IEncoding>();

        static DefaultEncoding()
        {
            DefaultEncoding<short>.Instance = new UnicodeEncoding<short>(BitConverter.GetBytes, BitConverter.ToInt16);
            DefaultEncoding<int>.Instance = new UnicodeEncoding<int>(BitConverter.GetBytes, BitConverter.ToInt32);
            DefaultEncoding<long>.Instance = new UnicodeEncoding<long>(BitConverter.GetBytes, BitConverter.ToInt64);
            DefaultEncoding<ushort>.Instance = new UnicodeEncoding<ushort>(BitConverter.GetBytes, BitConverter.ToUInt16);
            DefaultEncoding<uint>.Instance = new UnicodeEncoding<uint>(BitConverter.GetBytes, BitConverter.ToUInt32);
            DefaultEncoding<ulong>.Instance = new UnicodeEncoding<ulong>(BitConverter.GetBytes, BitConverter.ToUInt64);
            DefaultEncoding<float>.Instance = new UnicodeEncoding<float>(BitConverter.GetBytes, BitConverter.ToSingle);
            DefaultEncoding<double>.Instance = new UnicodeEncoding<double>(BitConverter.GetBytes, BitConverter.ToDouble);
            DefaultEncoding<bool>.Instance = new BooleanEncoding();
            DefaultEncoding<char>.Instance = new CharEncoding();
            DefaultEncoding<string>.Instance = new StringEncoding();
        }

        public static IEncoding TryGet(Type type)
        {
            if (type.IsEnum)
            {
                type = Enum.GetUnderlyingType(type);
            }

            IEncoding result;
            if (!SecondaryCache.TryGetValue(type, out result))
            {
                PropertyInfo property;
                if (!PrimaryCache.TryGetValue(type, out property))
                {
                    property = typeof(DefaultEncoding<>)
                        .MakeGenericType(type)
                        .GetProperty(nameof(DefaultEncoding<object>.Instance));
                    PrimaryCache[type] = property;
                }

                result = (IEncoding)property.GetValue(null, null);
                if (result != null)
                {
                    SecondaryCache[type] = result;
                }
            }
            return result;
        }

        public static IEncoding Get(Type type)
        {
            var result = TryGet(type);
            if (result == null)
            {
                throw new InvalidOperationException($"Encoding for {type} not defined.");
            }

            return result;
        }

        public static T Decode<T>(string str)
        {
            var generic = DefaultEncoding<T>.Instance;
            if (generic == null)
                throw new InvalidOperationException($"Encoding for {typeof(T)} not defined.");
            return generic.Decode(str);
        }

        public static string Encode<T>(T value)
        {
            var generic = DefaultEncoding<T>.Instance;
            if (generic == null)
                throw new InvalidOperationException($"Encoding for {typeof(T)} not defined.");
            return generic.Encode(value);
        }
    }

    public static class DefaultEncoding<T>
    {
        public static IEncoding<T> Instance { get; set; }
    }
}
