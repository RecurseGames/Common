﻿using System;
using System.Text;

namespace Recurse.Persist.V2.Encodings
{
    public class UnicodeEncoding<T> : IEncoding<T>
    {
        private readonly Func<T, byte[]> _encoder;

        private readonly Func<byte[], int, T> _decoder;

        public UnicodeEncoding(Func<T, byte[]> encoder, Func<byte[], int, T> decoder)
        {
            _encoder = encoder;
            _decoder = decoder;
        }

        public string EncodeObject(object value)
        {
            var bytes = _encoder((T)value);
            return Encoding.Unicode.GetString(bytes);
        }

        public object DecodeObject(string str)
        {
            var bytes = Encoding.Unicode.GetBytes(str);
            return _decoder(bytes, 0);
        }

        public string Encode(T value)
        {
            var bytes = _encoder(value);
            return Encoding.Unicode.GetString(bytes);
        }

        public T Decode(string str)
        {
            var bytes = Encoding.Unicode.GetBytes(str);
            return _decoder(bytes, 0);
        }
    }
}
