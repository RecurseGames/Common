﻿namespace Recurse.Persist.V2.Encodings
{
    public interface IEncoding
    {
        object DecodeObject(string str);

        string EncodeObject(object value);
    }

    public interface IEncoding<T> : IEncoding
    {
        string Encode(T value);

        T Decode(string str);
    }
}
