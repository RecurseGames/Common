﻿namespace Recurse.Persist
{
    public class SerializationEntry
    {
        private static readonly StringEncoding LineEncoding = new StringEncoding('\\')
            .AddEscape('\n', 'n');

        public int SerializerId { get; }

        public string Data { get; set; }

        public SerializationEntry(int serializerId)
        {
            SerializerId = serializerId;
        }

        public static SerializationEntry FromLine(string line)
        {
            var decodedLine = LineEncoding.Decode(line);

            return new SerializationEntry(line[0])
            {
                Data = line.Substring(1)
            };
        }

        public string ToLine()
        {
            var decodedLine = (char)SerializerId + Data;

            return LineEncoding.Encode(decodedLine);
        }
    }
}
