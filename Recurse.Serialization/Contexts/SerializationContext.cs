﻿using System;
using System.Collections.Generic;

namespace Recurse.Persist
{
    public class SerializationContext : ISerializerContext, ISerializationContext
    {
        private readonly ISerializationModel _model;

        private readonly Dictionary<object, int> _entryIds = new Dictionary<object, int>();

        private readonly List<SerializationEntry> _entries = new List<SerializationEntry>();

        private Queue<object> _uninitializedDataObjects { get; } = new Queue<object>();

        public IReadOnlyList<SerializationEntry> Entries => _entries;

        public int UninitializedEntries => _uninitializedDataObjects.Count;

        public SerializationContext(ISerializationModel model)
        {
            _model = model;
        }

        public void InitializeEntry()
        {
            if (UninitializedEntries == 0)
            {
                throw new InvalidOperationException("No uninitialised entries remaining.");
            }

            var uninitialisedDataObject = _uninitializedDataObjects.Dequeue();
            var uninitialisedEntryId = _entryIds[uninitialisedDataObject];
            var uninitialisedEntry = _entries[uninitialisedEntryId];
            var serializer = _model.GetSerializer(uninitialisedEntry.SerializerId);

            uninitialisedEntry.Data = serializer.CreateData(this, uninitialisedDataObject);
        }

        public int GetOrCreateId(object obj)
        {
            if (obj == null)
            {
                return -1;
            }

            int id;
            if (!_entryIds.TryGetValue(obj, out id))
            {
                id = _entries.Count;

                var type = obj.GetType();
                var serializerId = _model.GetSerializerId(type);
                var serializer = _model.GetSerializer(serializerId);
                var entry = new SerializationEntry(serializerId);

                _entryIds[obj] = id;
                _entries.Add(entry);
                _uninitializedDataObjects.Enqueue(obj);
            }
            return id;
        }
    }
}
