﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Recurse.Persist
{
    public class DeserializationContext : ISerializerContext, IDeserializationContext
    {
        private readonly ISerializationModel _model;

        private readonly List<SerializationEntry> _entries = new List<SerializationEntry>();

        private readonly List<object> _objects = new List<object>();

        private readonly Queue<int> _uninitializedEntryIds = new Queue<int>();

        private readonly Queue<IDeserializationCallback> _deserializationCallbacks = new Queue<IDeserializationCallback>();

        public int UninitializedEntries => _uninitializedEntryIds.Count + _deserializationCallbacks.Count;

        public DeserializationContext(ISerializationModel model)
        {
            _model = model;
        }

        public void AddEntries(IEnumerable<SerializationEntry> entries)
        {
            foreach (var entry in entries)
            {
                AddEntry(entry);
            }
        }

        public void AddEntry(SerializationEntry entry)
        {
            _objects.Add(null);
            _entries.Add(entry);
        }

        public void InitializeEntry()
        {
            if (UninitializedEntries == 0)
            {
                throw new InvalidOperationException("No uninitialised entries remaining.");
            }

            if (_uninitializedEntryIds.Count != 0)
            {
                var uninitialisedEntryId = _uninitializedEntryIds.Dequeue();
                var uninitialisedEntry = _entries[uninitialisedEntryId];
                var uninitialisedDataObject = _objects[uninitialisedEntryId];
                var serializer = _model.GetSerializer(uninitialisedEntry.SerializerId);

                serializer.InitializeObject(this, uninitialisedDataObject, uninitialisedEntry.Data);
            }
            else
            {
                var deserializationCallback = _deserializationCallbacks.Dequeue();

                deserializationCallback.OnDeserialization(this);
            }
        }

        public T GetOrCreateRoot<T>()
        {
            return _entries.Count == 0
                ? default(T)
                : (T)GetOrCreateObject(0);
        }

        public object GetOrCreateObject(int id)
        {
            if (id == -1)
            {
                return null;
            }

            var obj = _objects[id];
            if (obj == null)
            {
                var entry = _entries[id];
                var serializer = _model.GetSerializer(entry.SerializerId);

                obj = serializer.CreateObject(this, entry.Data);

                _objects[id] = obj;
                _uninitializedEntryIds.Enqueue(id);

                var deserializationCallback = obj as IDeserializationCallback;
                if (deserializationCallback != null)
                {
                    _deserializationCallbacks.Enqueue(deserializationCallback);
                }
            }
            return obj;
        }
    }
}
