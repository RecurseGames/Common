﻿namespace Recurse.Persist
{
    public interface IDeserializationContext
    {
        object GetOrCreateObject(int id);
    }
}
