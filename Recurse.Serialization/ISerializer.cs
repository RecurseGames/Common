﻿using System;

namespace Recurse.Persist
{
    public interface ISerializer
    {
        bool CanSerializeType(Type type);

        string CreateData(ISerializationContext context, object obj);

        object CreateObject(IDeserializationContext context, string data);

        void InitializeObject(IDeserializationContext context, object obj, string data);
    }
}
