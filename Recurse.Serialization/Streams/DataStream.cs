﻿using System;

namespace Recurse.Persist.Files
{
    public abstract class DataStream : IDisposable
    {
        public abstract double Progress { get; }

        public abstract void Dispose();

        public abstract bool TryContinue();
    }
}
