﻿using Recurse.Persist.V2.Defaults;
using System.IO;

namespace Recurse.Persist.Files
{
    public class ReadDataStream<TRoot> : DataStream
    {
        private StreamReader _stream;

        private DeserializationContext _context = new DeserializationContext(DefaultSerializationModel.Instance);

        private int _initialisedEntries;

        public TRoot FileRoot { get; private set; }

        public override double Progress
        {
            get
            {
                if (_stream != null)
                {
                    return _stream.BaseStream.Length != 0
                        ? 0.5 * _stream.BaseStream.Position / _stream.BaseStream.Length
                        : 0;
                }
                else
                {
                    var requiredInitializations = _context.UninitializedEntries + _initialisedEntries;
                    return requiredInitializations != 0
                        ? 0.5 + 0.5 * _initialisedEntries / requiredInitializations
                        : 0.5;
                }
            }
        }

        public ReadDataStream(StreamReader stream)
        {
            _stream = stream;
        }

        public override void Dispose()
        {
            _stream?.Dispose();
            _context = null;
        }

        public override bool TryContinue()
        {
            if (_context == null)
            {
                return false;
            }
            else if (_stream != null)
            {
                var line = _stream.ReadLine();
                if (line != null)
                {
                    var entry = SerializationEntry.FromLine(line);
                    _context.AddEntry(entry);
                }
                else
                {
                    _stream.Dispose();
                    _stream = null;

                    // Ensure the context begins deserializing items
                    _context.GetOrCreateRoot<TRoot>();
                }
                return true;
            }
            else if (_context.UninitializedEntries != 0)
            {
                _context.InitializeEntry();
                _initialisedEntries++;
                return true;
            }
            else
            {
                FileRoot = _context.GetOrCreateRoot<TRoot>();

                return false;
            }
        }
    }
}
