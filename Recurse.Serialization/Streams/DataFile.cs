﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Persist.Files;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Recurse.Persist.IO
{
    public class DataFile
    {
        private readonly string _path;

        public string Name => Path.GetFileNameWithoutExtension(_path);

        public DataFile(string path)
        {
            _path = path;
        }

        public DataFile(DataFile parent, string name, string extension = null)
        {
            var nameWithExtension = extension != null ? $"{name}.{extension}" : name;
            _path = Path.Combine(parent._path, nameWithExtension);
        }

        public DataFile<T> Create<T>(string key, T data) where T : class
        {
            return new DataFile<T>(this, key, data);
        }

        public StreamReader CreateReader(string name)
        {
            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);

            var path = Path.Combine(_path, name);
            if (!File.Exists(path))
                throw new InvalidOperationException($"File not found: {path}");

            return File.OpenText(path);
        }

        public StreamWriter CreateWriter(string name)
        {
            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);

            var path = Path.Combine(_path, name);
            return File.CreateText(path);
        }

        public IEnumerable<DataFile> GetChildrenWithExtension(string extension)
        {
            return !Directory.Exists(_path) ?
                Enumerable.Empty<DataFile>() :
                Directory
                    .GetDirectories(_path)
                    .Where(path => Equals(Path.GetExtension(path).TrimStart('.'), extension))
                    .Select(path => new DataFile(path));
        }

        public void Delete()
        {
            if (Directory.Exists(_path))
                Directory.Delete(_path, true);
        }

        public DataFile GetParent()
        {
            return new DataFile(Path.GetDirectoryName(_path));
        }

        public override string ToString()
        {
            return _path;
        }
    }

    public class DataFile<T> where T : class
    {
        private readonly string _key;

        private readonly DataFile _parent;

        private T _initialContent;

        private ReadDataStream<T> _read;

        public T Content => _read != null ? _read.FileRoot : _initialContent;

        public DataFile(DataFile parent, string key, T data = null)
        {
            _key = key;
            _parent = parent;
            _initialContent = data;
        }

        /// <summary>
        /// Clears the cache, and begins loading data from the filesystem (any previous
        /// loading of data is cancelled) if possible.
        /// </summary>
        public DataStream StartRead()
        {
            _initialContent = default(T);
            _read?.Dispose();
            var stream = _parent.CreateReader(_key);
            _read = new ReadDataStream<T>(stream);
            return _read;
        }

        /// <summary>
        /// Creates an operation which writes the current contents of the cache to the filesystem
        /// </summary>
        public DataStream StartWrite()
        {
            if (Content == null)
                throw new InvalidOperationException("Cache is empty");

            var stream = _parent.CreateWriter(_key);
            return new WriteDataStream<T>(stream, Content);
        }
    }
}
