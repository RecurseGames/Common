﻿using Recurse.Persist.V2.Defaults;
using System.IO;

namespace Recurse.Persist.Files
{
    public class WriteDataStream<TRoot> : DataStream
    {
        private StreamWriter _stream;

        private SerializationContext _context = new SerializationContext(DefaultSerializationModel.Instance);

        private int _initialisedEntries;

        private int _pendingEntry;

        public override double Progress
        {
            get
            {
                if (_stream != null)
                {
                    return 0.5 + 0.5 * _pendingEntry / _context.Entries.Count;
                }
                else if (_context == null || _context.UninitializedEntries == 0)
                {
                    return 1;
                }
                else
                {
                    var requiredInitializations = _context.UninitializedEntries + _initialisedEntries;
                    return requiredInitializations != 0
                        ? 0.5 * _initialisedEntries / requiredInitializations
                        : 0;
                }
            }
        }

        public WriteDataStream(StreamWriter stream, TRoot fileRoot)
        {
            _stream = stream;
            _context.GetOrCreateId(fileRoot);
        }

        public override void Dispose()
        {
            _stream?.Dispose();
            _context = null;
        }

        public override bool TryContinue()
        {
            if (_context == null)
            {
                return false;
            }
            else if (_context.UninitializedEntries != 0)
            {
                _context.InitializeEntry();
                _initialisedEntries++;
                return true;
            }
            else if (_pendingEntry < _context.Entries.Count)
            {
                var line = _context.Entries[_pendingEntry].ToLine();
                _stream.WriteLine(line);
                _pendingEntry++;
                return true;
            }
            else
            {
                _stream.Dispose();
                _stream = null;
                _context = null;
                return false;
            }
        }
    }
}
