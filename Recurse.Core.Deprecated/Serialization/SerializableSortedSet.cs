﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Recurse.Core.Serialization
{
    public class SerializableSortedSet<T> : SerializableSet<SortedSet<T>, IComparer<T>, T>
    {
        protected override IComparer<T> Comparer
            => Backing.Comparer;

        public T Min => Backing.Min;

        public T Max => Backing.Max;

        public SerializableSortedSet()
        {
        }

        public SerializableSortedSet(IComparer<T> comparer) : base(comparer)
        {
        }

        public SerializableSortedSet(IEnumerable<T> elements) : base(elements)
        {
        }

        public SerializableSortedSet(IEnumerable<T> elements, IComparer<T> comparer) : base(elements, comparer)
        {
        }

        protected SerializableSortedSet(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected override SortedSet<T> CreateBacking(IEnumerable<T> elements, IComparer<T> comparer)
            => new SortedSet<T>(elements, comparer ?? Comparer<T>.Default);
    }
}
