﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Recurse.Core.Serialization
{
    public enum DeserializationState
    {
        /// <summary>
        /// Indicates deserialization is in progress, and object members cannot be
        /// safely accessed.
        /// </summary>
        Deserializing,

        /// <summary>
        /// Indicates deserialization is completed, and object members can be
        /// safely accessed, but objects may still need to update their internal state
        /// as a result of deserialization.
        /// </summary>
        Deserialized,

        /// <summary>
        /// Indicates objects have been deserialized and have updated their
        /// internal states as required.
        /// </summary>
        Initialised
    }
}
