﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Recurse.Core.Serialization
{
    public class SerializableHashSet<T> : SerializableSet<HashSet<T>, IEqualityComparer<T>, T>
    {
        protected override IEqualityComparer<T> Comparer => Backing.Comparer;

        public SerializableHashSet()
        {
        }

        public SerializableHashSet(IEqualityComparer<T> comparer) : base(comparer)
        {
        }

        public SerializableHashSet(IEnumerable<T> elements) : base(elements)
        {
        }

        public SerializableHashSet(IEnumerable<T> elements, IEqualityComparer<T> comparer) : base(elements, comparer)
        {
        }

        protected SerializableHashSet(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public int RemoveWhere(Predicate<T> match)
        {
            return Backing.RemoveWhere(match);
        }

        protected override HashSet<T> CreateBacking(IEnumerable<T> elements, IEqualityComparer<T> comparer)
        {
            return new HashSet<T>(elements, comparer ?? EqualityComparer<T>.Default);
        }
    }
}
