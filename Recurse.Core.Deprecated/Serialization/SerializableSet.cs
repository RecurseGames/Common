﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Core.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Recurse.Core.Serialization
{
    public abstract class SerializableSet<TBacking, TComparer, TElement> 
        : BaseSet<TBacking, TElement>, ISerializable, IDeserializationObserver
        where TBacking : ISet<TElement>
    {
        private readonly TBacking _backing;

        private DeserializationState _deserializationState;
        
        private TElement[] _deserializedElements;

        protected abstract TComparer Comparer { get; }

        protected override TBacking Backing
        {
            get
            {
                OnDeserialization(DeserializationState.Initialised);
                return _backing;
            }
        }

        public SerializableSet() : this(Enumerable.Empty<TElement>(), default(TComparer))
        {
        }

        public SerializableSet(TComparer comparer) : this(Enumerable.Empty<TElement>(), comparer)
        {
        }

        public SerializableSet(IEnumerable<TElement> elements) : this(elements, default(TComparer))
        {
        }

        public SerializableSet(IEnumerable<TElement> elements, TComparer comparer)
        {
            _backing = CreateBacking(elements, comparer);
            _deserializationState = DeserializationState.Initialised;
        }

        protected SerializableSet(SerializationInfo info, StreamingContext context)
        {
            var comparer = (TComparer)info.GetValue("comparer", typeof(TComparer));

            _backing = CreateBacking(Enumerable.Empty<TElement>(), comparer);
            _deserializedElements = (TElement[])info.GetValue("elements", typeof(TElement[]));
            _deserializationState = DeserializationState.Deserialized;
        }

        public void OnDeserialization(DeserializationState state)
        {
            if (state.TryChange(ref _deserializationState) && state == DeserializationState.Initialised)
            {
                foreach (var entry in _deserializedElements)
                    _backing.Add(entry);
                _deserializedElements = null;
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("comparer", Comparer);
            info.AddValue("elements", Backing.ToArray());
        }

        protected abstract TBacking CreateBacking(IEnumerable<TElement> elements, TComparer comparer);
    }
}
