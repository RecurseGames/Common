﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Recurse.Core.Serialization
{
    /// <summary>
    /// An alternative to <see cref="System.Runtime.Serialization.IDeserializationCallback"/>, which
    /// cannot be supported by this serialization framework due to its repulsive misuse by 
    /// <see cref="System.Collections.Generic.Dictionary{TKey, TValue}"/> and
    /// <see cref="System.Collections.Generic.HashSet{T}"/> (these classes do not actually deserialize
    /// their data when their serialization constructor is called, which prevents them from being
    /// deserialized asynchronously.
    /// </summary>
    #warning Delete IDeserializationObserver
    public interface IDeserializationObserver
    {
        void OnDeserialization(DeserializationState state);
    }
}
