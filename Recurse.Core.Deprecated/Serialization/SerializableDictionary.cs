﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Core.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Recurse.Core.Serialization
{
    [Serializable]
    public class SerializableDictionary<TKey, TValue> : 
        ISerializable, 
        IDeserializationObserver,
        IDictionary<TKey, TValue>, 
        IReadOnlyDictionary<TKey, TValue>,
        IReadOnlyMapping<TKey, TValue>
    {
        private readonly Dictionary<TKey, TValue> _implementation;

        private KeyValuePair<TKey, TValue>[] _deserializedEntries;

        private DeserializationState _deserializationState;

        private Dictionary<TKey, TValue> Implementation
        {
            get
            {
                OnDeserialization(DeserializationState.Initialised);
                return _implementation;
            }
        }

        public SerializableDictionary() : this(EqualityComparer<TKey>.Default)
        {

        }

        public SerializableDictionary(IEqualityComparer<TKey> keyComparer)
        {
            _implementation = new Dictionary<TKey, TValue>(keyComparer);
            _deserializationState = DeserializationState.Initialised;
        }

        public SerializableDictionary(IDictionary<TKey, TValue> values)
        {
            _implementation = new Dictionary<TKey, TValue>(values);
            _deserializationState = DeserializationState.Initialised;
        }

        protected SerializableDictionary(SerializationInfo info, StreamingContext context)
        {
            var keyComparer = (IEqualityComparer<TKey>)info.GetValue("comparer", typeof(IEqualityComparer<TKey>));

            _implementation = new Dictionary<TKey, TValue>(keyComparer);
            _deserializedEntries = (KeyValuePair<TKey, TValue>[])info.GetValue("entries", typeof(KeyValuePair<TKey, TValue>[]));
            _deserializationState = DeserializationState.Deserialized;
        }

        public void OnDeserialization(DeserializationState state)
        {
            if (state.TryChange(ref _deserializationState) && state == DeserializationState.Initialised)
            {
                foreach (var entry in _deserializedEntries)
                    _implementation.Add(entry.Key, entry.Value);
                _deserializedEntries = null;
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("comparer", Implementation.Comparer);
            info.AddValue("entries", Implementation.ToArray());
        }

        #region IReadOnlyDictionary Implementation
        
        IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => Implementation.Keys;

        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => Implementation.Values;

        #endregion

        #region IDictionary Implementation

        public bool IsReadOnly => ((IDictionary<TKey, TValue>)Implementation).IsReadOnly;

        public virtual TValue this[TKey key]
        {
            get
            {
                return Implementation[key];
            }
            set
            {
                Implementation[key] = value;
            }
        }

        public ICollection<TKey> Keys => Implementation.Keys;

        public ICollection<TValue> Values => Implementation.Values;

        public int Count => Implementation.Count;

        public void Add(TKey key, TValue value)
        {
            Implementation.Add(key, value);
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            ((IDictionary<TKey, TValue>)Implementation).Add(item);
        }

        public void Clear()
        {
            Implementation.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return Implementation.Contains(item);
        }

        public bool ContainsKey(TKey key)
        {
            return Implementation.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            ((IDictionary<TKey, TValue>)Implementation).CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return Implementation.GetEnumerator();
        }

        public bool Remove(TKey key)
        {
            return Implementation.Remove(key);
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return ((IDictionary<TKey, TValue>)Implementation).Remove(item);
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return Implementation.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Implementation.GetEnumerator();
        }

        #endregion
    }
}
