﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Recurse.Core.Serialization
{
    public static class DeserializationStateExtensions
    {
        public static bool TryChange(this DeserializationState newState, ref DeserializationState state)
        {
            if (newState == state)
                return false;

            if (state == DeserializationState.Deserializing && newState == DeserializationState.Deserialized ||
                state == DeserializationState.Deserialized && newState == DeserializationState.Initialised)
            {
                state = newState;
                return true;
            }

            throw new InvalidOperationException("Invalid state change.");
        }
    }
}
