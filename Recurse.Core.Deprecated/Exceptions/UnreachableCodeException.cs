﻿using System;

namespace Recurse.Core.Exceptions
{
    /// <summary>
    /// This exception indicates code has executed which should not be
    /// reachable. It indicates an error in programming logic.
    /// </summary>
    public class UnreachableCodeException : Exception
    {
        public UnreachableCodeException(string message) : base(message)
        {

        }

        public UnreachableCodeException() : base("Code executed which should have been unreachable.")
        {

        }
    }
}
