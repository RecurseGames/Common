﻿using System;

namespace Recurse.Core.Exceptions
{
    public class UnexpectedEnumException : Exception
    {
        public UnexpectedEnumException(object value) : base($"Unexpected enum value: {value}")
        {
        }
    }
}
