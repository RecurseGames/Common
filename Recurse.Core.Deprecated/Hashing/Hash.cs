﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Hashing
{
    /// <summary>
    /// A hashing function designed to handle low-quality inputs
    /// </summary>
    public struct Hash : IEquatable<Hash>, IConsistentHashable
    {
        public static readonly Hash Default = default(Hash);

        private uint Seed { get; }

        public Hash(string str)
        {
            unchecked
            {
                int hash1 = 5381;
                int hash2 = hash1;

                for (int i = 0; i < str.Length && str[i] != '\0'; i += 2)
                {
                    hash1 = ((hash1 << 5) + hash1) ^ str[i];
                    if (i == str.Length - 1 || str[i + 1] == '\0')
                        break;
                    hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
                }

                Seed = (uint)(hash1 + hash2 * 1566083941);
            }
        }

        public Hash(uint value)
        {
            Seed = value;
        }

        public Hash(double value)
        {
            Seed = (uint)value.GetHashCode();
        }

        public Hash(float value)
        {
            Seed = (uint)value.GetHashCode();
        }

        public Hash(int value)
        {
            Seed = (uint)value;
        }

        public static Hash FromUnordered(Hash a, Hash b)
        {
            return a.Seed < b.Seed ? a.Add(b) : b.Add(a);
        }

        /// <summary>
        /// Converts the given arguments into a hashcode.
        /// It is considered unstable because, if the objects
        /// are deserialized, their hashcode may change.
        /// </summary>
        public static int GetUnstableHashCode<T>(params T[] args)
            => default(Hash).AddUnstable(args).GetHashCode();

        /// <summary>
        /// Converts the given arguments into a hashcode.
        /// It is considered unstable because, if the objects
        /// are deserialized, their hashcode may change.
        /// </summary>
        public static int GetUnstableHashCode<T>(IEnumerable<T> args)
            => default(Hash).AddUnstable(args).GetHashCode();

        public Hash Add(params int[] values)
            => AddUnstable(values);

        public Hash Add(params double[] values)
            => AddUnstable(values);

        public Hash Add(params float[] values)
            => AddUnstable(values);

        public Hash Add(params uint[] values)
            => AddUnstable(values);

        public Hash Add(params string[] values)
            => Add(values.Select(s => new Hash(s)).ToArray());

        public Hash Add<T>(params T[] values)
            where T : IConsistentHashable
            => AddUnstable(values);

        public Hash AddRange<T>(IEnumerable<T> values)
            where T : IConsistentHashable
            => AddUnstable(values);

        private Hash AddUnstable<T>(IEnumerable<T> args)
        {
            var argCount = 0;
            var result = this;
            foreach (var arg in args)
            {
                var argHashCode = arg.GetHashCode();
                result = result.GetPartial(unchecked((uint)argHashCode));
                argCount++;
            }
            return result.GetFinalised(argCount * 4);
        }

        public T ToElement<T>(IList<T> list)
        {
            return list[ToInteger(list.Count)];
        }

        public bool ToBoolean()
        {
            return ToInteger(2) == 0;
        }

        public int ToHashCode()
        {
            return unchecked((int)Seed);
        }

        public int ToInteger(int maxValue)
        {
            var modulo = ToHashCode() % maxValue;
            return modulo < 0 ? modulo + maxValue : modulo;
        }

        public float ToFloat()
        {
            return (float)Seed / uint.MaxValue;
        }

        public double ToDouble()
        {
            return (double)Seed / uint.MaxValue;
        }

        public bool Equals(Hash other)
        {
            return Equals(Seed, other.Seed);
        }

        public override bool Equals(object obj)
        {
            return obj is Hash ? Equals((Hash)obj) : false;
        }

        public override int GetHashCode()
        {
            return unchecked((int)Seed);
        }

        public override string ToString()
        {
            return Seed.ToString();
        }

        private Hash GetPartial(uint data)
        {
            var accumulator = Seed;

            data *= 0xcc9e2d51;
            data = RotateBitsLeft(data, 15);
            data *= 0x1b873593;

            accumulator ^= data;
            accumulator = RotateBitsLeft(accumulator, 13);
            accumulator = accumulator * 5 + 0xe6546b64;

            return new Hash(accumulator);
        }

        private Hash GetFinalised(int consumedBytes)
        {
            var accumulator = Seed;

            accumulator ^= unchecked((uint)consumedBytes);
            accumulator ^= accumulator >> 16;
            accumulator *= 0x85ebca6b;
            accumulator ^= accumulator >> 13;
            accumulator *= 0xc2b2ae35;
            accumulator ^= accumulator >> 16;

            return new Hash(accumulator);
        }

        private static uint RotateBitsLeft(uint bytes, byte offset)
            => (bytes << offset) | (bytes >> (32 - offset));
    }
}