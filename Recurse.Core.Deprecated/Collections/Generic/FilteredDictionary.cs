﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Collections.Generic
{
    public static class FilteredDictionary
    {
        public static FilteredDictionary<TKey, TValue> Create<TKey, TValue>(
            IReadOnlyDictionary<TKey, TValue> dictionary,
            Func<KeyValuePair<TKey, TValue>, bool> predicate)
            => new FilteredDictionary<TKey, TValue>(dictionary, predicate);
    }

    [Serializable]
    public class FilteredDictionary<TKey, TValue> : IReadOnlyDictionary<TKey, TValue>
    {
        private readonly IReadOnlyDictionary<TKey, TValue> _dictionary;

        private readonly Func<KeyValuePair<TKey, TValue>, bool> _predicate;

        public TValue this[TKey key]
        {
            get
            {
                var value = _dictionary[key];
                if (!_predicate(new KeyValuePair<TKey, TValue>(key, value)))
                {
                    throw new KeyNotFoundException();
                }
                return value;
            }
        }

        public IEnumerable<TKey> Keys => _dictionary.Where(_predicate).Select(e => e.Key);

        public IEnumerable<TValue> Values => _dictionary.Where(_predicate).Select(e => e.Value);

        public int Count => _dictionary.Count(_predicate);

        public FilteredDictionary(IReadOnlyDictionary<TKey, TValue> dictionary, Func<KeyValuePair<TKey, TValue>, bool> predicate)
        {
            _dictionary = dictionary;
            _predicate = predicate;
        }

        public bool ContainsKey(TKey key)
        {
            return _dictionary.TryGetValue(key, out TValue value) && _predicate(new KeyValuePair<TKey, TValue>(key, value));
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _dictionary.Where(_predicate).GetEnumerator();
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            if (_dictionary.TryGetValue(key, out value))
            {
                if (_predicate(new KeyValuePair<TKey, TValue>(key, value)))
                {
                    return true;
                }
                else
                {
                    value = default(TValue);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
