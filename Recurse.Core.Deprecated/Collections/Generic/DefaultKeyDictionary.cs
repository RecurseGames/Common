﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Recurse.Core.Collections.Generic
{
    /// <summary>
    /// Implementation of <see cref="IDictionary{TKey, TValue}"/> which
    /// supports default and null keys.
    /// </summary>
    /// <remarks>
    /// Generally, this class would not be used when <see cref="TKey"/> is
    /// a reference type, but it may be useful in cases where this isn't known.
    /// </remarks>
    [Serializable]
    public class DefaultKeyDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IReadOnlyDictionary<TKey, TValue>
    {
        private readonly Dictionary<TKey, TValue> _backing = new Dictionary<TKey, TValue>();

        private TValue _defaultKeyValue;

        private bool _defaultKeyExists;

        public TValue this[TKey key]
        {
            get
            {
                if (!Equals(key, default(TKey)))
                {
                    return _backing[key];
                }
                else if (_defaultKeyExists)
                {
                    return _defaultKeyValue;
                }
                else
                {
                    throw new KeyNotFoundException();
                }
            }
            set
            {
                if (!Equals(key, default(TKey)))
                {
                    _backing[key] = value;
                }
                else
                {
                    _defaultKeyExists = true;
                    _defaultKeyValue = value;
                }
            }
        }

        public IEnumerable<TKey> Keys
        {
            get
            {
                if (_defaultKeyExists)
                {
                    yield return default(TKey);
                }

                foreach (var key in _backing.Keys)
                {
                    yield return key;
                }
            }
        }

        public IEnumerable<TValue> Values
        {
            get
            {
                if (_defaultKeyExists)
                {
                    yield return default(TValue);
                }

                foreach (var value in _backing.Values)
                {
                    yield return value;
                }
            }
        }

        ICollection<TKey> IDictionary<TKey, TValue>.Keys
        {
            get
            {
                if (_defaultKeyExists)
                {
                    return new ReadOnlyCollection<TKey>(Keys.ToList());
                }
                else
                {
                    return _backing.Keys;
                }
            }
        }

        ICollection<TValue> IDictionary<TKey, TValue>.Values
        {
            get
            {
                if (_defaultKeyExists)
                {
                    return new ReadOnlyCollection<TValue>(Values.ToList());
                }
                else
                {
                    return _backing.Values;
                }
            }
        }

        public int Count => _defaultKeyExists ? _backing.Count + 1 : _backing.Count;

        public bool IsReadOnly => false;

        public void Add(TKey key, TValue value)
        {
            if (ContainsKey(key))
            {
                throw new ArgumentException("Key already exists.", nameof(key));
            }
            else
            {
                this[key] = value;
            }
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            if (ContainsKey(item.Key))
            {
                throw new ArgumentException("Key already exists.", nameof(item.Key));
            }
            else
            {
                this[item.Key] = item.Value;
            }
        }

        public void Clear()
        {
            _defaultKeyExists = false;
            _defaultKeyValue = default(TValue);
            _backing.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return Equals(item.Key, default(TKey))
                ? _defaultKeyExists && Equals(item.Value, _defaultKeyValue)
                : _backing.Contains(item);
        }

        public bool ContainsKey(TKey key)
        {
            return Equals(key, default(TKey))
                ? _defaultKeyExists
                : _backing.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            this.ToArray().CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            if (_defaultKeyExists)
            {
                yield return new KeyValuePair<TKey, TValue>(default(TKey), _defaultKeyValue);
            }

            foreach (var e in _backing)
            {
                yield return e;
            }
        }

        public bool Remove(TKey key)
        {
            if (!Equals(key, default(TKey)))
            {
                return _backing.Remove(key);
            }
            else if (_defaultKeyExists)
            {
                _defaultKeyExists = false;
                _defaultKeyValue = default(TValue);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            if (Contains(item))
            {
                return Remove(item.Key);
            }
            else
            {
                return false;
            }
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            if (!Equals(key, default(TKey)))
            {
                return _backing.TryGetValue(key, out value);
            }
            else if (_defaultKeyExists)
            {
                value = _defaultKeyValue;
                return true;
            }
            else
            {
                value = default(TValue);
                return false;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
