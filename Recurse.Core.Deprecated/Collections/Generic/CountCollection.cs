﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Collections.Generic
{
    /// <summary>
    /// A collection which counts how many copies of each elements it contains.
    /// </summary>
    public class CountCollection<T> : ICollection<T>
    {
        private readonly CountMapping _counts = new CountMapping();

        public int Count => _counts.Values.Sum();

        public bool IsReadOnly => false;

        public IEnumerable<T> UniqueElements => _counts.Keys;

        public IMapping<T, int> ElementCounts => _counts;

        public void Add(T item)
        {
            ElementCounts[item]++;
        }

        public void Clear()
        {
            _counts.Clear();
        }

        public bool Contains(T item)
        {
            return _counts.ContainsKey(item);
        }

        public bool Remove(T item)
        {
            var newCount = ElementCounts[item] - 1;
            if (newCount < 0)
            {
                return false;
            }

            ElementCounts[item] = newCount;
            return true;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
            else if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(arrayIndex), "Array index cannot be negative.");
            }
            else if (Count > array.Length)
            {
                throw new ArgumentException("Array cannot fit elements.", nameof(array));
            }

            var i = 0;
            foreach (var element in this)
            {
                array[i++] = element;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var entry in _counts)
            {
                for (var i = 0; i < entry.Value; i++)
                {
                    yield return entry.Key;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        [Serializable]
        private class CountMapping : Dictionary<T, int>, IMapping<T, int>
        {
            int IMapping<T, int>.this[T key]
            {
                get
                {
                    int count;
                    return TryGetValue(key, out count) ? count : 0;
                }

                set
                {
                    if (value < 0)
                    {
                        throw new ArgumentOutOfRangeException(nameof(value), "Count of an item cannot be negative.");
                    }
                    else if (value == 0)
                    {
                        Remove(key);
                    }
                    else
                    {
                        base[key] = value;
                    }
                }
            }

            int IReadOnlyMapping<T, int>.this[T key]
            {
                get
                {
                    int count;
                    return TryGetValue(key, out count) ? count : 0;
                }
            }
        }
    }
}
