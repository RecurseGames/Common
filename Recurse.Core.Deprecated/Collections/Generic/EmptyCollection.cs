﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;

namespace Recurse.Core.Collections.Generic
{
    [Serializable]
    public class EmptyCollection<T> : ICollection<T>
    {
        public static ICollection<T> Instance { get; } = new EmptyCollection<T>();

        public int Count => 0;

        public bool IsReadOnly => true;

        public bool Contains(T item) => false;

        public void Add(T item)
        {
            throw CreateWriteNotSupportedException();
        }

        public void Clear()
        {
            throw CreateWriteNotSupportedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
            else if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(arrayIndex), "Array index cannot be less than 0.");
            }
        }

        public bool Remove(T item)
        {
            throw CreateWriteNotSupportedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            yield break;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private static Exception CreateWriteNotSupportedException()
        {
            return new NotSupportedException("Collection is read-only.");
        }
    }
}
