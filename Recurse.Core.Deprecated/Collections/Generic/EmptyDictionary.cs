﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;

namespace Recurse.Core.Collections.Generic
{
    [Serializable]
    public class EmptyDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IReadOnlyDictionary<TKey, TValue>
    {
        public static EmptyDictionary<TKey, TValue> Instance { get; } = new EmptyDictionary<TKey, TValue>();

        public static IReadOnlyDictionary<TKey, TValue> ReadOnlyInstance => Instance;

        IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => Keys;

        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => Values;

        public TValue this[TKey key]
        {
            get
            {
                throw new KeyNotFoundException("The given key was not present in the dictionary.");
            }

            set
            {
                throw CreateWriteNotSupportedException();
            }
        }

        public ICollection<TKey> Keys => EmptyCollection<TKey>.Instance;

        public ICollection<TValue> Values => EmptyCollection<TValue>.Instance;

        public int Count => 0;

        public bool IsReadOnly => true;

        public void Add(TKey key, TValue value)
        {
            throw CreateWriteNotSupportedException();
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            throw CreateWriteNotSupportedException();
        }

        public void Clear()
        {
            throw CreateWriteNotSupportedException();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return false;
        }

        public bool ContainsKey(TKey key)
        {
            return false;
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
            else if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(arrayIndex), "Array index cannot be less than 0.");
            }
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            yield break;
        }

        public bool Remove(TKey key)
        {
            throw CreateWriteNotSupportedException();
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            throw CreateWriteNotSupportedException();
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            value = default(TValue);
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            yield break;
        }

        private static Exception CreateWriteNotSupportedException()
        {
            return new NotSupportedException("Collection is read-only.");
        }
    }
}
