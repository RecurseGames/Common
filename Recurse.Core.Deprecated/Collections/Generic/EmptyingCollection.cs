﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;

namespace Recurse.Core.Collections.Generic
{
    /// <summary>
    /// A collection which immediately removes anything added to it.
    /// </summary>
    [Serializable]
    public class EmptyingCollection<T> : ICollection<T>
    {
        public static EmptyingCollection<T> Instance = new EmptyingCollection<T>();

        public int Count => 0;

        public bool IsReadOnly => false;

        public void Add(T item)
        {

        }

        public void Clear()
        {

        }

        public bool Contains(T item)
        {
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }
            else if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException("arrayIndex");
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            yield break;
        }

        public bool Remove(T item)
        {
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}