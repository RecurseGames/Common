﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Collections.Generic
{
    [Serializable]
    public class WeakCollection<T> : ICollection<T> where T : class
    {
        /// <summary>
        /// References to the items in the set. If the set contains NULL, this
        /// list will contain a NULL value.
        /// </summary>
        private readonly List<WeakReference> _references = new List<WeakReference>();

        /// <summary>
        /// Removes dead references from the set.
        /// </summary>
        /// <remarks>
        /// This should only be called when the set is modified, to avoid concurrent
        /// modification issues.
        /// </remarks>
        private void RemoveDeadReferences()
        {
            _references.RemoveAll(reference => reference != null && !reference.IsAlive);
        }

        #region ICollection Implementation

        public int Count => _references.Count;

        public bool IsReadOnly => false;

        // TODO: Consider rename to AddWeak for clarity
        public virtual void Add(T item)
        {
            RemoveDeadReferences();
            _references.Add(item != null ? new WeakReference(item) : null);
        }

        public void Clear()
        {
            _references.Clear();
        }

        public bool Contains(T item)
        {
            return item != null ?
                _references.Any(reference => reference != null && Equals(reference.Target, item)) :
                _references.Contains(null);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            var nullPlaceholder = new object();
            _references
                .Select(reference => reference == null ? nullPlaceholder : reference.Target)
                .Where(value => value != null)
                .Select(value => value == nullPlaceholder ? null : value)
                .ToArray()
                .CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var reference in _references)
            {
                if (reference == null)
                {
                    yield return null;
                }
                else
                {
                    var item = reference.Target;
                    if (item != null) yield return (T)item;
                }
            }
        }

        public bool Remove(T item)
        {
            if (item == null)
            {
                RemoveDeadReferences();
                return _references.Remove(null);
            }

            foreach (var reference in _references)
            {
                if (reference.Target == item)
                {
                    RemoveDeadReferences();
                    _references.Remove(reference);
                    return true;
                }
            }

            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}