﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;

namespace Recurse.Core.Collections.Generic
{
    public static class ImmutableMultiset
    {
        public static ImmutableMultiset<T> Create<T>(params T[] elements)
        {
            return new ImmutableMultiset<T>(elements);
        }

        public static ImmutableMultiset<T> Create<T>(IEnumerable<T> elements)
        {
            return new ImmutableMultiset<T>(elements);
        }
    }

    [Serializable]
    public class ImmutableMultiset<T> : IEnumerable<T>, IEquatable<ImmutableMultiset<T>>
    {
        private IEnumerable<T> Elements { get; }

        public ImmutableMultiset(params T[] elements)
        {
            Elements = elements;
        }

        public ImmutableMultiset(IEnumerable<T> elements)
        {
            Elements = elements;
        }

        public bool Equals(ImmutableMultiset<T> other)
        {
            return other != null && MultisetComparer.Equals(Elements, other.Elements);
        }

        public sealed override bool Equals(object obj)
        {
            return Equals(obj as ImmutableMultiset<T>);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Elements.GetEnumerator();
        }

        public sealed override int GetHashCode()
        {
            return MultisetComparer.GetHashCode(Elements);
        }

        public override string ToString()
        {
            return "{" + string.Join(", ", Elements) + "}";
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Elements.GetEnumerator();
        }
    }
}
