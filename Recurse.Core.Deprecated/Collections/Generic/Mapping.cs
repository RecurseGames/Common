﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;

namespace Recurse.Core.Collections.Generic
{
    [Serializable]
    public class Mapping<TKey, TValue> : IMapping<TKey, TValue>
    {
        private readonly Dictionary<TKey, TValue> _implementation = new Dictionary<TKey, TValue>();

        private TValue _defaultKeyValue;

        public TValue this[TKey key]
        {
            get
            {
                if (Equals(key, default(TKey)))
                {
                    return _defaultKeyValue;
                }
                else
                {
                    return _implementation.ContainsKey(key) ? _implementation[key] : default(TValue);
                }
            }

            set
            {
                if (Equals(key, default(TKey)))
                {
                    _defaultKeyValue = value;
                }
                else if (Equals(value, default(TValue)))
                {
                    _implementation.Remove(key);
                }
                else
                {
                    _implementation[key] = value;
                }
            }
        }
    }
}
