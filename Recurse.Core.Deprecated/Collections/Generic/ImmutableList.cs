﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Core.Hashing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Collections.Generic
{
    public static class ImmutableList
    {
        public static ImmutableList<T> Create<T>(params T[] elements)
        {
            return new ImmutableList<T>(elements);
        }

        public static ImmutableList<T> Create<T>(IEnumerable<T> elements)
        {
            return elements is ImmutableList<T> list ? list : new ImmutableList<T>(elements);
        }
    }

    [Serializable]
    public struct ImmutableList<T> : IEnumerable<T>, IEquatable<ImmutableList<T>>
    {
        public static ImmutableList<T> Empty { get; } = new ImmutableList<T>();

        private readonly T[] _elements;

        private T[] Elements => _elements ?? new T[0];

        public int Count => Elements.Length;

        public T this[int index] => Elements[index];

        public ImmutableList(params T[] elements)
        {
            _elements = elements.ToArray();
        }

        public ImmutableList(IEnumerable<T> elements)
        {
            _elements = elements.ToArray();
        }

        public ImmutableList<T> AddRange(IEnumerable<T> elements)
        {
            return new ImmutableList<T>(Elements.Concat(elements).ToArray());
        }

        public ImmutableList<T> Add(T element)
        {
            return new ImmutableList<T>(Elements.Append(element).ToArray());
        }

        public ImmutableList<T> GetRange(int index, int count)
        {
            return new ImmutableList<T>(Elements.Skip(index).Take(count).ToArray());
        }

        public bool Equals(ImmutableList<T> other)
        {
            return other.SequenceEqual(this);
        }

        public override bool Equals(object obj)
        {
            return obj is ImmutableList<T> sequence && Equals(sequence);
        }

        public override int GetHashCode()
        {
            return Hash.GetUnstableHashCode(Elements) + 0x2e792d23;
        }

        public override string ToString()
        {
            return "[" + string.Join(", ", Elements) + "]";
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (Elements != null)
            {
                foreach (var element in Elements)
                {
                    yield return element;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            if (Elements != null)
            {
                foreach (var element in Elements)
                {
                    yield return element;
                }
            }
        }
    }
}
