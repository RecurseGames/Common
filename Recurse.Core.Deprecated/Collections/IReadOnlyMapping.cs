﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    /// <summary>
    /// Represents a read-only mapping of keys to values. Unlike <see cref="IReadOnlyDictionary{TKey, TValue}{TKey, TValue}"/>,
    /// there is not necessarily a finite number of keys, and null keys are valid.
    /// </summary>
    public interface IReadOnlyMapping<in TKey, out TValue>
    {
        TValue this[TKey key] { get; }
    }
}
