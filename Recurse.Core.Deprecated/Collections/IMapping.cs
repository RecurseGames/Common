﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    /// <summary>
    /// Represents a mapping of keys to values. Unlike <see cref="IDictionary{TKey, TValue}"/>,
    /// there is not necessarily a finite number of keys, and null keys are valid.
    /// </summary>
    public interface IMapping<in TKey, TValue> : IReadOnlyMapping<TKey, TValue>
    {
        new TValue this[TKey key] { get; set; }
    }
}
