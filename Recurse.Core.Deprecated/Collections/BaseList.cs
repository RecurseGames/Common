﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.ObjectModel;
using System.Linq;

namespace Recurse.Core.Collections
{
    /// <summary>
    /// Wrapper for <see cref="Collection{T}"/> which allows base
    /// classes to respond to changes.
    /// </summary>
    public class BaseList<T> : Collection<T>
    {
        protected override void SetItem(int index, T item)
        {
            var oldItem = this[index];
            if (Equals(oldItem, item)) return;
            base.SetItem(index, item);
            OnItemChanged(index, oldItem, item);
        }

        protected virtual void OnItemChanged(int index, T oldItem, T newItem)
        {
        }

        protected override void InsertItem(int index, T item)
        {
            base.InsertItem(index, item);
            OnItemAdded(index, item);
        }

        protected virtual void OnItemAdded(int index, T item)
        {
        }

        protected override void RemoveItem(int index)
        {
            var item = this[index];
            base.RemoveItem(index);
            OnItemRemoved(index, item);
        }

        protected virtual void OnItemRemoved(int index, T item)
        {
        }

        protected override void ClearItems()
        {
            if (this.Any())
            {
                base.ClearItems();
                OnItemsCleared();
            }
        }

        protected virtual void OnItemsCleared()
        {

        }
    }
}