﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections;
using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    public abstract class BaseSet<TBacking, TElement> : ISet<TElement>, ICollection<TElement>
        where TBacking : ISet<TElement>
    {
        protected abstract TBacking Backing { get; }
        
        public int Count => Backing.Count;

        public bool IsReadOnly => Backing.IsReadOnly;

        public bool Add(TElement item)
        {
            return Backing.Add(item);
        }

        public void Clear()
        {
            Backing.Clear();
        }

        public bool Contains(TElement item)
        {
            return Backing.Contains(item);
        }

        public void CopyTo(TElement[] array, int arrayIndex)
        {
            Backing.CopyTo(array, arrayIndex);
        }

        public void ExceptWith(IEnumerable<TElement> other)
        {
            Backing.ExceptWith(other);
        }

        public IEnumerator<TElement> GetEnumerator()
        {
            return Backing.GetEnumerator();
        }

        public void IntersectWith(IEnumerable<TElement> other)
        {
            Backing.IntersectWith(other);
        }

        public bool IsProperSubsetOf(IEnumerable<TElement> other)
        {
            return Backing.IsProperSubsetOf(other);
        }

        public bool IsProperSupersetOf(IEnumerable<TElement> other)
        {
            return Backing.IsProperSupersetOf(other);
        }

        public bool IsSubsetOf(IEnumerable<TElement> other)
        {
            return Backing.IsSubsetOf(other);
        }

        public bool IsSupersetOf(IEnumerable<TElement> other)
        {
            return Backing.IsSupersetOf(other);
        }

        public bool Overlaps(IEnumerable<TElement> other)
        {
            return Backing.Overlaps(other);
        }

        public bool Remove(TElement item)
        {
            return Backing.Remove(item);
        }

        public bool SetEquals(IEnumerable<TElement> other)
        {
            return Backing.SetEquals(other);
        }

        public void SymmetricExceptWith(IEnumerable<TElement> other)
        {
            Backing.SymmetricExceptWith(other);
        }

        public void UnionWith(IEnumerable<TElement> other)
        {
            Backing.UnionWith(other);
        }

        void ICollection<TElement>.Add(TElement item)
        {
            Backing.Add(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Backing.GetEnumerator();
        }
    }
}
