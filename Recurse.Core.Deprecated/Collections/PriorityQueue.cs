﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Collections
{
    public class PriorityQueue<TPriority, TElement> : IPriorityQueue<TPriority, TElement>
    {
        /// <summary>
        /// Mapping of priorities to the (non-empty) elements with those
        /// priorities, in order added to the queue
        /// </summary>
        private readonly SortedDictionary<TPriority, List<TElement>> _contents;

        public bool IsEmpty => !_contents.Any();

        public TPriority HighestPriority
        {
            get
            {
                if (!_contents.Keys.Any())
                    throw new InvalidOperationException("Queue contains no elements");
                return _contents.Keys.Last();
            }
        }

        public PriorityQueue()
        {
            _contents = new SortedDictionary<TPriority, List<TElement>>();
        }

        public PriorityQueue(IComparer<TPriority> comparer)
        {
            _contents = new SortedDictionary<TPriority, List<TElement>>(comparer);
        }

        public void Clear()
        {
            _contents.Clear();
        }

        public void Insert(TPriority priority, TElement element)
        {
            if (!_contents.ContainsKey(priority))
                _contents[priority] = new List<TElement>();
            _contents[priority].Add(element);
        }

        public TElement Peek()
        {
            return _contents[HighestPriority][0];
        }

        public TElement Pull()
        {
            // Get the first-in highest priority element
            var queue = _contents[HighestPriority];
            var element = queue[0];

            // Remove the element
            queue.RemoveAt(0);
            if (!queue.Any())
                _contents.Remove(HighestPriority);

            return element;
        }

        public bool Remove(TPriority priority, TElement element)
        {
            // If we don't have anything at the given priority,
            // there is no element to remove
            if (!_contents.ContainsKey(priority))
                return false;

            // Try to remove the element at the given priority,
            // returning if it is not found
            var queue = _contents[priority];
            if (!queue.Remove(element))
                return false;

            // Having removed the element, if nothing has the
            // same priority, we no longer need a list to store
            // such elements
            if (!queue.Any())
                _contents.Remove(priority);

            // Having removed the element, return
            return true;
        }
    }
}