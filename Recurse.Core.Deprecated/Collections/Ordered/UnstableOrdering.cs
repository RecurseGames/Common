﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Collections.Ordered
{
    [Serializable]
    public class UnstableOrdering
    {
        public static UnstableOrdering Instance { get; } = new UnstableOrdering();

        private UnstableOrdering()
        {

        }

        public IEnumerable<T> Merge<T>(IEnumerable<T> enumerable, Func<T, T, T> tryMerge) where T : class
        {
            var elements = enumerable.ToList();
            var merge = true;
            while (merge)
            {
                merge = false;
                for (var i = 0; i < elements.Count; i++)
                {
                    var j = i + 1;
                    while (j < elements.Count)
                    {
                        var merged = tryMerge(elements[i], elements[j]);
                        if (merged != null)
                        {
                            merge = true;
                            elements[i] = merged;
                            elements.RemoveAt(j);
                        }
                        else
                        {
                            j++;
                        }
                    }
                }
            }
            return elements;
        }

        public IEqualityComparer<IEnumerable<T>> GetComparer<T>() => MultisetComparer<T>.Instance;

        public override string ToString() => nameof(UnstableOrdering);
    }
}
