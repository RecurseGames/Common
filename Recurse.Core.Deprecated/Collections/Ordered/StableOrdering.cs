﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Collections.Ordered
{
    [Serializable]
    public class StableOrdering
    {
        public static StableOrdering Instance { get; } = new StableOrdering();

        private StableOrdering()
        {

        }

        public IEnumerable<T> Merge<T>(IEnumerable<T> elements, Func<T, T, T> tryMerge) where T : class
        {
            var result = new List<T>(elements);
            var i = 0;
            while (i < result.Count - 1)
            {
                var current = result[i];
                var next = result[i + 1];
                var merged = tryMerge(current, next);
                if (merged != null)
                {
                    result[i] = merged;
                    result.RemoveAt(i + 1);
                    if (i > 0)
                    {
                        i--;
                    }
                }
                else
                {
                    i++;
                }
            }
            return result;
        }

        public IEqualityComparer<IEnumerable<T>> GetComparer<T>() => Comparer<T>.Instance;

        public override string ToString() => nameof(StableOrdering);

        public class Comparer<T> : IEqualityComparer<IEnumerable<T>>
        {
            public static Comparer<T> Instance { get; } = new Comparer<T>();

            public bool Equals(IEnumerable<T> x, IEnumerable<T> y)
            {
                if (x == null)
                    return y == null;
                if (y == null)
                    return x == null;
                return x.SequenceEqual(y);
            }

            public int GetHashCode(IEnumerable<T> obj)
            {
                var hashcode = 0x27d72d96;
                if (obj != null)
                {
                    hashcode += obj.Take(2)
                        .Select(e => e != null ? e.GetHashCode() : unchecked((int)0xc4f86f36))
                        .Aggregate((a, b) => unchecked(a + b));
                }
                return hashcode;
            }
        }
    }
}
