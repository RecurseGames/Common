﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    /// <summary>
    /// A comparer which is the opposite of another converter
    /// </summary>
    public class ReverseComparer<T> : IComparer<T>
    {
        public static ReverseComparer<T> Default { get; } = new ReverseComparer<T>(Comparer<T>.Default);

        private readonly IComparer<T> _original;

        public ReverseComparer(IComparer<T> original)
        {
            _original = original;
        }

        public int Compare(T x, T y)
        {
            return -_original.Compare(x, y);
        }
    }
}