﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Recurse.Core.Collections
{
    /// <summary>
    /// An interface for a priority queue
    /// </summary>
    public interface IPriorityQueue<TPriority, TElement>
    {
        /// <summary>
        /// Gets whether the queue contains any elements
        /// </summary>
        bool IsEmpty { get; }

        /// <summary>
        /// Returns the highest priority of all elements,
        /// or throws an InvalidOperationException if there
        /// are no elements
        /// </summary>
        TPriority HighestPriority { get; }

        /// <summary>
        /// Removes and returns the highest priority element,
        /// or throws an InvalidOperationException if there is
        /// no such element
        /// </summary>
        TElement Pull();

        /// <summary>
        /// Tturns the highest priority element,
        /// or throws an InvalidOperationException if there is
        /// no such element
        /// </summary>
        TElement Peek();

        /// <summary>
        /// Adds an element to the queue with the given priority
        /// </summary>
        void Insert(TPriority priority, TElement element);

        /// <summary>
        /// Removes an element from the queue with the given priority,
        /// if any. Returns TRUE if an element was removed
        /// </summary>
        bool Remove(TPriority priority, TElement element);

        /// <summary>
        /// Removes all elements from the queue
        /// </summary>
        void Clear();
    }
}
