﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Collections
{
    public static class MultisetComparer
    {
        public static bool Equals<T>(IEnumerable<T> x, IEnumerable<T> y)
        {
            if (x == null)
                return y == null;
            if (y == null)
                return x == null;

            x = x.OrderBy(i => i?.GetHashCode());
            y = y.OrderBy(i => i?.GetHashCode());

            return x.SequenceEqual(y);
        }

        public static int GetHashCode<T>(IEnumerable<T> obj)
        {
            var hashcode = 0x27d72d96;
            if (obj != null)
            {
                hashcode += obj.Take(2)
                    .Select(e => e != null ? e.GetHashCode() : unchecked((int)0xc4f86f36))
                    .DefaultIfEmpty()
                    .Aggregate((a, b) => unchecked(a + b));
            }
            return hashcode;
        }
    }

    [Serializable]
    public class MultisetComparer<T> : IEqualityComparer<IEnumerable<T>>
    {
        public static MultisetComparer<T> Instance { get; } = new MultisetComparer<T>();

        public bool Equals(IEnumerable<T> x, IEnumerable<T> y) => MultisetComparer.Equals(x, y);

        public int GetHashCode(IEnumerable<T> obj) => MultisetComparer.GetHashCode(obj);

        public override bool Equals(object obj) => obj is MultisetComparer<T>;

        public override int GetHashCode() => 0x4b722e52;
    }
}
