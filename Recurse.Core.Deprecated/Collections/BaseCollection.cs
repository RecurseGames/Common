﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    /// <summary>
    /// Wrapper for <see cref="ICollection{T}"/> which allows base
    /// classes to respond to changes.
    /// </summary>
    [Serializable]
    public class BaseCollection<T> : ICollection<T>
    {
        private readonly ICollection<T> _implementation;

        public int Count => _implementation.Count;

        public bool IsReadOnly => _implementation.IsReadOnly;

        public BaseCollection(ICollection<T> implementation)
        {
            _implementation = implementation;
        }

        public virtual void Add(T item)
        {
            _implementation.Add(item);
        }

        public virtual void Clear()
        {
            _implementation.Clear();
        }

        public bool Contains(T item)
        {
            return _implementation.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _implementation.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _implementation.GetEnumerator();
        }

        public virtual bool Remove(T item)
        {
            return _implementation.Remove(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _implementation.GetEnumerator();
        }
    }
}
