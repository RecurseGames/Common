﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Recurse.Core.Utils
{
    /// <summary>
    /// A unique key. It is equal to all other keys with
    /// the same value and type.
    /// </summary>
    [Serializable]
    public class Key<T1> : IEquatable<Key<T1>>
    {
        protected T1 Value { get; }

        public Key(T1 value)
        {
            Value = value;
        }

        public bool Equals(Key<T1> other)
            => other != null && GetType().Equals(other.GetType()) && Equals(Value, other.Value);

        public override bool Equals(object obj)
            => Equals(obj as Key<T1>);

        public override int GetHashCode()
            => GetType().GetHashCode() + (Value != null ? Value.GetHashCode() : 0);

        public override string ToString()
            => Value != null ? Value.ToString() : "null";
    }

    /// <summary>
    /// A unique key. It is equal to all other keys with
    /// the same values and type.
    /// </summary>
    [Serializable]
    public class Key<T1, T2> : IEquatable<Key<T1, T2>>
    {
        private T1 Value1 { get; }

        private T2 Value2 { get; }

        public Key(T1 value1, T2 value2)
        {
            Value1 = value1;
            Value2 = value2;
        }

        public bool Equals(Key<T1, T2> other) =>
            other != null && 
            GetType().Equals(other.GetType()) &&
            Equals(Value1, other.Value1) &&
            Equals(Value2, other.Value2);

        public override bool Equals(object obj)
            => Equals(obj as Key<T1, T2>);

        public override int GetHashCode() => 
            GetType().GetHashCode() +
            (Value1 != null ? Value1.GetHashCode() : 0) +
            0x1eb26bbe * (Value2 != null ? Value2.GetHashCode() : 0);

        public override string ToString() => string.Format(
            "{0} {1}",
            Value1 != null ? Value1.ToString() : "null",
            Value2 != null ? Value2.ToString() : "null");
    }
}
