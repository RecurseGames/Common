﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Persist
{
    public class ArrayIndexEnumerable : IEnumerable<int[]>
    {
        private readonly Array _array;

        public ArrayIndexEnumerable(Array array)
        {
            _array = array;
        }

        public IEnumerator<int[]> GetEnumerator()
        {
            if (_array.Length == 0)
            {
                yield break;
            }

            var indices = new int[_array.Rank];
            while (true)
            {
                yield return indices.ToArray();

                var incrementableDimension = Enumerable
                    .Range(0, _array.Rank)
                    .Where(dimension => indices[dimension] + 1 < _array.GetLength(dimension))
                    .DefaultIfEmpty(-1)
                    .Last();
                if (incrementableDimension == -1)
                {
                    yield break;
                }

                indices[incrementableDimension]++;
                foreach (var resetDimension in Enumerable.Range(incrementableDimension + 1, _array.Rank - incrementableDimension - 1))
                {
                    indices[resetDimension] = 0;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
