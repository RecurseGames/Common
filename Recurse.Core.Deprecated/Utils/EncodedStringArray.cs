﻿using Recurse.Core.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Persist
{
    [Serializable]
    public class EncodedStringArray
    {
        private static readonly StringEncoding ElementEncoding 
            = new StringEncoding('\\').AddEscape(ElementDelimiter, 'p');

        private const char ElementDelimiter = '|';

        private const string EncodedNullArray = "\\n";

        private const string EncodedEmptyArray = "\\e";

        private const string EncodedNullElement = "\\0";

        private string _encodedElements;

        public IList<string> Elements { get; }

        public string EncodedElements
        {
            get { return _encodedElements ?? (_encodedElements = Encode(Elements.ToArray())); }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                else if (_encodedElements == null || !Equals(_encodedElements, value))
                {
                    Elements.Clear();
                    foreach (var element in Decode(value))
                    {
                        Elements.Add(element);
                    }
                    _encodedElements = value;
                }
            }
        }

        public EncodedStringArray()
        {
            Elements = new ElementList(this);
        }

        public static string Encode(params string[] elements)
        {
            if (elements == null)
            {
                return EncodedNullArray;
            }
            else if (elements.Length == 0)
            {
                return EncodedEmptyArray;
            }
            else
            {
                return string.Join("|", elements.Select(EncodeElement));
            }
        }

        public static string[] Decode(string encodedElements)
        {
            if (encodedElements == null)
            {
                throw new ArgumentNullException(nameof(encodedElements));
            }
            else if (Equals(encodedElements, EncodedNullArray))
            {
                return null;
            }
            else if (Equals(encodedElements, EncodedEmptyArray))
            {
                return Array.Empty<string>();
            }
            else
            {
                return encodedElements.Split(ElementDelimiter).Select(DecodeElement).ToArray();
            }
        }

        private static string DecodeElement(string encodedElement)
        {
            if (encodedElement.Equals(EncodedNullElement))
            {
                return null;
            }
            else
            {
                return ElementEncoding.Decode(encodedElement);
            }
        }

        private static string EncodeElement(string element)
        {
            if (element == null)
            {
                return EncodedNullElement;
            }
            else
            {
                return ElementEncoding.Encode(element);
            }
        }

        private void InvalidateCachedEncodedElements()
        {
            _encodedElements = null;
        }

        [Serializable]
        private class ElementList : BaseList<string>
        {
            private readonly EncodedStringArray _owner;

            public ElementList(EncodedStringArray owner)
            {
                _owner = owner;
            }

            protected override void OnItemAdded(int index, string item)
            {
                base.OnItemAdded(index, item);

                _owner.InvalidateCachedEncodedElements();
            }

            protected override void OnItemChanged(int index, string oldItem, string newItem)
            {
                base.OnItemChanged(index, oldItem, newItem);

                _owner.InvalidateCachedEncodedElements();
            }

            protected override void OnItemRemoved(int index, string item)
            {
                base.OnItemRemoved(index, item);

                _owner.InvalidateCachedEncodedElements();
            }

            protected override void OnItemsCleared()
            {
                base.OnItemsCleared();

                _owner.InvalidateCachedEncodedElements();
            }
        }
    }
}
