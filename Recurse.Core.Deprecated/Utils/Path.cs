﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;

namespace Recurse.Core.Utils
{
    public class Path
    {
        public static IEnumerable<T> Find<T>(
            T origin,
            Func<T, bool> endCondition,
            Func<T, IEnumerable<T>> getChildren)
        {
            if (endCondition(origin))
            {
                yield return origin;
            }
            else
            {
                var children = getChildren(origin);
                if (children != null)
                {
                    foreach (var child in children)
                    {
                        var pathFound = false;
                        foreach (var node in Find(child, endCondition, getChildren))
                        {
                            if (!pathFound)
                            {
                                yield return origin;
                                pathFound = true;
                            }
                            yield return node;
                        }
                        if (pathFound) yield break;
                    }
                }
            }
        }
    }
}
