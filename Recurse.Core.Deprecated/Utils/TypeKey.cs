﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Recurse.Core.Utils
{
    [Serializable]
    public abstract class TypeKey : IEquatable<TypeKey>
    {
        public bool Equals(TypeKey other)
        {
            return other != null && Equals(GetType(), other.GetType());
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TypeKey);
        }

        public override int GetHashCode()
        {
            return 0x5999fa1b + GetType().GetHashCode();
        }

        public override string ToString()
        {
            return GetType().Name;
        }
    }

    [Serializable]
    public sealed class TypeKey<T> : TypeKey
    {
        public static TypeKey Instance { get; } = new TypeKey<T>();

        private TypeKey()
        {

        }

        public override string ToString()
        {
            return typeof(T).Name;
        }
    }
}
