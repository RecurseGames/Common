﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Recurse.Core.Utils
{
    public abstract class Equatable
    {
        protected abstract object Identity { get; }

        public sealed override bool Equals(object obj)
        {
            return obj is Equatable other && Equals(GetType(), other.GetType()) && Equals(Identity, other.Identity);
        }

        public sealed override int GetHashCode()
        {
            return 0x20ec4182 + Identity.GetHashCode() + GetType().GetHashCode();
        }

        public override string ToString()
        {
            return $"{GetType().Name}({Identity})";
        }
    }

#warning Consider replace with records in C#9
    public abstract class Equatable<T> : Equatable, IEquatable<T> where T : class
    {
        public bool Equals(T obj)
        {
            var equatable = obj as Equatable<T>;
            return equatable != null && Equals(GetType(), equatable.GetType()) && Equals(Identity, equatable.Identity);
        }
    }
}
