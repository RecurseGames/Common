﻿using System;
using System.Collections.Generic;

namespace Recurse.Persist
{
    [Serializable]
    public class StringEncoding
    {
        private readonly Dictionary<char, string> _escapedCharacters = new Dictionary<char, string>();

        private readonly Dictionary<char, char> _escapeCharacters = new Dictionary<char, char>();

        public char EscapeCharacter { get; }

        public StringEncoding(char escapeCharacter)
        {
            EscapeCharacter = escapeCharacter;

            AddEscape(escapeCharacter, escapeCharacter);
        }

        public StringEncoding AddEscape(char escapedCharacter, char escapeCharacterSuffix)
        {
            if (_escapeCharacters.ContainsKey(escapedCharacter))
                throw new InvalidOperationException("Escape character suffix already defined.");
            if (_escapedCharacters.ContainsKey(escapeCharacterSuffix))
                throw new InvalidOperationException("Escape character suffix already assigned.");

            _escapeCharacters[escapeCharacterSuffix] = escapedCharacter;
            _escapedCharacters[escapedCharacter] = new string(new[] { EscapeCharacter, escapeCharacterSuffix });

            return this;
        }

        public string Encode(string decodedString)
        {
            var result = string.Empty;
            foreach (var c in decodedString)
            {
                string escapedCharacter;
                if (_escapedCharacters.TryGetValue(c, out escapedCharacter))
                {
                    result += escapedCharacter;
                }
                else
                {
                    result += c;
                }
            }
            return result;
        }

        public string Decode(string encodedString)
        {
            var result = string.Empty;
            var isEscaped = false;
            foreach (var c in encodedString)
            {
                if (isEscaped)
                {
                    char decodedCharacter;
                    if (!_escapeCharacters.TryGetValue(c, out decodedCharacter))
                    {
                        throw new InvalidOperationException($"Unrecognised escape sequence {EscapeCharacter}{c}");
                    }

                    result += decodedCharacter;
                    isEscaped = false;
                }
                else if (c == EscapeCharacter)
                {
                    isEscaped = true;
                }
                else
                {
                    result += c;
                }
            }
            return result;
        }
    }
}
