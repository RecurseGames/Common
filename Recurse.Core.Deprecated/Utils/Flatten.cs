﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;

namespace Recurse.Core.Utils
{
    public static class Flatten
    {
        public static IEnumerable<T> Execute<T>(IEnumerable<T> enumerable)
        {
            foreach (var item in enumerable)
            {
                var subitems = item as IEnumerable<T>;
                if (subitems != null)
                {
                    foreach (var subitem in Execute(subitems))
                    {
                        yield return subitem;
                    }
                }
                else
                {
                    yield return item;
                }
            }
        }
    }
}
