﻿///*
// * Copyright (c) 2017 Ben Lambell.
// * 
// * This Source Code Form is subject to the terms of the Mozilla Public
// * License, v. 2.0. If a copy of the MPL was not distributed with this
// * file, You can obtain one at http://mozilla.org/MPL/2.0/.
// */

//using System.Collections;
//using System.Collections.Generic;
//using Recurse.Core.Maths.Space.Points;

//namespace Recurse.Core.Maths.Space.Areas
//{
//    public abstract class PointArray : IEnumerable<XYd>
//    {
//        public abstract XYd this[int i] { get; }

//        public abstract int Length { get; }

//        public abstract IEnumerator<XYd> GetEnumerator();

//        IEnumerator IEnumerable.GetEnumerator()
//        {
//            return GetEnumerator();
//        }

//        public static PointArray operator +(PointArray array, XYd offset)
//        {
//            return new Offset(array, offset);
//        }

//        public static PointArray operator -(PointArray array, XYd offset)
//        {
//            return new Offset(array, -offset);
//        }

//        private class Offset : PointArray
//        {
//            private readonly PointArray _wrapped;

//            private readonly XYd _offset;

//            public override XYd this[int i] => _wrapped[i] + _offset;

//            public override int Length => _wrapped.Length;

//            public Offset(PointArray wrapped, XYd offset)
//            {
//                _wrapped = wrapped;
//                _offset = offset;
//            }

//            public override IEnumerator<XYd> GetEnumerator()
//            {
//                foreach (var wrappedPoint in _wrapped)
//                {
//                    yield return wrappedPoint + _offset;
//                }
//            }
//        }
//    }
//}
