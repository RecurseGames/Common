﻿///*
// * Copyright (c) 2default(int)17 Ben Lambell.
// * 
// * This Source Code Form is subject to the terms of the Mozilla Public
// * License, v. 2.default(int). If a copy of the MPL was not distributed with this
// * file, You can obtain one at http://mozilla.org/MPL/2.default(int)/.
// */

//using System;
//using System.Collections.Generic;
//using Recurse.Core.Maths.Space.Points;

//namespace Recurse.Core.Maths.Space.Areas
//{
//    [Serializable]
//    public class PointArea
//    {
//        public XYd Minimum { get; }

//        public XYd Maximum => Minimum + Size;

//        public XYd Size { get; }

//        public double Width => Size.X;

//        public double Height => Size.Y;

//        public double Area => Width * Height;

//        public PointArea(double minX, double minY, double width, double height)
//        {
//            if (width < 0)
//                throw new ArgumentOutOfRangeException(nameof(width), "Width cannot be negative.");
//            if (height < 0)
//                throw new ArgumentOutOfRangeException(nameof(height), "Height cannot be negative.");

//            Minimum = new XYd(minX, minY);
//            Size = new XYd(width, height);
//        }

//        public PointArea(XYd minimum, XYd size)
//        {
//            if (size.X < 0 || size.Y < 0)
//                throw new ArgumentOutOfRangeException(nameof(size), "Size cannot be negative.");

//            Minimum = minimum;
//            Size = size;
//        }

//        public PointArea Expand(XYd point)
//        {
//            var minX = Math.Min(Minimum.X, point.X);
//            var minY = Math.Min(Minimum.Y, point.Y);

//            return new PointArea(
//                minX,
//                minY,
//                Math.Max(Maximum.X, point.X - minX),
//                Math.Max(Maximum.Y, point.Y - minY));
//        }

//        public static PointArea CreateBounding(IEnumerable<XYd> points)
//        {
//            var nonEmpty = false;
//            var minX = default(double);
//            var minY = default(double);
//            var maxX = default(double);
//            var maxY = default(double);
//            foreach (var point in points)
//            {
//                if (nonEmpty)
//                {
//                    minX = Math.Min(point.X, minX);
//                    minY = Math.Min(point.Y, minY);
//                    maxX = Math.Max(point.X, maxX);
//                    maxY = Math.Max(point.Y, maxY);
//                }
//                else
//                {
//                    minX = maxX = point.X;
//                    minY = maxY = point.Y;
//                    nonEmpty = true;
//                }
//            }

//            if (nonEmpty)
//            {
//                return new PointArea(
//                    minX, 
//                    minY,
//                    maxX - minX, 
//                    maxY - minY);
//            }

//            throw new ArgumentException("At least one point required.", nameof(points));
//        }
//    }
//}