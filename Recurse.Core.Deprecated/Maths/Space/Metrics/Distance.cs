﻿///*
// * Copyright (c) 2017 Ben Lambell.
// * 
// * This Source Code Form is subject to the terms of the Mozilla Public
// * License, v. 2.0. If a copy of the MPL was not distributed with this
// * file, You can obtain one at http://mozilla.org/MPL/2.0/.
// */

//using System;
//using System.Linq;

//namespace Recurse.Core.Maths.Space.Metrics
//{
//    /// <summary>
//    /// Contains a set of metrics for measuring distance
//    /// </summary>
//    public static class Distance
//    {
//        public static readonly Metric Euclidean = new Metric(coords => Math.Sqrt(coords.Sum(coord => Math.Pow(coord, 2))));

//        public static readonly Metric L0 = new Metric(coords => coords.Select(Math.Abs).Min());

//        /// <summary>
//        /// Measures the distance between the two coordinate values?
//        /// </summary>
//        public static readonly Metric Difference = new Metric(coords => 
//        {
//            var pointArray = coords as double[] ?? coords.ToArray();
//            if (pointArray.Length != 2) throw new NotSupportedException("Point must have exactly 2 coordinates.");
//            return Math.Abs(Math.Abs(pointArray.First()) - Math.Abs(pointArray.Last()));
//        });

//        public static readonly Metric Chebyshev = new Metric(coords => coords.Select(Math.Abs).Max());

//        /// <summary>
//        /// Measures the cumulative Chebyshev distance from the origin point,
//        /// which counts 1, 3, 6... rather than 1, 2, 3...
//        /// </summary>
//        public static readonly Metric CumulativeChebyshev = new Metric(coords =>
//        {
//            double chebyshev = Chebyshev.Measure(coords);
//            return chebyshev * (chebyshev + 1) / 2;
//        });

//        public static readonly Metric Manhattan = new Metric(coords => coords.Select(Math.Abs).Sum());

//        /// <summary>
//        /// Measures number of moves for a chess queen to reach the
//        /// origin point, assuming an infinite and unobstructed board
//        /// </summary>
//        public static readonly Metric Queen = new Metric(
//            coords => coords
//            .Where(coord => coord != 0)
//            .Select(Math.Abs)
//            .Distinct()
//            .Count());
//    }
//}