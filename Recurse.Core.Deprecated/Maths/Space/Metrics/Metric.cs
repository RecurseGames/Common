﻿///*
// * Copyright (c) 2017 Ben Lambell.
// * 
// * This Source Code Form is subject to the terms of the Mozilla Public
// * License, v. 2.0. If a copy of the MPL was not distributed with this
// * file, You can obtain one at http://mozilla.org/MPL/2.0/.
// */

//using System;
//using System.Collections.Generic;
//using Recurse.Core.Maths.Space.Points;

//namespace Recurse.Core.Maths.Space.Metrics
//{
//    /// <summary>
//    /// Interface for a metric used to measure a point,
//    /// such as Euclidean Distance.
//    /// </summary>
//    public class Metric
//	{
//        private readonly Func<IEnumerable<double>, double> _measure;

//        public Metric(Func<IEnumerable<double>, double> measure)
//        {
//            _measure = measure;
//        }

//        /// <summary>
//        /// Measures the given point.
//        /// </summary>
//        public double Measure(IEnumerable<double> point)
//        {
//            return _measure(point);
//        }

//        /// <summary>
//        /// Measures the given point.
//        /// </summary>
//        public double Measure(XYd point)
//        {
//            return Measure(new [] { point.X, point.Y });
//        }

//        /// <summary>
//        /// Measures the given point, given an origin point.
//        /// </summary>
//        public double Measure(XYd a, XYd b)
//        {
//            return Measure(a - b);
//        }

//        /// <summary>
//        /// Returns how much closer the given offset is to the given
//        /// destination compared to the origin point.
//        /// </summary>
//        public double MeasureApproach(XYd offset, XYd destination)
//        {
//            return Measure(destination) - Measure(destination - offset);
//        }
//    }
//}