﻿///*
// * Copyright (c) 2017 Ben Lambell.
// * 
// * This Source Code Form is subject to the terms of the Mozilla Public
// * License, v. 2.0. If a copy of the MPL was not distributed with this
// * file, You can obtain one at http://mozilla.org/MPL/2.0/.
// */

//using System.Collections.Generic;
//using Recurse.Core.Maths.Space.Points;

//namespace Recurse.Core.Maths.Space.Direction
//{
//    /// <summary>
//    /// Maps cartesian directions (where [0,1] is North) to visual directions.
//    /// Useful when dealing with screen coordinates, where [0,1] is typically downwards.
//    /// </summary>
//    public class Orientation
//    {
//        public IEnumerable<XY> Directions => new[] { Top, TopLeft, Left, BottomLeft, Bottom, BottomRight, Right, TopRight };

//        public readonly XY Top, Bottom, Left, Right, TopLeft, BottomLeft, TopRight, BottomRight;

//        public static readonly Orientation Cartesian = new Orientation(true, false, false);

//        public static readonly Orientation Screen = new Orientation(true, true, false);

//        private Orientation(bool yIsVerticalAxis, bool invertY, bool invertX)
//        {
//            Top = Transform(Direction.Directions.N, yIsVerticalAxis, invertY, invertX);
//            Bottom = Transform(Direction.Directions.S, yIsVerticalAxis, invertY, invertX);
//            Left = Transform(Direction.Directions.W, yIsVerticalAxis, invertY, invertX);
//            Right = Transform(Direction.Directions.E, yIsVerticalAxis, invertY, invertX);
//            TopLeft = Transform(Direction.Directions.NW, yIsVerticalAxis, invertY, invertX);
//            TopRight = Transform(Direction.Directions.NE, yIsVerticalAxis, invertY, invertX);
//            BottomLeft = Transform(Direction.Directions.SW, yIsVerticalAxis, invertY, invertX);
//            BottomRight = Transform(Direction.Directions.SE, yIsVerticalAxis, invertY, invertX);
//        }

//        private static XY Transform(XY direction, bool yIsVerticalAxis, bool invertY, bool invertX)
//        {
//            return new XY(
//                yIsVerticalAxis ? Transform(direction.X, invertX) : Transform(direction.Y, invertY),
//                yIsVerticalAxis ? Transform(direction.Y, invertY) : Transform(direction.X, invertX));
//        }

//        private static int Transform(int value, bool invert)
//        {
//            return invert ? -value : value;
//        }
//    }
//}