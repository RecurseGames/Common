﻿///*
// * Copyright (c) 2017 Ben Lambell.
// * 
// * This Source Code Form is subject to the terms of the Mozilla Public
// * License, v. 2.0. If a copy of the MPL was not distributed with this
// * file, You can obtain one at http://mozilla.org/MPL/2.0/.
// */

//using Recurse.Core.Maths.Space.Points;

//namespace Recurse.Core.Maths.Space.Direction
//{
//    /// <summary>
//    /// Determines the relative chance of taking the given step towards
//    /// the given destination.
//    /// </summary>
//    public delegate double ApproachStrategy(XYd step, XYd direction);
//}