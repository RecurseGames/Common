﻿///*
// * Copyright (c) 2017 Ben Lambell.
// * 
// * This Source Code Form is subject to the terms of the Mozilla Public
// * License, v. 2.0. If a copy of the MPL was not distributed with this
// * file, You can obtain one at http://mozilla.org/MPL/2.0/.
// */

//using System;
//using System.Collections.Generic;
//using Recurse.Core.Maths.Randomness;
//using Recurse.Core.Maths.Space.Metrics;
//using Recurse.Core.Maths.Space.Points;

//namespace Recurse.Core.Maths.Space.Direction
//{
//    /// <summary>
//    /// Determines what direction we step towards a target
//    /// </summary>
//    public class Approach
//    {
//        #region Backing Fields

//        private IEnumerable<XY> _range;

//        private ApproachStrategy _strategy;

//        #endregion

//        #region Strategies

//        public static readonly ApproachStrategy Weighted = (step, direction) => {
//            var distanceCovered = Distance.Euclidean.MeasureApproach(step, direction);
//            return distanceCovered > 0 ? distanceCovered * distanceCovered : 0;
//        };

//        public static readonly ApproachStrategy EuclideanWander = (step, direction) => Distance.Euclidean.Measure(step);

//        #endregion

//        public IEnumerable<XY> Range
//        {
//            get { return _range; }
//            set
//            {
//                if (value == null)
//                    throw new ArgumentNullException();
//                _range = value;
//            }
//        }

//        public ApproachStrategy Strategy
//        {
//            get { return _strategy; }
//            set
//            {
//                if (value == null)
//                    throw new ArgumentNullException();
//                _strategy = value;
//            }
//        }

//        public Approach() : this((step, direction) => 1)
//        {
//        }

//        public Approach(Metric distanceMetric, IEnumerable<XY> range = null)
//            : this((step, direction) => distanceMetric.MeasureApproach(step, direction))
//        {
//        }

//        public Approach(ApproachStrategy strategy, IEnumerable<XY> range = null)
//        {
//            Strategy = strategy;
//            Range = range ?? Directions.Octal;
//        }

//        public WeightedOutcomes<XY> GetOutcomes(XY origin, XY destination)
//        {
//            return GetOutcomes(destination - origin);
//        }

//        public WeightedOutcomes<XY> GetOutcomes(XY destination)
//        {
//            var result = new WeightedOutcomes<XY>();
//            foreach (var step in Range)
//            {
//                result.Add(step, Strategy(step, destination));
//            }
//            return result;
//        }
//    }
//}