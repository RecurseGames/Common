﻿///*
// * Copyright (c) 2017 Ben Lambell.
// * 
// * This Source Code Form is subject to the terms of the Mozilla Public
// * License, v. 2.0. If a copy of the MPL was not distributed with this
// * file, You can obtain one at http://mozilla.org/MPL/2.0/.
// */

//using Recurse.Core.Maths.Space.Points;

//namespace Recurse.Core.Maths.Space.Direction
//{
//    /// <summary>
//    /// Represents a direction, as an offset from the origin point
//    /// </summary>
//    public static class Directions
//    {
//        public static readonly XY E = new XY(1, 0);
//        public static readonly XY NE = new XY(1, 1);
//        public static readonly XY N = new XY(0, 1);
//        public static readonly XY NW = new XY(-1, 1);
//        public static readonly XY W = new XY(-1, 0);
//        public static readonly XY SW = new XY(-1, -1);
//        public static readonly XY S = new XY(0, -1);
//        public static readonly XY SE = new XY(1, -1);

//        /// <summary>
//        /// All points orthogonally adjacent to the origin on a cartesian plane
//        /// </summary>
//        public static readonly XY[] Orthogonal = new[] { E, N, W, S };

//        /// <summary>
//        /// All points diagonally adjacent to the origin on a cartesian plane
//        /// </summary>
//        public static readonly XY[] Diagonal = new[] { NE, NW, SW, SE };

//        /// <summary>
//        /// All points diagonally and orthogonally adjacent to the origin on a cartesian plane
//        /// </summary>
//        public static readonly XY[] Octal = new[] { E, NE, N, NW, W, SW, S, SE };

//        /// <summary>
//        /// All points horizontally adjacent to the origin on a cartesian plane
//        /// </summary>
//        public static readonly XY[] Horizontal = new[] { E, W };

//        /// <summary>
//        /// All points vertically adjacent to the origin on a cartesian plane
//        /// </summary>
//        public static readonly XY[] Vertical = new[] { N, S };
//    }
//}