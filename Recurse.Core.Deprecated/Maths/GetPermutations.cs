﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Maths
{
    public static class GetPermutations
    {
        public static IEnumerable<T[]> Execute<T>(params T[] items) => Execute(0, items);

        private static IEnumerable<T[]> Execute<T>(int startIndex, params T[] items)
        {
            if (startIndex < items.Length - 1)
            {
                for (var i = startIndex; i < items.Length; i++)
                {
                    var permutation = items.ToArray();
                    permutation[startIndex] = permutation[i];
                    permutation[i] = items[startIndex];

                    foreach (var ordering in Execute(startIndex + 1, permutation))
                    {
                        yield return ordering;
                    }
                }
            }
            else
            {
                yield return items;
            }
        }
    }
}
