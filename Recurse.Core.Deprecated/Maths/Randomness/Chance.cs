﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * <#@ template language="C#" #>
<#@ output extension=".tt.cs" #>
<#
    string Namespace = "Recurse.Core.Maths.Randomness";
	string Name = "Chance";
	string ValueType = "double";
#>
<#@ include file="..\Specialized\ValueTemplate.tt" #>
<#@ include file="..\Specialized\ComparableTemplate.tt" #>
 */

//using System;
//using System.Linq;

//namespace Recurse.Core.Maths.Randomness
//{
//    public partial struct Chance
//    {
//        public static Chance Zero => new Chance(0);

//        public static Chance One => new Chance(1);

//        public static Chance Half => new Chance(0.5);

//        public static Chance Create(double chance)
//            => new Chance(chance);

//        /// <summary>
//        /// Gets this chance with the given bonus applied, which exponentially increases the
//        /// chance of success (if positive) or failure (if negative).
//        /// </summary>
//        public Chance WithBonus(double bonus)
//        {
//            return bonus > 0 ?
//                Create(1 - Math.Pow(Value, bonus + 1)) :
//                Create(Math.Pow(Value, -bonus + 1));
//        }

//        public static Chance OfAll(double chance, double total)
//            => new Chance(Math.Pow(chance, total));

//        public static Chance OfAll(Chance chance, double total)
//            => OfAll(chance.Value, total);

//        public static Chance OfAll(params Chance[] chances)
//            => new Chance(chances.Select(chance => chance.Value).Aggregate((a, b) => a * b));

//        public static Chance OfAny(double chance, double attempts)
//            => new Chance(1 - Math.Pow(1 - chance, attempts));

//        public static Chance OfAny(Chance chance, double attempts)
//            => OfAny(chance.Value, attempts);

//        public static Chance OfAny(params Chance[] chances)
//            => new Chance(1 - chances.Select(chance => 1 - chance.Value).Aggregate((a, b) => a * b));

//        public double GetSuccessTrials(Random rng)
//        {
//            if (Value <= 0)
//                return double.PositiveInfinity;
//            return Math.Log(1 - rng.NextDouble()) / Math.Log(1 - Value);
//        }

//        public static double GetDistance(Random rng, double unitChance)
//            => Create(unitChance).GetSuccessTrials(rng);

//        static partial void OnCreated(ref double value)
//        {
//            if (value < 0)
//                throw new ArgumentOutOfRangeException(nameof(value), "Chance cannot be negative");
//            if (value > 1)
//                throw new ArgumentOutOfRangeException(nameof(value), "Chance cannot be greater than 1");
//        }

//        public double ToDouble() => Value;
//    }
//}
