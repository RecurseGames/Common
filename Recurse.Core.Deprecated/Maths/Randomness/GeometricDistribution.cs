﻿///*
// * Copyright (c) 2017 Ben Lambell.
// * 
// * This Source Code Form is subject to the terms of the Mozilla Public
// * License, v. 2.0. If a copy of the MPL was not distributed with this
// * file, You can obtain one at http://mozilla.org/MPL/2.0/.
// */

//using System;

//namespace Recurse.Core.Maths.Randomness
//{
//    [Serializable]
//    public class GeometricDistribution : IDistribution<double>
//    {
//        public Chance Chance { get; set; }

//        public static double GetValue(Random rng, Chance chance)
//        {
//            if (chance.ToDouble() <= 0)
//                return double.PositiveInfinity;
//            return Math.Log(1 - rng.NextDouble()) / Math.Log(1 - chance.ToDouble());
//        }

//        public static double GetValue(double randomDouble, Chance chance)
//        {
//            if (chance.ToDouble() <= 0)
//                return double.PositiveInfinity;
//            return Math.Log(1 - randomDouble) / Math.Log(1 - chance.ToDouble());
//        }

//        public static double GetValue(Random rng, double chance)
//            => GetValue(rng, Chance.Create(chance));

//        public double GetValue(Random rng)
//            => GetValue(rng, Chance);

//        public double GetValue(double randomDouble)
//            => GetValue(randomDouble, Chance);
//    }
//}
