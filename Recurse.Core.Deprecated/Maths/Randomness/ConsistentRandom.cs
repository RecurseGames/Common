﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Core.Hashing;
using System;

namespace Recurse.Core.Maths.Randomness
{
    /// <summary>
    /// An implementation of <see cref="Random"/> which ensures
    /// consistent behaviour across platforms (e.g., Unity and the
    /// Windows console).
    /// </summary>
    [Serializable]
    public class ConsistentRandom : Random
    {
        private uint
            _x = 548787455,
            _y = 842502087, 
            _z = 3579807591,
            _w = 273326509;

        public ConsistentRandom()
        {
            _w = unchecked((uint)base.Next());
        }

        public ConsistentRandom(int seed) : base(seed)
        {
            _w = unchecked((uint)seed);
        }

        public override int Next()
        {
            var result = (int) (Sample() * int.MaxValue);
            return result;
        }

        public override int Next(int maxValue)
        {
            var result = Next() % maxValue;
            return result;
        }

        public override int Next(int minValue, int maxValue)
        {
            var result = minValue + Next(maxValue - minValue);
            return result;
        }

        public override double NextDouble()
        {
            var result = (double)SampleUInt32() / uint.MaxValue;
            return result;
        }

        public override void NextBytes(byte[] buffer)
        {
            for (var i = 0; i < buffer.Length; i++)
                buffer[i] = (byte) (SampleUInt32() % byte.MaxValue);
        }

        protected override double Sample()
        {
            return new Hash(SampleUInt32()).ToDouble();
        }

        private uint SampleUInt32()
        {
            uint t = _x ^ (_x << 11);

            _x = _y; _y = _z; _z = _w;
            _w = _w ^ (_w >> 19) ^ t ^ (t >> 8);

            return _w;
        }
    }
}
