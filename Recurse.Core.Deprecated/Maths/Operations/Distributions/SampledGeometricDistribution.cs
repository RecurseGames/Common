﻿///*
// * Copyright (c) 2017 Ben Lambell.
// * 
// * This Source Code Form is subject to the terms of the Mozilla Public
// * License, v. 2.0. If a copy of the MPL was not distributed with this
// * file, You can obtain one at http://mozilla.org/MPL/2.0/.
// */

//using System;

//namespace Recurse.Core.Maths.Randomness
//{
//    public class SampledGeometricDistribution
//    {
//        private readonly Func<double, double> _getUnitChance;

//        private readonly double _sampleFrequency;

//        public SampledGeometricDistribution(Func<double, double> getUnitChance, double sampleFrequency)
//        {
//            _getUnitChance = getUnitChance;
//            _sampleFrequency = sampleFrequency;
//        }

//        public double GetNext(Random rng, double start)
//        {
//            return GetNext(rng, start, double.PositiveInfinity, _sampleFrequency, _getUnitChance);
//        }

//        public static double GetNext(
//            Random rng,
//            double start,
//            double sampleFrequency,
//            Func<double, double> getUnitChance)
//        {
//            return GetNext(rng, start, double.PositiveInfinity, sampleFrequency, getUnitChance);
//        }

//        public static double GetNext(
//            Random rng,
//            double start,
//            double limit,
//            double sampleFrequency,
//            Func<double, double> getUnitChance)
//        {
//            while (start < limit && !double.IsPositiveInfinity(start))
//            {
//                var result = Chance.GetDistance(rng, getUnitChance(start));
//                if (result <= sampleFrequency) return start + result;
//                start += sampleFrequency;
//            }
//            return double.PositiveInfinity;
//        }
//    }
//}