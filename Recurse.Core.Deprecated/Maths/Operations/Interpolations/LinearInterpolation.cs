﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;

namespace Recurse.Core.Maths.Operations.Interpolations
{
    [Serializable]
    public class LinearInterpolation : IEnumerable<KeyValuePair<double, double>>
    {
        private readonly SortedDictionary<double, double> _points = new SortedDictionary<double, double>();

        public void Add(double position, double value)
        {
            if (double.IsNaN(position))
            {
                throw new ArgumentException("Cannot interpolate NaN values.", nameof(position));
            }
            else if (double.IsNaN(value))
            {
                throw new ArgumentException("Cannot interpolate NaN values.", nameof(value));
            }
            else if (_points.ContainsKey(position))
            {
                throw new InvalidOperationException("Position already defined.");
            }

            _points[position] = value;
        }

        public double Get(double position)
        {
            KeyValuePair<double, double>? lowerPoint = null;
            foreach (var point in _points)
            {
                if (position == point.Key)
                {
                    return point.Value;
                }
                else if (position > point.Key)
                {
                    lowerPoint = point;
                }
                else if (lowerPoint == null)
                {
                    return point.Value;
                }
                else
                {
                    var lowerValue = lowerPoint.Value.Value;
                    var relativePosition = (position - lowerPoint.Value.Key) / (point.Key - lowerValue);

                    return relativePosition * point.Value + (1 - relativePosition) * lowerPoint.Value.Value;
                }
            }
            return lowerPoint != null ? lowerPoint.Value.Value : double.NaN;
        }

        public IEnumerator<KeyValuePair<double, double>> GetEnumerator()
        {
            return _points.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
