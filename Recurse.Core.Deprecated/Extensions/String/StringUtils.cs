﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Extensions.String
{
    /// <summary>
    /// Contains utility methods for dealing with Strings
    /// </summary>
    public static class StringUtils
    {
        public static string GetNamedQuantity(double amount, string noun)
        {
            return GetNamedQuantity(amount, noun, getRegularPlural(noun));
        }

        public static string GetNamedQuantity(double amount, string singular, string plural)
        {
            if (amount == 1) return GetA(singular) + " " + singular;
            return amount + " " + plural;
        }
        
        public static string GetA(string nounPhrase)
        {
            if (isVowel(nounPhrase[0]))
                return "an";
            return "a";
        }

        public static string GetPossessive(string fullName)
        {
            return fullName + (fullName.EndsWith("s") ? "'" : "'s");
        }

        /// <summary>
        /// Uses heuristics to create the plural form of a singular noun,
        /// by adding -s, -es or -ies.Nouns in upper case are assumed to
        /// be acronyms, and have -s appended only.NULL becomes NULL.
        /// </summary>
        public static string getRegularPlural(string singular)
        {
            if (singular == null)
                return null;

            // Deal with acronyms
            if (singular.All(char.IsUpper))
                return singular + "s";

            string lower = singular.ToLowerInvariant();
            if (new[] {"ch", "s", "sh", "x", "z"}.Any(lower.EndsWith))
                return singular + "es";
            if (lower.Length >= 2 && lower.EndsWith("y") && !isVowel(lower[lower.Length - 2]))
                return singular + "ies";
            return singular + "s";
        }

        public static string Capitalise(string name)
        {
            return name.Length > 0 ?
                char.ToUpperInvariant(name[0]) + name.Substring(1) :
                name;
        }

        public static string TitleCase(string name)
        {
            var result = string.Empty;
            for (var i = 0; i < name.Length; i++)
            {
                var c = name[i];
                result += i == 0 || char.IsWhiteSpace(name[i - 1]) ?
                    char.ToUpperInvariant(c) :
                    c;
            }
            return result;
        }

        public static bool isVowel(char c)
        {
            c = Char.ToLowerInvariant(c);
            return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
        }

        /// <summary>
        /// Prepends a number (eg, 3) to its ordinal suffix (eg, "rd")
        /// </summary>
        public static string addOrdinalSuffix(long i)
        {
            return i + getOrdinalSuffix(i);
        }

        /// <summary>
        /// Converts a number (eg, 3) to its ordinal suffix (eg, "rd")
        /// </summary>
        public static string getOrdinalSuffix(long i)
        {
            long j = i % 10, k = i % 100;
            if (j == 1 && k != 11)
                return "st";
            if (j == 2 && k != 12)
                return "nd";
            if (j == 3 && k != 13)
                return "rd";
            return "th";
        }

        public static string GetDirectionName(object direction, object orientation)
        {
            throw new NotImplementedException();

            //var names = new Dictionary<Angle, string>
            //{
            //    { Angle.FromPoint(orientation.Top), "North" },
            //    { Angle.FromPoint(orientation.TopLeft), "North West" },
            //    { Angle.FromPoint(orientation.Left), "West" },
            //    { Angle.FromPoint(orientation.BottomLeft), "South West" },
            //    { Angle.FromPoint(orientation.Bottom), "South" },
            //    { Angle.FromPoint(orientation.BottomRight), "South East" },
            //    { Angle.FromPoint(orientation.Right), "East" },
            //    { Angle.FromPoint(orientation.TopRight), "North East" }
            //};
            //names[360] = names[0];
            //var degrees = Angle.FromPoint(direction);
            //return names.OrderBy(e => Math.Abs(e.Key.Degrees - degrees.Degrees)).First().Value;
        }

        /// <summary>
        /// Formats the given objects (eg, [A, B, C]) as an english list (eg, "A, B and C")
        /// </summary>
        public static string englishList(System.Collections.IEnumerable parts)
        {
            var partList = parts as IList ?? parts.OfType<object>().ToList();
            var result = "";
            for (int i = 0; i < partList.Count; i++)
            {
                result += partList[i];
                if (i < partList.Count - 1)
                {
                    result += i == partList.Count - 2 ?
                            " and " :
                            ", ";
                }
            }
            return result;
        }

        public static IEnumerable<string> GetWrappedLines(string s, int width)
        {
            var lines = s.Split('\n');
            foreach (var line in lines)
            {
                var currentLine = "";
                foreach (var word in line.Split(' '))
                {
                    if (currentLine.Length == 0)
                    {
                        currentLine = word;
                    }
                    else if (currentLine.Length + word.Length + 1 > width)
                    {
                        yield return currentLine;
                        currentLine = word;
                    }
                    else
                    {
                        currentLine += " " + word;
                    }
                }
                if (currentLine.Length > 0)
                {
                    yield return currentLine;
                }
            }
        }
    }
}
