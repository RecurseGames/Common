﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;

namespace Recurse.Core.Extensions.Stack
{
    public static class StackExtensions
    {
        public static IDisposable WithTop<T>(this Stack<T> stack, T item)
        {
            return new StackWithTop<T>(stack, item);
        }

        private class StackWithTop<T> : IDisposable
        {
            private readonly Stack<T> _stack;

            public StackWithTop(Stack<T> stack, T item)
            {
                _stack = stack;
                _stack.Push(item);
            }

            public void Dispose()
            {
                _stack.Pop();
            }
        }
    }
}
