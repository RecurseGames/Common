﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Core.Hashing;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Deprecated.Extensions.RandomDeprecated
{
    /// <summary>
    /// Extension methods for the Random class
    /// </summary>
    public static class RandomExtensions
    {
        public static bool NextBool(this Random random)
        {
            return random.Next() % 2 == 0;
        }

        /// <summary>
        /// Removes and returns a random element from the given list
        /// </summary>
        public static T Remove<T>(this IList<T> list, Random random)
        {
            if (list.Count == 0)
                throw new ArgumentException("Cannot select random element from empty list");
            var i = random.Next(list.Count);
            var result = list[i];
            list.RemoveAt(i);
            return result;
        }

        /// <summary>
        /// Returns a random element from the sequence or, if it's empty, throws an Exception
        /// </summary>
        public static T Random<T>(this IEnumerable<T> enumerable, Random random)
        {
            var array = enumerable.ToArray();
            if (array.Length == 0)
                throw new InvalidOperationException("Sequence contains no elements");
            return array[random.Next(array.Length)];
        }

        /// <summary>
        /// Returns a random element from the sequence or, if it's empty, a default element.
        /// </summary>
        public static T RandomOrDefault<T>(this IEnumerable<T> enumerable, Random random)
        {
            var array = enumerable.ToArray();
            return array.Length == 0 ? default : array[random.Next(array.Length)];
        }

        /// <summary>
        /// Returns a random element from the sequence or, if it's empty, throws an Exception
        /// </summary>
        public static T Random<T>(this IEnumerable<T> enumerable, Hash hash)
        {
            var array = enumerable.ToArray();
            if (array.Length == 0)
                throw new InvalidOperationException("Sequence contains no elements");
            return array[hash.ToInteger(array.Length)];
        }

        /// <summary>
        /// Returns a random element from the sequence or, if it's empty, a default element.
        /// </summary>
        public static T RandomOrDefault<T>(this IEnumerable<T> enumerable, Hash hash)
        {
            var array = enumerable.ToArray();
            return array.Length == 0 ? default : array[hash.ToInteger(array.Length)];
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> enumerable, Random random)
        {
            var list = enumerable.ToList();
            for (var index = list.Count - 1; index > 0; index--)
            {
                var newIndex = random.Next(index + 1);
                if (index != newIndex)
                {
                    var displacedElement = list[newIndex];

                    list[newIndex] = list[index];
                    list[index] = displacedElement;
                }
            }
            return list;
        }
    }
}