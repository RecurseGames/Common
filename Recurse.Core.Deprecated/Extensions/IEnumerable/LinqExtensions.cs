﻿using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace System.Linq
{
    public static class RecurseLinqExtensions
    {
        public static T Max<T>(this IEnumerable<T> enumerable, IComparer<T> comparer)
        {
            return enumerable.OrderBy(e => e, comparer).Last();
        }

        public static T Min<T>(this IEnumerable<T> enumerable, IComparer<T> comparer)
        {
            return enumerable.OrderBy(e => e, comparer).First();
        }

        public static IEnumerable<T> Concat<T>(this IEnumerable<T> enumerable, params T[] elements)
        {
            return enumerable.Concat((IEnumerable<T>)elements);
        }

        public static IEnumerable<T> Except<T>(this IEnumerable<T> enumerable, params T[] elements)
        {
            return enumerable.Except((IEnumerable<T>)elements);
        }
    }
}