﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Extensions.IEnumerable
{
    public static class IEnumerableExtensions
    {
        public static bool ContainsOnly<T>(this IEnumerable<T> enumerable, T value)
        {
            var valueFound = false;
            foreach (var element in enumerable)
            {
                if (valueFound)
                {
                    return false;
                }
                else if (Equals(element, value))
                {
                    valueFound = true;
                }
                else
                {
                    return false;
                }
            }
            return valueFound;
        }

        public static T SingleIfAny<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate = null)
        {
            if (predicate != null)
                enumerable = enumerable.Where(predicate);

            T candidate = default(T);
            var candidateFound = false;
            foreach (var item in enumerable)
            {
                if (candidateFound) return default(T);
                candidateFound = true;
                candidate = item;
            }
            return candidate;
        }

        public static IEnumerable<T> ConcatItem<T>(this IEnumerable<T> items, T concatItem)
        {
            foreach (var item in items)
                yield return item;
            yield return concatItem;
        }
    }
}
