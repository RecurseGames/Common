﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Recurse.Core.Diagnostics
{
    public class DictionaryProxy
    {
        private readonly IEnumerable _dictionary;

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public object[] Items => GetItems().ToArray();

        public DictionaryProxy(IEnumerable dictionary)
        {
            _dictionary = dictionary;
        }

        private IEnumerable<object> GetItems()
        {
            foreach (var entry in _dictionary)
            {
                var entryType = entry.GetType();

                if (entryType?.GetProperty("Key") == null)
                {
                    yield return new KeyValuePairProxy("ERROR", entryType);
                }
                else
                {
                    var key = entryType?.GetProperty("Key").GetValue(entry);
                    var value = entryType?.GetProperty("Value").GetValue(entry);
                    var valueType = value?.GetType();
                    var proxies = valueType?.GetCustomAttributes(false).OfType<DebuggerTypeProxyAttribute>().ToArray();

                    if (proxies != null 
                        && proxies.Length == 1 
                        && Equals(proxies[0].ProxyTypeName, typeof(DictionaryProxy).AssemblyQualifiedName))
                    {
                        yield return new NestedKeyValuePairProxy(key, value);
                    }
                    else
                    {
                        yield return new KeyValuePairProxy(key, value);
                    }
                }
            }
        }
    }
}
