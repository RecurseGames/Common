﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Diagnostics
{
    public struct DebugString
    {
        private readonly string _delimiter;
        
        private readonly IEnumerable<object> _elements;

        public DebugString(object element)
        {
            _delimiter = string.Empty;
            _elements = new[] { element };
        }

        public DebugString(string delimiter, IEnumerable<object> elements)
        {
            _delimiter = delimiter;
            _elements = elements;
        }

        public override string ToString()
        {
            try
            {
                return string.Join(_delimiter, _elements.Select(GetElementString));
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        private static string GetElementString(object element)
        {
            if (element is Type type)
            {
                return type.Name;
            }
            else if (element is Delegate action)
            {
                return GetElementString(action.Target) + "." + action.Method.Name;
            }
            else if (element == null)
            {
                return "null";
            }
            else if (Equals(element.GetType().GetMethod("ToString", new Type[0]).DeclaringType, typeof(object)))
            {
                return element.GetType().Name + "(...)";
            }
            else
            {
                return element.ToString();
            }
        }
    }
}
