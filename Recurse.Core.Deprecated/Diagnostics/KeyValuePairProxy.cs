﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Diagnostics;

namespace Recurse.Core.Diagnostics
{
    [DebuggerDisplay("{DebuggerDisplay}")]
    public struct KeyValuePairProxy
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string DebuggerDisplay => ToString();

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private object Key { get; }

        public object Value { get; }

        public KeyValuePairProxy(object key, object value)
        {
            Key = key;
            Value = value;
        }

        public override string ToString()
        {
            return new DebugString(Key).ToString();
        }
    }
}
