﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Core.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

[assembly: DebuggerTypeProxy(typeof(Recurse.Core.Diagnostics.NestedDictionaryDisplayProxy<object,object>), Target = typeof(Dictionary<ImmutableList<object>,object>))]

namespace Recurse.Core.Diagnostics
{
    /// <summary>
    /// Debugger display proxy which displays a dictionary with
    /// list keys as if it were a series of nested dicionaries.
    /// </summary>
    public class NestedDictionaryDisplayProxy<TKey, TValue>
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        protected IDictionary<ImmutableList<TKey>, TValue> Dictionary { get; }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        protected ImmutableList<TKey> Keys { get; }

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public object[] Entries
        {
            get
            {
                try
                {
                    var entries = Dictionary
                        .Where(e => e.Key.Count > Keys.Count && e.Key.GetRange(0, Keys.Count).SequenceEqual(Keys))
                        .Select(e => e.Key[Keys.Count])
                        .Distinct()
                        .Select(key => new Entry(Dictionary, Keys.Add(key)))
                        .ToArray();
                    return entries;
                }
                catch (Exception e)
                {
                    return new object[] { e };
                }
            }
        }

        public NestedDictionaryDisplayProxy(IDictionary<ImmutableList<TKey>, TValue> dictionary)
        {
            Dictionary = dictionary;
            Keys = ImmutableList<TKey>.Empty;
        }

        public NestedDictionaryDisplayProxy(IDictionary<ImmutableList<TKey>, TValue> dictionary, ImmutableList<TKey> keys)
        {
            Dictionary = dictionary;
            Keys = keys;
        }

        [DebuggerDisplay("{Value}", Name = "{Key}")]
        private class Entry : NestedDictionaryDisplayProxy<TKey, TValue>
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public object Key => new DebugString(Keys.Last());

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public object Value
            {
                get
                {
                    if (Dictionary.TryGetValue(Keys, out TValue value))
                    {
                        return new DebugString(value).ToString();
                    }
                    else
                    {
                        return $"Count = {Entries.Length}";
                    }
                }
            }

            public Entry(IDictionary<ImmutableList<TKey>, TValue> dictionary, ImmutableList<TKey> keys) : base(dictionary, keys)
            {

            }
        }
    }
}
