﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Diagnostics
{
#warning Consider removing, functionality may be elsewhere
    public class StackLog
    {
        private static readonly List<string> Contexts = new List<string>();

        public static string Current => string.Join("\n", Contexts);

        public static IDisposable CreateContext(params object[] description)
        {
            Contexts.Add(string.Join(" ", description.Select(Render).ToArray()));
            return new Context();
        }

        private static string Render(object target)
        {
            if (target == null)
            {
                return "null";
            }
            var targetString = target.ToString();
            return Equals(targetString, target.GetType().ToString())
                ? target.GetType().Name
                : targetString;
        }

        private class Context : IDisposable
        {
            public void Dispose()
            {
                Contexts.RemoveAt(Contexts.Count - 1);
            }
        }
    }
}
