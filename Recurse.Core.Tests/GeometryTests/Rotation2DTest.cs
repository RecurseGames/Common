﻿using NUnit.Framework;
using Recurse.Core.Collections.Planes;

namespace Recurse.Core.Tests.GeometryTests
{
    public class Rotation2DTest
    {
        private readonly IRectangleMap<string> _array2x2;

        public Rotation2DTest()
        {
            var array2x2 = new Array2D<string>(2, 2);
            array2x2[new(0, 0)] = "A";
            array2x2[new(1, 0)] = "B";
            array2x2[new(0, 1)] = "C";
            array2x2[new(1, 1)] = "D";

            _array2x2 = array2x2;
        }

        [Test]
        [TestCase(0, 0, false, false)]
        [TestCase(0, 0, true, false)]
        [TestCase(0, 0, false, true)]
        [TestCase(0, 0, true, true)]
        [TestCase(1, 0, false, false)]
        [TestCase(1, 0, true, false)]
        [TestCase(1, 0, false, true)]
        [TestCase(1, 0, true, true)]
        [TestCase(1, 1, false, false)]
        [TestCase(1, 1, true, false)]
        [TestCase(1, 1, false, true)]
        [TestCase(1, 1, true, true)]
        [TestCase(10, 10, false, false)]
        [TestCase(10, 10, true, false)]
        [TestCase(10, 10, false, true)]
        [TestCase(10, 10, true, true)]
        public void TestContains(int offsetX, int offsetY, bool flipX, bool flipY)
        {
            var rotation = new Rotation2D(flipX, flipY);
            var view = rotation.CreateArray(_array2x2, offsetX, offsetY, 2, 2);

            Assert.That(view.Contains(new(0, 0)), Is.True);
            Assert.That(view.Contains(new(1, 0)), Is.True);
            Assert.That(view.Contains(new(0, 1)), Is.True);
            Assert.That(view.Contains(new(1, 1)), Is.True);
            Assert.That(view.Contains(new(1, 2)), Is.False);
            Assert.That(view.Contains(new(2, 2)), Is.False);
            Assert.That(view.Contains(new(-1, 1)), Is.False);
            Assert.That(view.Contains(new(-1, -1)), Is.False);
        }

        [Test]
        public void TestNoTransform()
        {
            var view = Rotation2D.Default.CreateArray(_array2x2, 0, 0, 2, 2);

            Assert.That(Render(view), Is.EqualTo(
                "AB\n" +
                "CD"));
        }

        [Test]
        public void TestRotateLeft()
        {
            var view = Rotation2D.Left.CreateArray(_array2x2, 0, 0, 2, 2);

            Assert.That(Render(view), Is.EqualTo(
                "BD\n" +
                "AC"));
        }

        [Test]
        public void TestRotateRight()
        {
            var view = Rotation2D.Right.CreateArray(_array2x2, 0, 0, 2, 2);

            Assert.That(Render(view), Is.EqualTo(
                "CA\n" +
                "DB"));
        }

        [Test]
        public void TestRotate180()
        {
            var view = Rotation2D.Full.CreateArray(_array2x2, 0, 0, 2, 2);

            Assert.That(Render(view), Is.EqualTo(
                "DC\n" +
                "BA"));
        }

        [Test]
        public void TestFlipX()
        {
            var view = Rotation2D.Horizontal.CreateArray(_array2x2, 0, 0, 2, 2);

            Assert.That(Render(view), Is.EqualTo(
                "BA\n" +
                "DC"));
        }

        [Test]
        public void TestFlipY()
        {
            var view = Rotation2D.Vertical.CreateArray(_array2x2, 0, 0, 2, 2);

            Assert.That(Render(view), Is.EqualTo(
                "CD\n" +
                "AB"));
        }

        private static string Render(IRectangleMap<string> array)
        {
            var result = string.Empty;
            for (var y = 0; y < array.Height; y++)
            {
                if (y != 0)
                {
                    result += "\n";
                }

                for (var x = 0; x < array.Width; x++)
                {
                    result += array[new(x, y)];
                }
            }
            return result;
        }
    }
}
