﻿using NUnit.Framework;
using Recurse.Core.Geometry2D;
using Recurse.Core.Geometry2D.Spheres;
using System;

namespace Recurse.Core.Tests.GeometryTests
{
    public class LatLongTest
    {
        private const double MarginOfError = 0.000001;

        [Test]
        public void TestEquality()
        {
            Assert.That(LatLong.NorthPole, Is.EqualTo(LatLong.NorthPole));
            Assert.That(LatLong.Create(0, 0), Is.EqualTo(LatLong.Create(0, 0)));
        }

        [Test]
        [TestCase(0 * Math.PI / 2, 0, 90)]
        [TestCase(1 * Math.PI / 2, 90, 0)]
        [TestCase(2 * Math.PI / 2, 0, 0)]
        [TestCase(3 * Math.PI / 2, -90, 0)]
        public void TestOffsetsFromZero(double bearing, double latitude, double longitude)
        {
            var point = LatLong.Default.GetOffset(new Radians(Math.PI / 2), new Radians(bearing));

            Assert.That($"{point}", Is.EqualTo($"{LatLong.Create(latitude, longitude)}"));
        }

        [Test]
        [TestCase(0 * Math.PI / 2, "0, 180")]
        [TestCase(1 * Math.PI / 2, "0, 0")]
        [TestCase(2 * Math.PI / 2, "0, 0")]
        [TestCase(3 * Math.PI / 2, "0, 0")]
        public void TestOffsetsFromNorthPole(double bearing, string expected)
        {
            var point = LatLong.NorthPole.GetOffset(new Radians(Math.PI / 2), new Radians(bearing));

            Assert.AreEqual(expected, point.ToString());
        }

        [Test]
        public void TestDistanceToSelf(
            [Values(-90, 90, 0, 10)] double latitude,
            [Values(-180, 180, 0, 90)] double longitude)
        {
            var point = LatLong.Create(latitude, longitude);

            Assert.That(point.GetDistance(point), Is.EqualTo(0));
        }

        [Test]
        public void TestDistanceBetweenPoles()
        {
            Assert.That(LatLong.NorthPole.GetDistance(LatLong.SouthPole), Is.EqualTo(Math.PI));
            Assert.That(LatLong.Create(0, 180).GetDistance(LatLong.Create(0, 0)), Is.EqualTo(Math.PI));
            Assert.That(LatLong.Create(0, -180).GetDistance(LatLong.Create(0, 0)), Is.EqualTo(Math.PI));
            Assert.That(LatLong.Create(0, 90).GetDistance(LatLong.Create(0, -90)), Is.EqualTo(Math.PI));
        }

        [Test]
        public void TestZeroDistanceOffset(
            [Values(-90, 90, 0, 45)] double latitude,
            [Values(-180, 180, 0, 90)] double longitude,
            [Values(0, Math.PI)] double bearing)
        {
            var origin = LatLong.Create(latitude, longitude);
            var destination = origin.GetOffset(default, new Radians(bearing));

            Assert.That($"{destination}", Is.EqualTo($"{LatLong.Create(latitude, longitude)}"));
        }

        [Test]
        public void TestDistanceToOffset(
            [Values(-90, 90, 0, 45)] double latitude,
            [Values(-180, 180, 0, 90)] double longitude,
            [Values(1, Math.PI)] double distance,
            [Values(0, Math.PI)] double bearing)
        {
            var origin = LatLong.Create(latitude, longitude);
            var destination = origin.GetOffset(new Radians(distance), new Radians(bearing));

            Assert.AreEqual(distance, origin.GetDistance(destination).Value, MarginOfError);
        }

        [Test]
        public void TestBearingToOffset(
            [Values(-90, 90, 0, 45)] double latitude,
            [Values(-180, 180, 0, 90)] double longitude,
            [Values(1, Math.PI)] double distance,
            [Values(0, Math.PI)] double bearing)
        {
            var origin = LatLong.Create(latitude, longitude);
            var destination = origin.GetOffset(new Radians(distance), new Radians(bearing));

            if (origin.GetDistance(destination).Value < MarginOfError)
            {
                Assert.That(origin.GetBearing(destination), Is.EqualTo(bearing));
            }
        }
    }
}
