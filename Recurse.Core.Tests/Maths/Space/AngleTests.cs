﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Core.Geometry2D;

namespace Recurse.Core.Test.Maths.Space
{
    public class AngleTests
    {
        [TestCase(0, 0, 0)]
        [TestCase(0, 360, 360)]
        [TestCase(0, 361, 361)]
        [TestCase(0, 10, 10)]
        [TestCase(0, -10, -10)]
        [TestCase(10, 0, 10)]
        [TestCase(-10, 0, 350)]
        public void TestGetRotation(double expectedAngleDegrees, double startDegrees, double endDegrees)
        {
            var start = new Degrees(startDegrees);
            var end = new Degrees(endDegrees);
            var actualAngle = start.GetDifference(end);
            var expectedAngle = new Degrees(expectedAngleDegrees);

            Assert.AreEqual(expectedAngle, actualAngle);
        }

        [TestCase(0, 0, 0)]
        [TestCase(0, 360, 360)]
        [TestCase(0, 361, 361)]
        [TestCase(0, 10, 10)]
        [TestCase(0, -10, -10)]
        [TestCase(10, 0, 10)]
        [TestCase(-10, 0, 350)]
        public void TestGetRotationRadians(double expectedAngleDegrees, double startDegrees, double endDegrees)
        {
            Radians start = new Degrees(startDegrees);
            Radians end = new Degrees(endDegrees);
            Radians actualAngle = start.GetDifference(end);
            Radians expectedAngle = new Degrees(expectedAngleDegrees);

            Assert.AreEqual(expectedAngle, actualAngle);
        }
    }
}
