﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Linq;
using NUnit.Framework;
using Recurse.Core.Collections.Planes;
using Recurse.Core.Maths.Space.Points;

namespace Recurse.Core.Test.Maths.Space
{
    public class PointArrayTests
    {
        [Test]
        public void RectPointsCalculatedCorrectly()
        {
            var points = new XYRectangle(-1, -1, 4, 3).GetPoints().ToList();

            Assert.AreEqual(12, points.Count());
            foreach (var direction in default(XY).GetNeighbours())
                Assert.Contains(direction, points);
            Assert.Contains(new XY(0, 0), points);
            Assert.Contains(new XY(2, -1), points);
            Assert.Contains(new XY(2, 0), points);
            Assert.Contains(new XY(2, 1), points);
        }
    }
}
