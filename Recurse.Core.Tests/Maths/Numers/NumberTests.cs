﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Core.Maths;

namespace Recurse.Core.Test.Maths.Numers
{
    public class NumberTests
    {
        [Test]
        public void CanWrapNumbers()
        {
            Assert.AreEqual(0, Number.Wrap(0, 0, 1));
            Assert.AreEqual(0, Number.Wrap(3, 0, 3));
            Assert.AreEqual(-1, Number.Wrap(3, -1, 3));
        }
    }
}
