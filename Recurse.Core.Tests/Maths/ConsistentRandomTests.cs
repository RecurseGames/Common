﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Core.Maths.Randomness;

namespace Recurse.Core.Test.Maths
{
    public class ConsistentRandomTests
    {
        [Test]
        public void RandomNumbersAreConsistent()
        {
            var rng1 = new ConsistentRandom(1995);
            var rng2 = new ConsistentRandom(1995);

            Assert.AreEqual(rng1.Next(), rng2.Next());
        }
    }
}
