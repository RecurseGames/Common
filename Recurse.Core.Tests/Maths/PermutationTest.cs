﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using System.Linq;
using Recurse.Core.Maths;

namespace Recurse.Core.Test.Maths
{
    public class PermutationTest
    {
        [Test]
        public void AllPermutationsFound()
        {
            var array = new[] { 1, 2, 3 };

            var permutations = GetPermutations.Execute(array);

            var expectedPermutations = new[]
            {
                new[] { 1, 2, 3 },
                new[] { 1, 3, 2 },
                new[] { 2, 1, 3 },
                new[] { 2, 3, 1 },
                new[] { 3, 1, 2 },
                new[] { 3, 2, 1 }
            };
            foreach (var expectedPermutation in expectedPermutations)
            {
                Assert.IsTrue(permutations.Any(permutation => permutation.SequenceEqual(expectedPermutation)));
            }
        }
    }
}
