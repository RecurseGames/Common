﻿using NUnit.Framework;
using Recurse.Core.Collections.Planes;
using Recurse.Core.Maths.Space.Points;
using System.Linq;

namespace Recurse.Core.Tests.Maths
{
    public class TriangleTests
    {
        [Test]
        public void TestTrianglePoints()
        {
            var triangle = new Triangle(0, 0, 2, 0, 2, 2);

            var points = triangle.GetPoints().ToList();

            Assert.Contains(new XY(0, 0), points);
            Assert.Contains(new XY(1, 0), points);
            Assert.Contains(new XY(2, 0), points);
            Assert.Contains(new XY(1, 1), points);
            Assert.Contains(new XY(2, 1), points);
            Assert.Contains(new XY(2, 2), points);
            Assert.AreEqual(points.Count, 6);
        }

        [Test]
        public void TestTrianglePoints2()
        {
            var triangle = new Triangle(0, 0, 0, 2, 2, 0);

            var points = triangle.GetPoints().ToList();

            Assert.Contains(new XY(0, 0), points);
            Assert.Contains(new XY(1, 0), points);
            Assert.Contains(new XY(2, 0), points);
            Assert.Contains(new XY(0, 1), points);
            Assert.Contains(new XY(1, 1), points);
            Assert.Contains(new XY(0, 2), points);
            Assert.AreEqual(points.Count, 6);
        }

        [Test]
        public void TestTrianglePoints3()
        {
            var triangle = new Triangle(0, 0, 1, 1, 1, -1);

            var points = triangle.GetPoints().ToList();

            Assert.Contains(new XY(0, 0), points);
            Assert.Contains(new XY(1, -1), points);
            Assert.Contains(new XY(1, 0), points);
            Assert.Contains(new XY(1, 1), points);
            Assert.AreEqual(points.Count, 4);
        }

        [Test]
        public void TestTrianglePoints4()
        {
            var triangle = new Triangle(0, -1, 0, 1, 1, 0);

            var points = triangle.GetPoints().ToList();

            Assert.Contains(new XY(1, 0), points);
            Assert.Contains(new XY(0, -1), points);
            Assert.Contains(new XY(0, 0), points);
            Assert.Contains(new XY(0, 1), points);
            Assert.AreEqual(points.Count, 4);
        }

        [Test]
        public void TestTrianglePoints5()
        {
            var triangle = new Triangle(0, 0, 1, 1, 2, 0);

            var points = triangle.GetPoints().ToList();

            Assert.Contains(new XY(0, 0), points);
            Assert.Contains(new XY(1, 0), points);
            Assert.Contains(new XY(2, 0), points);
            Assert.Contains(new XY(1, 1), points);
            Assert.AreEqual(points.Count, 4);
        }

        [Test]
        public void TestTrianglePoints6()
        {
            var triangle = new Triangle(0, 0, 1, -1, 2, 0);

            var points = triangle.GetPoints().ToList();

            Assert.Contains(new XY(0, 0), points);
            Assert.Contains(new XY(1, 0), points);
            Assert.Contains(new XY(2, 0), points);
            Assert.Contains(new XY(1, -1), points);
            Assert.AreEqual(points.Count, 4);
        }

        [Test]
        public void TestBoundedTrianglePoints()
        {
            var triangle = new Triangle(0, 0, 3, 0, 3, 3);

            var points = triangle.GetPoints(new XYRectangle(1, 1, 2, 2)).ToList();

            Assert.Contains(new XY(1, 1), points);
            Assert.Contains(new XY(2, 1), points);
            Assert.Contains(new XY(2, 2), points);
            Assert.AreEqual(points.Count, 3);
        }
    }
}
