﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using System.Linq;
using Recurse.Persist;

namespace Recurse.Core.Test.Maths
{
    public class ArrayIndexEnumerableTests
    {
        [Test]
        public void CanEnumerateEmptyArrayIndices()
        {
            var array = new int [0];
            var expectedIndices = new int[0][];

            var indices = new ArrayIndexEnumerable(array).ToArray();

            Assert.AreEqual(expectedIndices, indices);
        }

        [Test]
        public void CanEnumerateOneDimensionalArrayIndices()
        {
            var array = new[] { 6, 7, 8 };
            var expectedIndices = new[] { new[] { 0 }, new[] { 1 }, new[] { 2 } };

            var indices = new ArrayIndexEnumerable(array).ToArray();

            Assert.AreEqual(expectedIndices, indices);
        }

        [Test]
        public void CanEnumerateTwoDimensionalArrayIndices()
        {
            var array = new[,] { { 11, 12 }, { 13, 14 } };
            var expectedIndices = new[] { new[] { 0, 0 }, new[] { 0, 1 }, new[] { 1, 0 }, new[] { 1, 1 } };

            var indices = new ArrayIndexEnumerable(array).ToArray();

            Assert.AreEqual(expectedIndices, indices);
        }
    }
}
