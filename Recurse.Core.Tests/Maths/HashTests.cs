﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Core.Hashing;

namespace Recurse.Core.Test.Maths
{
    public class HashTests
    {
        [Test]
        public void HashDoubleToDouble()
        {
            var result1 = Hash.Default.Add(24.3414153331027).ToDouble();
            var result2 = Hash.Default.Add(24.3414153331026).ToDouble();

            Assert.AreNotEqual(result1, result2);
        }

        [Test]
        public void HashDoubleToHashCode()
        {
            var result1 = Hash.Default.Add(24.3414153331027).ToHashCode();
            var result2 = Hash.Default.Add(24.3414153331026).ToHashCode();

            Assert.AreNotEqual(result1, result2);
        }
    }
}
