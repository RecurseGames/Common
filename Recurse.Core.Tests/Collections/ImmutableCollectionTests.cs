﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Core.Collections.Generic;

namespace Recurse.Core.Test.Collections
{
    public static class ImmutableCollectionTests
    {
        [Test]
        public static void SetEqualsSelf()
        {
            Assert.AreEqual(ImmutableMultiset.Create(1, 2), ImmutableMultiset.Create(1, 2));
        }

        [Test]
        public static void EqualityIgnoresOrder()
        {
            Assert.AreEqual(ImmutableMultiset.Create(1, 2), ImmutableMultiset.Create(2, 1));
        }

        [Test]
        public static void PartialIntersectionIsUnequal()
        {
            Assert.AreNotEqual(ImmutableMultiset.Create(1, 2), ImmutableMultiset.Create(1, 3));
        }

        [Test]
        public static void DisjointSetsAreUnequal()
        {
            Assert.AreNotEqual(ImmutableMultiset.Create(1, 2), ImmutableMultiset.Create(3, 4));
        }
    }
}
