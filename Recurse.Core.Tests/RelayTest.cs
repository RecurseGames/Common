﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Core.Messaging;
using System;

namespace Recurse.Core.Test
{
    public class CallbackInvokerTest
    {
        [Test]
        public void CanGarbageCollectWeakReceiver()
        {
            var relayToGarbageCollectTarget = new InvocationCounter();
            var relayToHoldWeakReference = ICallbackInvoker.CreateExecutable();
            
            AddWeakReference(relayToHoldWeakReference, relayToGarbageCollectTarget);
            GC.Collect();

            relayToHoldWeakReference.Execute();

            Assert.AreEqual(0, relayToGarbageCollectTarget.Invocations);
        }

        [Test]
        public void CanAddWeakReceiver()
        {
            var receiver = new InvocationCounter();
            var relay = ICallbackInvoker.CreateExecutable();
            Action a = receiver.Execute;
            relay.AddWeak(a);
            relay.Execute();

            Assert.AreEqual(1, receiver.Invocations);
        }

        [Test]
        public void CanRemoveReceiver()
        {
            var receiver = new InvocationCounter();
            var relay = ICallbackInvoker.CreateExecutable();
            relay.Add(receiver.Execute);
            relay.Remove(receiver.Execute);
            relay.Execute();

            Assert.AreEqual(0, receiver.Invocations);
        }

        [Test]
        public void CanRemoveWeakReceiver()
        {
            var receiver = new InvocationCounter();
            var relay = ICallbackInvoker.CreateExecutable();
            relay.AddWeak(receiver.Execute);
            relay.Remove(receiver.Execute);
            relay.Execute();

            Assert.AreEqual(0, receiver.Invocations);
        }

        private void AddWeakReference(ICallbackInvoker relay, InvocationCounter target)
        {
            var relayToGarbageCollect = ICallbackInvoker.CreateExecutable();
            relayToGarbageCollect.Add(target.Execute);
            relay.AddWeak(relayToGarbageCollect.Execute);
        }

        public class InvocationCounter
        {
            public int Invocations { get; private set; }

            public void Execute()
            {
                Invocations++;
            }
        }
    }
}
