﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Core.Randomness.PRNGs;
using System.Collections.Generic;

namespace Recurse.Core.Test
{
    public class PcgTest
    {
        [TestCase(0)]
        public void DoesNotRepeat(int seed)
        {
            var pcg = new Pcg(seed);
            var values = new HashSet<int>();

            //    int.MaxValue == 2147483647
            while (values.Count < 10000000)
            {
                var nextValue = pcg.Next();

                Assert.IsTrue(
                    values.Add(nextValue),
                    $"Repeated value {nextValue} after {values.Count} values");
            }
        }
    }
}
