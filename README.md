## Common Framework

General purpose library for implementing Collection types, handling coordinates, and other basic tasks.

### Usage

This library is under active development, and not yet ready to be published as a Nuget package. To use this library in a project, it and its dependencies must be added to the same directory. They may be added as Git submodules.

This library has no dependencies.

### Unity Compatibility

To support adding this repository to Unity Assets folders:
* Some directories (e.g., test projects) include a `~` suffix. This ensures they are omitted from Unity builds.
* Non-test projects use C# version 6.
