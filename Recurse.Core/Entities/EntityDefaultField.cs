﻿using System;

namespace Recurse.Amaranth2.Components.Entities
{
    [Serializable]
    public class EntityDefaultField<T>
    {
        private readonly IEntity _entity;

        private readonly object _key;

        private readonly T _defaultValue;

        public T Value
        {
            get
            {
                if (_entity.Data.TryGetValue(_key, out var value))
                {
                    return value is T validValue ? validValue : throw new InvalidOperationException("Entity data key mapped to unexpected type.");
                }
                return _defaultValue;
            }
            // Note setting the value to the default still stores data, since failure to do so
            // could create extremely-difficult-to-debug defects if another instance is used with
            // a different _defaultValue
            set => _entity.Data[_key] = value;
        }

        public EntityDefaultField(IEntity entity, object key, T defaultValue)
        {
            _entity = entity;
            _key = key;
            _defaultValue = defaultValue;
        }
    }
}
