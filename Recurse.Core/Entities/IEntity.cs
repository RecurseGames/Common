﻿using System.Collections.Generic;

namespace Recurse.Amaranth2.Components.Entities
{
    public interface IEntity
    {
        IDictionary<object, object?> Data { get; }
    }
}
