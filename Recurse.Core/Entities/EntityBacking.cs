﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace Recurse.Amaranth2.Components.Entities
{
    public static class EntityBacking
    {
        public static EntityDefaultField<T> CreateDefaultField<T>(
            IEntity entity,
            T defaultValue,
            [CallerFilePath] string? callerFilePath = null,
            [CallerMemberName] string? callerMemberName = null)
        {
            if (callerFilePath == null)
            {
                throw new ArgumentNullException(nameof(callerFilePath));
            }
            else if (callerMemberName == null)
            {
                throw new ArgumentNullException(nameof(callerMemberName));
            }

            var key = new Key(callerFilePath, callerMemberName);

            return new EntityDefaultField<T>(entity, key, defaultValue);
        }

        public static EntityField<T> CreateField<T>(
            IEntity entity,
            [CallerFilePath] string? callerFilePath = null,
            [CallerMemberName] string? callerMemberName = null)
        {
            if (callerFilePath == null)
            {
                throw new ArgumentNullException(nameof(callerFilePath));
            }
            else if (callerMemberName == null)
            {
                throw new ArgumentNullException(nameof(callerMemberName));
            }

            var key = new Key(callerFilePath, callerMemberName);

            return new EntityField<T>(entity, key);
        }

        public static LazyEntityField<T> CreateLazyField<T>(
            IEntity entity,
            Func<T> valueFactory,
            [CallerFilePath] string? callerFilePath = null,
            [CallerMemberName] string? callerMemberName = null)
            where T : class
        {
            if (callerFilePath == null)
            {
                throw new ArgumentNullException(nameof(callerFilePath));
            }
            else if (callerMemberName == null)
            {
                throw new ArgumentNullException(nameof(callerMemberName));
            }

            var key = new Key(callerFilePath, callerMemberName);

            return new LazyEntityField<T>(entity, key, valueFactory);
        }

        public static object CreateKey(
            [CallerFilePath] string? callerFilePath = null,
            [CallerMemberName] string? callerMemberName = null)
        {
            if (callerFilePath == null)
            {
                throw new ArgumentNullException(nameof(callerFilePath));
            }
            else if (callerMemberName == null)
            {
                throw new ArgumentNullException(nameof(callerMemberName));
            }

            return new Key(callerFilePath, callerMemberName);
        }

        [Serializable]
        private record Key
        {
            private readonly string _memberFilePath;

            private readonly string _memberName;

            public Key(string memberFilePath, string memberName)
            {
                _memberFilePath = memberFilePath;
                _memberName = memberName;
            }

            public override string ToString()
            {
                var memberFileName = Path.GetFileNameWithoutExtension(_memberFilePath);

                return $"{memberFileName}/{_memberName}";
            }
        }
    }
}
