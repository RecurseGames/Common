﻿using System;

namespace Recurse.Amaranth2.Components.Entities
{
    [Serializable]
    public class LazyEntityField<T> where T : class
    {
        private readonly IEntity _entity;

        private readonly object _key;

        private readonly Func<T> _valueFactory;

        public bool IsValueCreated => _entity.Data.ContainsKey(_key);

        public LazyEntityField(IEntity entity, object key, Func<T> valueFactory)
        {
            _entity = entity;
            _key = key;
            _valueFactory = valueFactory;
        }

        public T GetOrCreateValue()
        {
            if (_entity.Data.TryGetValue(_key, out var value))
            {
                return value is T validValue ? validValue : throw new InvalidOperationException("Entity data key mapped to unexpected type.");
            }
            else
            {
                var validValue = _valueFactory();
                _entity.Data[_key] = validValue;
                return validValue;
            }
        }

        public T? GetValue()
        {
            if (_entity.Data.TryGetValue(_key, out var value))
            {
                if (value is T validValue)
                {
                    return validValue;
                }
                throw new InvalidOperationException("Entity data key mapped to unexpected type.");
            }
            return null;
        }

        public T? PopValue()
        {
            if (_entity.Data.TryGetValue(_key, out var value))
            {
                if (value is T validValue)
                {
                    _entity.Data.Remove(_key);
                    return validValue;
                }
                throw new InvalidOperationException("Entity data key mapped to unexpected type.");
            }
            return null;
        }
    }
}
