﻿using System;

namespace Recurse.Amaranth2.Components.Entities
{
    [Serializable]
    public class EntityField<T>
    {
        private readonly IEntity _entity;

        private readonly object _key;

        public bool HasValue => _entity.Data.ContainsKey(_key);

        public T Value
        {
            get
            {
                if (_entity.Data.TryGetValue(_key, out var value))
                {
                    return value is T validValue ? validValue : throw new InvalidOperationException("Entity data key mapped to unexpected type.");
                }
                throw new InvalidOperationException("Value not set.");
            }
            set => _entity.Data[_key] = value;
        }

        public EntityField(IEntity entity, object key)
        {
            _entity = entity;
            _key = key;
        }

        public T GetOrSetValue(T defaultValue)
        {
            if (_entity.Data.TryGetValue(_key, out var value))
            {
                return value is T validValue ? validValue : throw new InvalidOperationException("Entity data key mapped to unexpected type.");
            }
            else
            {
                _entity.Data[_key] = defaultValue;
                return defaultValue;
            }
        }

        public T GetOrCreateValue(Func<T> valueFactory)
        {
            if (_entity.Data.TryGetValue(_key, out var value))
            {
                return value is T validValue ? validValue : throw new InvalidOperationException("Entity data key mapped to unexpected type.");
            }
            else
            {
                var createdValue = valueFactory();
                _entity.Data[_key] = createdValue;
                return createdValue;
            }
        }
    }
}
