﻿using System;

namespace Recurse.Core.Maths
{
    public static class Number
    {
        /// <summary>
        /// Wraps a value between a minimum (inclusive) and maximum (exclusive).
        /// </summary>
        public static int Wrap(int value, int min, int max)
        {
            if (min >= max) throw new ArgumentException("Minimum must be less than maximum.");
            return min + Modulus.Get(value - min, max - min);
        }

        /// <summary>
        /// Wraps a value between a minimum (inclusive) and maximum (exclusive).
        /// </summary>
        public static double Wrap(double value, double min, double max)
        {
            if (min >= max) throw new ArgumentException("Minimum must be less than maximum.");
            return min + Modulus.Get(value - min, max - min);
        }

        /// <summary>
        /// Performs integer division rounding to the lowest value, rather than the value closest to zero.
        /// For example, FloorDivision(-1, 30) is -30 whereas -1 / 30 is 0.
        /// </summary>
        public static int FloorDivision(int dividend, int divisor)
        {
            return dividend / divisor - Convert.ToInt32(((dividend < 0) ^ (divisor < 0)) && (dividend % divisor != 0));
        }

        /// <summary>
        /// Gets the greatest common divisor/denominator of two numbers
        /// </summary>
        public static int GCD(int a, int b)
        {
            a = Math.Abs(a);
            b = Math.Abs(b);
            while (a != 0 && b != 0)
            {
                if (a > b)
                {
                    a %= b;
                }
                else
                {
                    b %= a;
                }
            }
            return a | b;
        }

        /// <summary>
        /// Gets index of the least significant bit in the given value (up to 31).
        /// For example, this would be 2 for values 00100, 10100, 01100, and 11100.
        /// If there is no significant bit, -1 is returned.
        /// </summary>
        public static int GetLeastSignificantBitIndex(int value)
        {
            if (value == 0)
            {
                return -1;
            }

            var leastSignificantBitIndex = 31;
            const int partition1 = unchecked((int)0b11111111111111110000000000000000);
            if ((value & partition1) != value)
            {
                value &= ~partition1;
                leastSignificantBitIndex -= 16;
            }
            const int partition2 = unchecked((int)0b11111111000000001111111100000000);
            if ((value & partition2) != value)
            {
                value &= ~partition2;
                leastSignificantBitIndex -= 8;
            }
            const int partition3 = unchecked((int)0b11110000111100001111000011110000);
            if ((value & partition3) != value)
            {
                value &= ~partition3;
                leastSignificantBitIndex -= 4;
            }
            const int partition4 = unchecked((int)0b11001100110011001100110011001100);
            if ((value & partition4) != value)
            {
                value &= ~partition4;
                leastSignificantBitIndex -= 2;
            }
            const int partition5 = unchecked((int)0b10101010101010101010101010101010);
            if ((value & partition5) != value)
            {
                leastSignificantBitIndex -= 1;
            }

            return leastSignificantBitIndex;
        }

        /// <summary>
        /// Gets index of most significant bit in the given value (up to 31).
        /// For example, this would be 2 for values 000100, 000101, 000110, and 000111.
        /// If there is no significant bit, -1 is returned.
        /// </summary>
        public static int GetMostSignificantBitIndex(int value)
        {
            if (value == 0)
            {
                return -1;
            }

            var mostSignificantBitIndex = 0;
            const int partition1 = 0b00000000000000001111111111111111;
            if ((value & partition1) != value)
            {
                value &= ~partition1;
                mostSignificantBitIndex += 16;
            }
            const int partition2 = 0b00000000111111110000000011111111;
            if ((value & partition2) != value)
            {
                value &= ~partition2;
                mostSignificantBitIndex += 8;
            }
            const int partition3 = 0b00001111000011110000111100001111;
            if ((value & partition3) != value)
            {
                value &= ~partition3;
                mostSignificantBitIndex += 4;
            }
            const int partition4 = 0b00110011001100110011001100110011;
            if ((value & partition4) != value)
            {
                value &= ~partition4;
                mostSignificantBitIndex += 2;
            }
            const int partition5 = 0b01010101010101010101010101010101;
            if ((value & partition5) != value)
            {
                mostSignificantBitIndex += 1;
            }

            return mostSignificantBitIndex;
        }
    }
}
