﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Maths
{
    public class LinearModel : IEnumerable<KeyValuePair<double, double>>
    {
        private readonly SortedList<double, double> _points = new();

        public double GetY(double x)
        {
            if (_points.Count <= 1)
            {
                throw new InvalidOperationException("At least 2 points needed for extrapolation.");
            }
            else if (_points.Keys[0] >= x)
            {
                return GetY(x, 0, 1);
            }

            var i = 1;
            while (true)
            {
                if (i == _points.Count - 1 || _points.Keys[i] >= x)
                {
                    return GetY(x, i - 1, i);
                }
                i++;
            }
        }

        public void Add(double x, double y)
        {
            _points.Add(x, y);
        }

        private double GetY(double x, int point1, int point2)
        {
            var (x1, y1) = _points.ElementAt(point1);
            var (x2, y2) = _points.ElementAt(point2);

            var a = (y2 - y1) / (x2 - x1);
            var b = y1 - a * x1;

            return a * x + b;
        }

        public IEnumerator<KeyValuePair<double, double>> GetEnumerator()
        {
            return _points.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _points.GetEnumerator();
        }
    }
}
