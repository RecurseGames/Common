﻿namespace Recurse.Core.Maths
{
    public static class Modulus
    {
        public static int Get(int dividend, int divisor)
        {
            var remainder = dividend % divisor;
            return remainder < 0 ? remainder + divisor : remainder;
        }

        public static double Get(double dividend, double divisor)
        {
            var remainder = dividend % divisor;
            return remainder < 0 ? remainder + divisor : remainder;
        }
    }
}
