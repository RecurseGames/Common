﻿using System.Collections.Generic;

namespace Recurse.Core.Extensions
{
    public static class ListExtensions
    {
        public static void UnstableRemoveAt<T>(this IList<T> list, int index)
        {
            var lastIndex = list.Count - 1;
            if (index != lastIndex)
            {
                list[index] = list[lastIndex];
                list.RemoveAt(lastIndex);
            }
            else
            {
                list.Clear();
            }
        }
    }
}
