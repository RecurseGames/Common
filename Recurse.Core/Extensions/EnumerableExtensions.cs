﻿using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T?> items)
        {
            return items.Where(item => item != null).Cast<T>();
        }
    }
}
