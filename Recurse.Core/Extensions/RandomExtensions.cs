﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Extensions
{
    public static class RandomExtensions
    {
        public static double NextGaussian(this Random rand, double mean, double standardDeviation)
        {
            // Generate random numbers in range (0,1]
            double u1 = 1.0 - rand.NextDouble();
            double u2 = 1.0 - rand.NextDouble();

            // Generate normalised deviation
            double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
            
            return mean + standardDeviation * randStdNormal;
        }

        public static double NextDouble(this Random random, double minimum, double maximum)
        {
            if (maximum < minimum)
            {
                throw new ArgumentException("Maximum cannot be less than minimum");
            }

            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        public static T Next<T>(this Random random) where T : struct, Enum
        {
            var values = Enum.GetValues<T>();

            return values[random.Next(values.Length)];
        }

        public static ArraySegment<T> Select<T>(this Random random, int count) where T : struct, Enum
        {
            var values = Enum.GetValues<T>();
            if (count == values.Length)
            {
                return new ArraySegment<T>(values);
            }
            else if (count > values.Length)
            {
                throw new ArgumentException("Enum does not have that many values.", nameof(count));
            }
            else
            {
                for (var i = 0; i < count; i++)
                {
                    var j = random.Next(i, count);
                    if (i != j)
                    {
                        (values[i], values[j]) = (values[j], values[i]);
                    }
                }
                return new ArraySegment<T>(values, 0, count);
            }
        }

        public static T UnstablePop<T>(this Random rng, IList<T> items)
        {
            if (items.Count == 0)
            {
                throw new ArgumentException("Cannot get outcome from empty list.", nameof(items));
            }
            else if (items.Count == 1)
            {
                var item = items[0];
                items.Clear();
                return item;
            }
            else
            {
                var index = rng.Next(items.Count);
                var result = items[index];
                items.UnstableRemoveAt(index);
                return result;
            }
        }

        public static T Pop<T>(this Random rng, IList<T> outcomes)
        {
            var index = rng.Next(outcomes.Count);
            var result = outcomes[index];
            outcomes.RemoveAt(index);
            return result;
        }

        public static T NextParam<T>(this Random rng, params T[] values)
        {
            return values[rng.Next(values.Length)];
        }

        public static T Next<T>(this Random rng, IReadOnlyList<T> outcomes)
        {
            return outcomes[rng.Next(outcomes.Count)];
        }

        public static T Select<T>(this Random rng, IEnumerable<T> outcomes)
        {
            var outcomeArray = outcomes.ToArray();
            return outcomeArray[rng.Next(outcomeArray.Length)];
        }

        public static T Select<T>(this Random rng, IReadOnlyCollection<T> outcomes)
        {
            return outcomes.ElementAt(rng.Next(outcomes.Count));
        }

        public static void Shuffle<T>(this Random random, IList<T> list)
        {
            var i = list.Count;
            while (i > 1)
            {
                var j = random.Next(i--);
                var shuffledElement = list[i];

                list[i] = list[j];
                list[j] = shuffledElement;
            }
        }
    }
}
