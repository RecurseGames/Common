﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Recurse.Core.Randomness.PRNGs
{
    public struct Rng
    {
        private uint _seed;

        public Rng(int seed)
        {
            _seed = (uint)seed;
        }

        public int Next()
        {
            uint z = _seed += 0x9e3779b9;
            z ^= z >> 15;
            z *= 0x85ebca6b;
            z ^= z >> 13;
            z *= 0xc2b2ae35;
            return (int)(z ^= z >> 16);
        }

        public T Next<T>(IReadOnlyCollection<T> items)
        {
            if (items.Count == 0)
            {
                throw new ArgumentException("Cannot get random element from empty collection.", nameof(items));
            }

            return items.ElementAt(Next() % items.Count);
        }

        public bool Next<T>(IReadOnlyCollection<T> items, [MaybeNullWhen(false)] out T next)
        {
            if (items.Count == 0)
            {
                next = default;
                return false;
            }

            next = items.ElementAt(Next() % items.Count);
            return true;
        }

        public override readonly string ToString()
        {
            return _seed.ToString();
        }
    }
}
