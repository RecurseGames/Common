﻿namespace Recurse.Core.Randomness.PRNGs
{
    public struct Pcg
    {
        private const int A = 1103515245;

        private const int C = 12345;

        private uint _seed;

        public Pcg(int seed)
        {
            _seed = (uint)seed;
        }

        public int Next()
        {
            _seed = (A * _seed + C);
            return (int)_seed;
        }
    }
}
