﻿using System;
using System.Collections.Generic;

namespace Recurse.Core.Maths.Randomness
{
    /// <summary>
    /// Returns random outcomes based on their weighted probabilities.
    /// Based on https://github.com/BlueRaja/Weighted-Item-Randomizer-for-C-Sharp.
    /// </summary>
    public class OutcomeGetter<T>
    {
        private readonly List<ProbabilityBox> _probabilityBoxes = new();

        private readonly int _totalProbabilityBoxWeight;

        public bool IsEmpty => _totalProbabilityBoxWeight == 0;

        public OutcomeGetter(IReadOnlyDictionary<T, int> weights)
        {
            var totalWeight = 0;
            foreach (var weight in weights.Values)
            {
                if (weight < 0)
                {
                    throw new ArgumentException("Weights cannot be negative", nameof(weights));
                }

                totalWeight += weight;
            }

            if (totalWeight == 0)
            {
                _totalProbabilityBoxWeight = 0;
            }
            else
            {
                var gcd = Number.GCD(weights.Count, totalWeight);
                var weightMultiplier = weights.Count / gcd;
                _totalProbabilityBoxWeight = totalWeight / gcd;

                DistributeWeights(weights, weightMultiplier, out var largeWeights, out var smallWeights);
                CreateSplitProbabilityBoxes(largeWeights, smallWeights);
                AddRemainingProbabilityBoxes(smallWeights);
            }
        }

        public T GetOutcome(Random random)
        {
            if (IsEmpty)
            {
                throw new InvalidOperationException("Probability of all values is zero.");
            }

            var probabilityBox = _probabilityBoxes[random.Next(_probabilityBoxes.Count)];
            int randomNumBalls = random.Next(_totalProbabilityBoxWeight) + 1;

            if (randomNumBalls <= probabilityBox.OutcomeWeight)
            {
                return probabilityBox.Outcome;
            }
            else
            {
                return probabilityBox.AlternativeOutcome;
            }
        }

        private void DistributeWeights(
            IReadOnlyDictionary<T, int> _weights,
            int weightMultiplier, 
            out Stack<KeyValuePair<T, int>> largeWeights, 
            out Stack<KeyValuePair<T, int>> smallWeights)
        {
            smallWeights = new Stack<KeyValuePair<T, int>>();
            largeWeights = new Stack<KeyValuePair<T, int>>();
            foreach (var weight in _weights)
            {
                if (weight.Value != 0)
                {
                    int newWeight = weight.Value * weightMultiplier;
                    if (newWeight > _totalProbabilityBoxWeight)
                    {
                        largeWeights.Push(new KeyValuePair<T, int>(weight.Key, newWeight));
                    }
                    else
                    {
                        smallWeights.Push(new KeyValuePair<T, int>(weight.Key, newWeight));
                    }
                }
            }
        }

        private void CreateSplitProbabilityBoxes(Stack<KeyValuePair<T, int>> largeWeights, Stack<KeyValuePair<T, int>> smallWeights)
        {
            while (largeWeights.Count != 0)
            {
                var largeWeight = largeWeights.Pop();
                var smallWeight = smallWeights.Pop();
                _probabilityBoxes.Add(new ProbabilityBox(smallWeight.Key, largeWeight.Key, smallWeight.Value));

                int difference = _totalProbabilityBoxWeight - smallWeight.Value;
                var newWeight = largeWeight.Value - difference;
                if (newWeight > _totalProbabilityBoxWeight)
                {
                    largeWeights.Push(new KeyValuePair<T, int>(largeWeight.Key, newWeight));
                }
                else
                {
                    smallWeights.Push(new KeyValuePair<T, int>(largeWeight.Key, newWeight));
                }
            }
        }

        private void AddRemainingProbabilityBoxes(Stack<KeyValuePair<T, int>> smallWeights)
        {
            while (smallWeights.Count != 0)
            {
                var smallItem = smallWeights.Pop();

                _probabilityBoxes.Add(new ProbabilityBox(smallItem.Key, smallItem.Key, _totalProbabilityBoxWeight));
            }
        }

        private struct ProbabilityBox
        {
            public T Outcome { get; private set; }

            public T AlternativeOutcome { get; private set; }

            public int OutcomeWeight { get; private set; }

            public ProbabilityBox(T outcome, T alternativeOutcome, int outcomeWeight)
            {
                Outcome = outcome;
                AlternativeOutcome = alternativeOutcome;
                OutcomeWeight = outcomeWeight;
            }
        }
    }
}