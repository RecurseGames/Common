﻿using System;

namespace Recurse.Core.Randomness
{
    public struct Probability
    {
        private double Value { get; }

        /// <summary>
        /// The probability an event occurs after its average delay
        /// </summary>
        public static Probability AverageDelay = 0.554; // Value determined empirically

        public Probability(double value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public static implicit operator Probability(double probability)
        {
            if (probability >= 0 && probability <= 1)
            {
                return new Probability(probability);
            }
            else
            {
                throw new InvalidCastException("Probability must be between 0 and 1 (inclusive).");
            }
        }

        public static implicit operator double(Probability probability)
        {
            return probability.Value;
        }
    }
}
