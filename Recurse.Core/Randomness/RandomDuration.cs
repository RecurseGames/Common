﻿using System;

namespace Recurse.Core.Randomness
{
    public struct RandomDuration
    {
        private double BaseDuration { get; }

        private Probability BaseDurationProbability { get; }

        public RandomDuration(double baseDuration, Probability baseDurationProbability)
        {
            BaseDuration = baseDuration;
            BaseDurationProbability = baseDurationProbability;
        }

        public double CreateDuration(RandomElement randomElement)
        {
            return Math.Log(1 - randomElement.ToDouble()) / Math.Log(1 - BaseDurationProbability) * BaseDuration;
        }
    }
}
