﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

namespace Recurse.Core.Maths.Randomness
{
    [Serializable]
    public class OutcomeWeighting<T> : IReadOnlyDictionary<T, int> where T : notnull
    {
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        private readonly Dictionary<T, int> _implementation = new();

        public int this[T outcome]
        { 
            get => _implementation[outcome];
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Weight must not be negative");
                }
                else if (value == 0)
                {
                    _implementation.Remove(outcome);
                }
                else
                {
                    _implementation[outcome] = value;
                }
            }
        }

        public void Increment(T outcome, int weight)
        {
            if (weight < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(weight), "Weight must not be negative");
            }
            else if (weight == 0)
            {
                // Do nothing
            }
            else if (TryGetValue(outcome, out var existingWeight))
            {
                this[outcome] = existingWeight + weight;
            }
            else
            {
                this[outcome] = weight;
            }
        }

        public void Clear()
        {
            _implementation.Clear();
        }

        public bool Remove(T outcome)
        {
            return _implementation.Remove(outcome);
        }

        #region IReadOnlyDictionary Implementation

        int IReadOnlyDictionary<T, int>.this[T outcome]
        {
            get
            {
                if (!_implementation.ContainsKey(outcome))
                {
                    return _implementation[outcome];
                }
                else
                {
                    throw new KeyNotFoundException();
                }
            }
        }

        public IEnumerable<T> Keys => ((IReadOnlyDictionary<T, int>)_implementation).Keys;

        public IEnumerable<int> Values => ((IReadOnlyDictionary<T, int>)_implementation).Values;

        public int Count => _implementation.Count;

        public bool ContainsKey(T outcome)
        {
            return _implementation.ContainsKey(outcome);
        }

        public IEnumerator<KeyValuePair<T, int>> GetEnumerator()
        {
            return _implementation.GetEnumerator();
        }

        public bool TryGetValue(T outcome, [MaybeNullWhen(false)] out int weight)
        {
            return _implementation.TryGetValue(outcome, out weight);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _implementation.GetEnumerator();
        }

        #endregion
    }
}