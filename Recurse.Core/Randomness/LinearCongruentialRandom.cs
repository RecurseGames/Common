﻿namespace Recurse.Core.Maths.Randomness
{
    public class LinearCongruentialRandom
    {
        private const int A = 1103515245, B = 12345;

        private int _previousValue;

        public int Next()
        {
            return _previousValue = GetNext(_previousValue);
        }

        public static int GetNext(int previousValue)
        {
            return unchecked(A * previousValue + B);
        }
    }
}