﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Recurse.Core.Randomness
{
    /// <summary>
    /// Represents data used as a source of randomness
    /// to generate random values.
    /// </summary>
    public struct RandomElement
    {
        private static Dictionary<string, int> FileInstances { get; } = new Dictionary<string, int>();

        private static Dictionary<Type, RandomElement> TypeRandomElements { get; } = new Dictionary<Type, RandomElement>();

        private uint Data { get; }

        private uint KeyCount { get; }

        public RandomElement Next => new RandomElement(this, 0xde4ed8ae);

        public RandomElement(params RandomElement[] elements) : this(0x779ca895, elements)
        {

        }

        /// <summary>
        /// Creates a random element using data from a seed value and a string.
        /// </summary>
        public RandomElement(uint seed, string s) : this(seed, Encoding.UTF8.GetBytes(s))
        {

        }

        public RandomElement(uint data, params RandomElement[] elements)
        {
            if (elements.Length == 0)
            {
                throw new ArgumentException("At least one element required.", nameof(elements));
            }

            Data = data;
            KeyCount = (uint)elements.Length + 1;

            foreach (var element in elements)
            {
                if (element.KeyCount == 0)
                {
                    var key = CreateKey(element.Data);

                    Data = CreateData(Data, key);
                }
                else
                {
                    Data = CreateData(Data, element.Data);
                }
            }
        }

        private RandomElement(uint data)
        {
            Data = data;
            KeyCount = 1;
        }

        private RandomElement(RandomElement predecessor, uint key)
        {
            Data = CreateData(predecessor.Data, key);
            KeyCount = 2;
        }

        private RandomElement(uint data, params byte[] keyBytes)
        {
            Data = data;
            KeyCount = (uint)keyBytes.Length / 4;

            var i = 0;
            while (i < keyBytes.Length - 4)
            {
                var keySource = BitConverter.ToUInt32(keyBytes, i);
                var key = CreateKey(keySource);

                Data = CreateData(Data, key);

                i += 4;
            }

            if (i < keyBytes.Length)
            {
                var lastKeySourceBytes = new byte[4];
                Array.Copy(keyBytes, i, lastKeySourceBytes, 0, keyBytes.Length - i);
                var lastKeySource = BitConverter.ToUInt32(lastKeySourceBytes);
                var lastKey = CreateKey(lastKeySource);

                Data = CreateData(Data, lastKey);
                KeyCount++;
            }
        }

        /// <summary>
        /// Creates a random element associated with the calling file.
        /// This allows files to generate random data unique to that
        /// particular file.
        /// </summary>
        public static RandomElement FromCallingFile([CallerFilePath] string? callerFilePath = null)
        {
            if (callerFilePath == null)
            {
                throw new ArgumentNullException(nameof(callerFilePath));
            }

            var key = $"{callerFilePath}";
            if (!FileInstances.TryGetValue(key, out var fileInstance))
            {
                fileInstance = new RandomElement(0x50007eba, key).ToInt();
            }
            return fileInstance;
        }

        /// <summary>
        /// Creates a random element associated with the calling file.
        /// This allows files to generate random data unique to that
        /// particular file.
        /// </summary>
        public static RandomElement FromCallingFile(RandomElement element, [CallerFilePath] string? callerFilePath = null)
        {
            var callingFileRandomElement = FromCallingFile(callerFilePath);

            return new RandomElement(callingFileRandomElement.Data, element);
        }

        /// <summary>
        /// Creates a random element associated with the calling file.
        /// This allows files to generate random data unique to that
        /// particular file.
        /// </summary>
        public static RandomElement FromCallingFile(RandomElement element1, RandomElement element2, [CallerFilePath] string? callerFilePath = null)
        {
            var callingFileRandomElement = FromCallingFile(callerFilePath);

            return new RandomElement(callingFileRandomElement.Data, element1, element2);
        }

        public static implicit operator RandomElement(bool? value)
        {
            return value switch
            {
                true => new RandomElement(0x2551ef2fu),
                false => new RandomElement(0x7ce49d67u),
                null => new RandomElement(0x66a25184u),
            };
        }

        public static implicit operator RandomElement(double? value)
        {
            return value != null
                ? new RandomElement(CreateKey(unchecked((uint)value + 0xe96470f4u)))
                : new RandomElement(0xf1557be9u);
        }

        public static implicit operator RandomElement(int? value)
        {
            return value != null
                ? new RandomElement(CreateKey(unchecked((uint)value + 0x6a6f923a)))
                : new RandomElement(0x1437f957u);
        }

        public static implicit operator RandomElement(string? value)
        {
            return value != null
                ? new RandomElement(0xed922252, Encoding.UTF8.GetBytes(value))
                : new RandomElement(0x25560a82u);
        }

        public static implicit operator RandomElement(Type type)
        {
            if (type.FullName == null)
            {
                throw new ArgumentException("Type must have a name", nameof(type));
            }

            if (!TypeRandomElements.TryGetValue(type, out var randomElementForType))
            {
                randomElementForType = new RandomElement(0x5b5aa27eu, Encoding.UTF8.GetBytes(type.FullName));
                TypeRandomElements[type] = randomElementForType;
            }
            return randomElementForType;
        }

        /// <summary>
        /// Gets an object of the given type, generated using the given seed,
        /// as a <see cref="RandomElement"/>.
        /// </summary>
        public static RandomElement FromObject(object obj, params RandomElement[] seedElements)
        {
            var typeData = new RandomElement(0x71d1fcc3, obj.GetType()).Data;

            return new RandomElement(typeData, seedElements);
        }

        /// <summary>
        /// Creates a random number from 0 (inclusive) to 1 (exclusive).
        /// The number will always be the same no matter how many times this
        /// method is called.
        /// </summary>
        public double ToDouble()
        {
            return Convert.ToDouble(GetFinalisedData()) / (Convert.ToDouble(uint.MaxValue) + 1);
        }

        /// <summary>
        /// Creates a random number from the set of all possible integers.
        /// The number will always be the same no matter how many times this
        /// method is called.
        /// </summary>
        public int ToInt()
        {
            return unchecked((int)GetFinalisedData());
        }

        /// <summary>
        /// Creates a random element from a set.
        /// The element index will always be the same no matter how many times this
        /// method is called.
        /// </summary>
        public T ToElement<T>(IReadOnlyList<T> elements)
        {
            if (elements.Count == 0)
            {
                throw new ArgumentException("Cannot get random element from an empty list.", nameof(elements));
            }

            return elements[Math.Abs(ToInt()) % elements.Count];
        }

        /// <summary>
        /// Creates a random element from a set.
        /// The element index will always be the same no matter how many times this
        /// method is called.
        /// </summary>
        public T ToElement<T>(params T[] elements)
        {
            if (elements.Length == 0)
            {
                throw new ArgumentException("Cannot get random element from an empty list.", nameof(elements));
            }

            return elements[Math.Abs(ToInt()) % elements.Length];
        }

        /// <summary>
        /// Creates a random number generator. It will always have the same
        /// seed no matter how many times this method is called.
        /// </summary>
        public Random ToRandom()
        {
            int seed = unchecked((int)(0xbdc6fb9b + GetFinalisedData()));

            return new Random(seed);
        }

        private uint GetFinalisedData()
        {
            var finalisedData = Data ^ KeyCount;
            finalisedData ^= finalisedData >> 16;
            finalisedData *= 0x85ebca6b;
            finalisedData ^= finalisedData >> 13;
            finalisedData *= 0xc2b2ae35;
            finalisedData ^= finalisedData >> 16;
            return finalisedData;
        }

        private static uint CreateKey(uint rawData)
        {
            rawData *= 0xcc9e2d51;
            rawData = RotateBitsLeft(rawData, 15);
            rawData *= 0x1b873593;
            return rawData;
        }

        private static uint CreateData(uint data, uint key)
        {
            data ^= key;
            data = RotateBitsLeft(data, 13);
            data = data * 5 + 0xe6546b64;
            return data;
        }

        private static uint RotateBitsLeft(uint bytes, byte offset)
        {
            return bytes << offset | bytes >> 32 - offset;
        }
    }
}
