﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Recurse.Core.Collections
{
    /// <summary>
    /// A set which caches hashcodes of its items
    /// </summary>
    /// <remarks>
    /// This class is intended for cases where comparing items can be quite
    /// expensive (e.g., sets of images).
    /// </remarks>
    public class CacheSet<T> : IReadOnlyCollection<T>
    {
        private readonly HashSet<Node> _nodes;

        private readonly IEqualityComparer<T> _comparer;

        public int Count => _nodes.Count;

        public CacheSet(IEqualityComparer<T> comparer)
        {
            _comparer = comparer;
            _nodes = new(new NodeComparer(comparer));
        }

        public bool TryGetValue([DisallowNull] T equalValue, [MaybeNullWhen(false)] out T actualValue)
        {
            var equalNode = new Node(equalValue, _comparer.GetHashCode(equalValue));
            if (_nodes.TryGetValue(equalNode, out var actualNode))
            {
                actualValue = actualNode.Element;
                return true;
            }
            else
            {
                actualValue = default;
                return false;
            }
        }

        public bool Add([DisallowNull] T item)
        {
            return _nodes.Add(new Node(item, _comparer.GetHashCode(item)));
        }

        public bool Remove([DisallowNull] T item)
        {
            return _nodes.Remove(new Node(item, _comparer.GetHashCode(item)));
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _nodes.Select(node => node.Element).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _nodes.Select(node => node.Element).GetEnumerator();
        }

        private record Node(T Element, int ElementHashCode);

        private class NodeComparer : IEqualityComparer<Node>
        {
            private readonly IEqualityComparer<T> _elementComparer;

            public NodeComparer(IEqualityComparer<T> elementComparer)
            {
                _elementComparer = elementComparer;
            }

            public bool Equals(Node? x, Node? y)
            {
#pragma warning disable CS8602 // Dereference of a possibly null reference.
                return _elementComparer.Equals(x.Element, y.Element);
#pragma warning restore CS8602 // Dereference of a possibly null reference.
            }

            public int GetHashCode([DisallowNull] Node obj)
            {
                return obj.ElementHashCode;
            }
        }
    }
}