﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Recurse.Core.Collections
{
    [Serializable]
    public abstract class LazyCollection<T> : ICollection<T>
    {
        private static readonly ICollection<T> _emptyCollection = new ReadOnlyCollection<T>(new List<T>());

        public int Count => GetImplementation()?.Count ?? 0;

        public bool IsReadOnly => GetReadOnlyImplementation()?.IsReadOnly ?? false;

        private ICollection<T> GetReadOnlyImplementation() => GetImplementation() ?? _emptyCollection;

        protected abstract ICollection<T> GetOrCreateImplementation();

        protected abstract ICollection<T>? GetImplementation();

        public void Add(T item)
        {
            GetOrCreateImplementation().Add(item);
        }

        public void Clear()
        {
            GetImplementation()?.Clear();
        }

        public bool Contains(T item)
        {
            return GetImplementation()?.Contains(item) ?? false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            GetReadOnlyImplementation().CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return GetImplementation()?.Remove(item) ?? false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return GetReadOnlyImplementation().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetReadOnlyImplementation().GetEnumerator();
        }
    }
}
