﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;

namespace Recurse.Core.Collections
{
    [Serializable]
    public abstract class LazyDictionary<TKey, TValue> : IDictionary<TKey, TValue> where TKey : notnull
    {
        private static readonly IDictionary<TKey, TValue> _emptyDictionary = new ReadOnlyDictionary<TKey, TValue>(new Dictionary<TKey, TValue>());

        public int Count => GetImplementation()?.Count ?? 0;

        public IEnumerable<TKey> Keys => GetReadOnlyImplementation().Keys;

        public IEnumerable<TValue> Values => GetReadOnlyImplementation().Values;

        public TValue this[TKey key] => GetReadOnlyImplementation()[key];

        private IDictionary<TKey, TValue> GetReadOnlyImplementation() => GetImplementation() ?? _emptyDictionary;

        protected abstract IDictionary<TKey, TValue> GetOrCreateImplementation();

        protected abstract IDictionary<TKey, TValue>? GetImplementation();

        ICollection<TKey> IDictionary<TKey, TValue>.Keys => GetImplementation()?.Keys ?? new KeyCollection(this);

        ICollection<TValue> IDictionary<TKey, TValue>.Values => GetImplementation()?.Values ?? new ValueCollection(this);

        public bool IsReadOnly => GetReadOnlyImplementation()?.IsReadOnly ?? false;

        TValue IDictionary<TKey, TValue>.this[TKey key]
        {
            get => GetReadOnlyImplementation()[key]; 
            set => GetOrCreateImplementation()[key] = value; 
        }

        public bool ContainsKey(TKey key)
        {
            return GetImplementation()?.ContainsKey(key) ?? false;
        }

        public bool TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value)
        {
            return GetReadOnlyImplementation().TryGetValue(key, out value);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return GetReadOnlyImplementation().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetReadOnlyImplementation().GetEnumerator();
        }

        public void Add(TKey key, TValue value)
        {
            GetOrCreateImplementation().Add(key, value);
        }

        public bool Remove(TKey key)
        {
            return GetImplementation()?.Remove(key) ?? false;
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            GetOrCreateImplementation().Add(item);
        }

        public void Clear()
        {
            GetImplementation()?.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return GetImplementation()?.Contains(item) ?? false;
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            GetReadOnlyImplementation().CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return GetImplementation()?.Remove(item) ?? false;
        }

        [Serializable]
        private class KeyCollection : LazyCollection<TKey>
        {
            private readonly LazyDictionary<TKey, TValue> _dictionary;

            public KeyCollection(LazyDictionary<TKey, TValue> dictionary)
            {
                _dictionary = dictionary;
            }

            protected override ICollection<TKey>? GetImplementation()
            {
                return _dictionary.GetImplementation()?.Keys;
            }

            protected override ICollection<TKey> GetOrCreateImplementation()
            {
                return _dictionary.GetOrCreateImplementation().Keys;
            }
        }

        [Serializable]
        private class ValueCollection : LazyCollection<TValue>
        {
            private readonly LazyDictionary<TKey, TValue> _dictionary;

            public ValueCollection(LazyDictionary<TKey, TValue> dictionary)
            {
                _dictionary = dictionary;
            }

            protected override ICollection<TValue>? GetImplementation()
            {
                return _dictionary.GetImplementation()?.Values;
            }

            protected override ICollection<TValue> GetOrCreateImplementation()
            {
                return _dictionary.GetOrCreateImplementation().Values;
            }
        }
    }
}
