﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Recurse.Core.Collections
{
    public class TwoWayDictionary<TKey, TValue> : IDictionary<TKey, TValue>
        where TKey : notnull
        where TValue : notnull
    {
        private readonly IDictionary<TKey, TValue> _values = new Dictionary<TKey, TValue>();

        private readonly Dictionary<TValue, TKey> _inverse = new();

        public ICollection<TKey> Keys => _values.Keys;

        public ICollection<TValue> Values => _values.Values;

        public bool IsReadOnly => false;

        public int Count => _values.Count;

        public IReadOnlyDictionary<TValue, TKey> Inverse => _inverse;

        public TValue this[TKey key]
        {
            get { return _values[key]; }
            set { _values[key] = value; }
        }

        public void Add(TKey key, TValue value)
        {
            if (_values.ContainsKey(key))
            {
                throw new ArgumentException("Key already exists.", nameof(key));
            }
            else if (_inverse.ContainsKey(value))
            {
                throw new ArgumentException("Value already exists.", nameof(value));
            }

            _values[key] = value;
            _inverse[value] = key;
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            if (_values.ContainsKey(item.Key))
            {
                throw new ArgumentException("Key already exists.", nameof(item));
            }
            else if (_inverse.ContainsKey(item.Value))
            {
                throw new ArgumentException("Value already exists.", nameof(item));
            }

            _values[item.Key] = item.Value;
            _inverse[item.Value] = item.Key;
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            if (_values.Remove(item))
            {
                _inverse.Remove(item.Value);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Remove(TKey key)
        {
            if (_values.TryGetValue(key, out var value))
            {
                _values.Remove(key);
                _inverse.Remove(value);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value)
        {
            return _values.TryGetValue(key, out value);
        }

        public void Clear()
        {
            _values.Clear();
            _inverse.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _values.Contains(item);
        }

        public bool ContainsKey(TKey key)
        {
            return _values.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _values.CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _values.GetEnumerator();
        }
    }
}
