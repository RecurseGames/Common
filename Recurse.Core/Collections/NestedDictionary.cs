﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Recurse.Core.Collections
{
    [DebuggerDisplay("Count = {Count}")]
    public class NestedDictionary<TKey1, TKey2, TValue> where TKey1 : notnull where TKey2 : notnull
    {
        private readonly Dictionary<TKey1, Dictionary<TKey2, TValue>> _elements = new();

        public IEnumerable<TValue> Values => _elements.SelectMany(e => e.Value.Values);

        public IEnumerable<TKey1> FirstKeys => _elements.Keys;

        public int Count => _elements.Sum(e => e.Value.Count);

        [return: MaybeNull]
        public TValue GetValue(TKey1 key1, TKey2 key2)
        {
            if (_elements.TryGetValue(key1, out var values) && values.TryGetValue(key2, out var value))
            {
                return value;
            }
            else
            {
                return default;
            }
        }

        public TValue SetDefault(TKey1 key1, TKey2 key2, TValue defaultValue)
        {
            if (!_elements.TryGetValue(key1, out var values))
            {
                values = new();
                _elements[key1] = values;
            }

            if (!values.TryGetValue(key2, out var value))
            {
                value = defaultValue;
                values[key2] = value;
            }

            return value;
        }

        public TValue GetValue(TKey1 key1, TKey2 key2, Func<TValue> createValue)
        {
            if (!_elements.TryGetValue(key1, out var values))
            {
                values = new();
                _elements[key1] = values;
            }

            if (!values.TryGetValue(key2, out var value))
            {
                value = createValue();
                values[key2] = value;
            }

            return value;
        }

        public bool TryGetValue(TKey1 key1, TKey2 key2, [MaybeNullWhen(false)] out TValue value)
        {
            if (_elements.TryGetValue(key1, out var values))
            {
                return values.TryGetValue(key2, out value);
            }
            else
            {
                value = default;
                return false;
            }
        }

        public void SetValue(TKey1 key1, TKey2 key2, TValue value)
        {
            if (!_elements.TryGetValue(key1, out var values))
            {
                values = new();
                _elements[key1] = values;
            }
            values[key2] = value;
        }

        public IEnumerable<KeyValuePair<TKey2, TValue>> GetElements(TKey1 key1)
        {
            if (_elements.TryGetValue(key1, out var values))
            {
                return values;
            }
            else
            {
                return Enumerable.Empty<KeyValuePair<TKey2, TValue>>();
            }
        }
    }
}
