﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Collections
{
    [Serializable]
    public sealed class EquatableSet<T> : IEquatable<EquatableSet<T>>, IReadOnlySet<T>
    {
        [NonSerialized]
        private int? _hashCode;

        private HashSet<T> Elements { get; }

        public int Count => Elements.Count;

        public bool IsReadOnly => false;

        private EquatableSet(int hashCode, HashSet<T> elements)
        {
            _hashCode = hashCode;
            Elements = elements;
        }

        public EquatableSet()
        {
            _hashCode = 0;
            Elements = new();
        }

        public EquatableSet(IEnumerable<T> elements)
        {
            Elements = new(elements);
        }

        public EquatableSet<T> GetUnion(IEnumerable<T> other)
        {
            var unionElements = new HashSet<T>(Elements);
            var unionHashCode = GetHashCode();
            var isDifferent = false;
            foreach (var element in other)
            {
                if (unionElements.Add(element))
                {
                    isDifferent = true;
                    unionHashCode = GetHashCode(unionHashCode, GetHashCode(element));
                }
            }

            return isDifferent ? new EquatableSet<T>(unionHashCode, unionElements) : this;
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as EquatableSet<T>);
        }

        public bool Equals(EquatableSet<T>? other)
        {
            return other == this || other != null && Elements.SetEquals(other.Elements);
        }

        public override int GetHashCode()
        {
            return _hashCode ??= Elements.Select(GetHashCode).DefaultIfEmpty(0).Aggregate(GetHashCode);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Elements.CopyTo(array, arrayIndex);
        }

        public bool Contains(T item)
        {
            return Elements.Contains(item);
        }

        public bool IsProperSubsetOf(IEnumerable<T> other)
        {
            return Elements.IsProperSubsetOf(other);
        }

        public bool IsProperSupersetOf(IEnumerable<T> other)
        {
            return Elements.IsProperSupersetOf(other);
        }

        public bool IsSubsetOf(IEnumerable<T> other)
        {
            return Elements.IsSubsetOf(other);
        }

        public bool IsSupersetOf(IEnumerable<T> other)
        {
            return Elements.IsSupersetOf(other);
        }

        public bool Overlaps(IEnumerable<T> other)
        {
            return Elements.Overlaps(other);
        }

        public bool SetEquals(IEnumerable<T> other)
        {
            return Elements.SetEquals(other);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Elements.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Elements.GetEnumerator();
        }

        private static int GetHashCode(T obj)
        {
            // HashCode.Combine ensures integers are more thoroughly hashed, reducing collisions
            return obj != null ? HashCode.Combine(obj.GetHashCode()) : 0;
        }

        private static int GetHashCode(int initialHashCode, int addedOrRemovedHashCode)
        {
            // XOR ensures ordering doesn't matter, and that we can use this method for addition and removal
            return initialHashCode ^ addedOrRemovedHashCode;
        }
    }
}
