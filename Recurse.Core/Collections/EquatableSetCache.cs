﻿using System;
using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    [Serializable]
    public class EquatableSetCache<T>
    {
        private readonly Dictionary<EquatableSet<T>, EquatableSet<T>> _sets = new();

        private readonly Dictionary<EquatableSet<EquatableSet<T>>, EquatableSet<T>> _unions = new();

        public EquatableSet<T> GetCached(IEnumerable<T> items)
        {
            if (items is EquatableSet<T> set)
            {
                return GetCached(set);
            }
            else
            {
                set = new EquatableSet<T>(items);
                _sets[set] = set;
                return set;
            }
        }

        public EquatableSet<T> GetCached(EquatableSet<T> set)
        {
            if (_sets.TryGetValue(set, out var cachedSet))
            {
                return cachedSet;
            }
            else
            {
                _sets[set] = set;
                return set;
            }
        }

        public EquatableSet<T> GetUnion(EquatableSet<T> a, IEnumerable<T> b)
        {
            var cachedA = GetCached(a);
            var cachedB = GetCached(b);
            var key = new EquatableSet<EquatableSet<T>>(new[] { cachedA, cachedB });
            if (!_unions.TryGetValue(key, out var cachedUnion))
            {
                var union = cachedA.GetUnion(cachedB);
                cachedUnion = GetCached(union);
                _unions[key] = cachedUnion;
            }
            return cachedUnion;
        }
    }
}
