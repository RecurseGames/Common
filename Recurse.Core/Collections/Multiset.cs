﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Recurse.Core.Collections
{
    [DebuggerDisplay("Count = {Count}")]
    public class Multiset<T> : IReadOnlyList<T>, IReadOnlyCollection<T>, IEnumerable<T> where T : notnull
    {
        private readonly Dictionary<T, int> _counts;

        public int Count { get; private set; }

        public IReadOnlyDictionary<T, int> Counts => _counts;

        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= Count)
                {
                    throw new IndexOutOfRangeException();
                }

                return _counts.First(count => 
                {
                    if (index < count.Value)
                    {
                        return true;
                    }
                    else
                    {
                        index -= count.Value;
                        return false;
                    }
                }).Key;
            }
        }

        public Multiset()
        {
            _counts = new();
        }

        public Multiset(IEqualityComparer<T>? comparer)
        {
            _counts = new(comparer);
        }

        public bool Contains(T item)
        {
            return _counts.ContainsKey(item);
        }

        public void Add(T value)
        {
            Add(value, 1);
        }

        public void Add(T value, int addedCount)
        {
            _counts[value] = _counts.TryGetValue(value, out var count)
                ? count + addedCount
                : addedCount;
            Count += addedCount;
        }

        public bool Remove(T value)
        {
            return Remove(value, 1);
        }

        public bool Remove(T item, int removedCount)
        {
            if (_counts.TryGetValue(item, out var count))
            {
                if (count > removedCount)
                {
                    _counts[item] = count - removedCount;
                    Count -= removedCount;
                }
                else
                {
                    _counts.Remove(item);
                    Count -= count;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Clear()
        {
            _counts.Clear();
            Count = 0;
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var itemCount in _counts)
            {
                for (var i = 0; i < itemCount.Value; i++)
                {
                    yield return itemCount.Key;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
