﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    public class ShuffledCollection<T> : ICollection<T>, IEnumerable<T>
    {
        private readonly Random _random;

        private readonly IList<T> _items = new List<T>();

        public int Count => _items.Count;

        public bool IsReadOnly => _items.IsReadOnly;

        public ShuffledCollection(Random random)
        {
            _random = random;
        }

        public void Add(T item)
        {
            var index = _random.Next(_items.Count + 1);
            if (index == _items.Count)
            {
                _items.Add(item);
            }
            else
            {
                var shuffledItem = _items[index];

                _items[index] = item;
                _items.Add(shuffledItem);
            }
        }

        public bool Remove(T item)
        {
            return _items.Remove(item);
        }

        public void Clear()
        {
            _items.Clear();
        }

        public bool Contains(T item)
        {
            return _items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _items.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }
    }
}
