﻿using Recurse.Core.Randomness.PRNGs;
using System;
using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    public class HashCodeComparer<T> : IComparer<T>, IDisposable
    {
        private Dictionary<T, int>? _keys = new();

        private Pcg _keySource;

        public HashCodeComparer()
        {
            _keySource = new(0x22e71c6b);
        }

        public HashCodeComparer(int seed)
        {
            _keySource = new(seed);
        }

        public int Compare(T? x, T? y)
        {
            if (_keys == null)
            {
                throw new InvalidOperationException($"Comparer has been disposed.");
            }
            else if (x == null)
            {
                return y == null ? 0 : -1;
            }
            else if (y == null)
            {
                return 1;
            }
            else
            {
                var hashCodeComparison = x.GetHashCode().CompareTo(y.GetHashCode());
                if (hashCodeComparison != 0)
                {
                    return hashCodeComparison;
                }
                else
                {
                    return GetOrCreateKey(x).CompareTo(GetOrCreateKey(y));
                }
            }
        }

        public void Dispose()
        {
            _keys = null;
        }

        private int GetOrCreateKey(T item)
        {
            if (item == null)
            {
                return 0;
            }
            
            if (!_keys!.TryGetValue(item, out var key))
            {
                key = _keySource.Next();
                _keys[item] = key;
            }
            return key;
        }
    }
}
