﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Collections
{
    public class ShrinkingFlatSet<T> : IShrinkingSet<T> where T : notnull
    {
        private readonly ShrinkingSetCache<T> _cache;

        private readonly HashSet<T> _items;

        public int ItemHash { get; }

        public int ItemHashMask => ~0;

        public int Count => _items.Count;

        public ShrinkingFlatSet(ShrinkingSetCache<T> cache, IReadOnlySet<T> items)
        {
            if (items.Count <= 1)
            {
                throw new ArgumentException($"Can only store multiple items", nameof(items));
            }

            _cache = cache;
            _items = new(items);
            ItemHash = items.First().GetHashCode();

            if (_items.Skip(1).Any(item => item.GetHashCode() != ItemHash))
            {
                throw new ArgumentException($"Can only store items with same hash", nameof(items));
            }
        }

        public bool Contains(T item)
        {
            return item.GetHashCode() == ItemHash && _items.Contains(item);
        }

        public IShrinkingSet<T> GetDifference(IShrinkingSet<T> other)
        {
            if (other.Count == 0)
            {
                return this;
            }
            else if (Equals(this, other))
            {
                return _cache.GetEmpty();
            }
            else if (!other.MayContain(ItemHash))
            {
                return this;
            }
            else if (other.Count != 1)
            {
                var items = new HashSet<T>(_items);
                items.ExceptWith(other);
                return items.Count != Count ? _cache.GetLeafSet(items) : this;
            }
            else if (_items.Contains(other.First()))
            {
                var items = new HashSet<T>(_items);
                items.Remove(other.First());
                return _cache.GetLeafSet(items);
            }
            else
            {
                return this;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }
    }
}
