﻿using System.Collections;
using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    public class ShrinkingEmptySet<T> : IShrinkingSet<T> where T : notnull
    {
        public int Count => 0;

        public int ItemHash => 0;

        public int ItemHashMask => 0;

        public bool Contains(T item)
        {
            return false;
        }

        public IShrinkingSet<T> GetDifference(IShrinkingSet<T> other)
        {
            return this;
        }

        public IEnumerator<T> GetEnumerator()
        {
            yield break;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            yield break;
        }
    }
}
