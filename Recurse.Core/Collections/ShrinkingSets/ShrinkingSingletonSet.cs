﻿using System.Collections;
using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    public class ShrinkingSingletonSet<T> : IShrinkingSet<T> where T : notnull
    {
        private readonly ShrinkingSetCache<T> _cache;

        private readonly T _item;

        public int Count => 1;

        public int ItemHash => _item.GetHashCode();

        public int ItemHashMask => ~0;

        public ShrinkingSingletonSet(ShrinkingSetCache<T> cache, T item)
        {
            _cache = cache;
            _item = item;
        }

        public bool Contains(T item)
        {
            return Equals(_item, item);
        }

        public IShrinkingSet<T> GetDifference(IShrinkingSet<T> other)
        {
            if (other.Count == 0)
            {
                return this;
            }
            else if (other.Contains(_item))
            {
                return _cache.GetEmpty();
            }
            else
            {
                return this;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            yield return _item;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            yield return _item;
        }
    }
}
