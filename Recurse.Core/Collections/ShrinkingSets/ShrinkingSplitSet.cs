﻿using Recurse.Core.Maths;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    public class ShrinkingSplitSet<T> : IShrinkingSet<T> where T : notnull
    {
        public const int MaxDiscriminatorIndex = 31;

        private readonly Dictionary<IShrinkingSet<T>, IShrinkingSet<T>> _differenceCache = new();

        private readonly ShrinkingSetCache<T> _cache;

        private readonly IShrinkingSet<T> _left;

        private readonly IShrinkingSet<T> _right;

        public int Count { get; }

        public int ItemHash { get; }

        public int ItemHashMask { get; }
        
        public ShrinkingSplitSet(ShrinkingSetCache<T> cache, IShrinkingSet<T> subset1, IShrinkingSet<T> subset2)
        {
            var discriminatorBit = Number.GetLeastSignificantBitIndex(subset1.ItemHash ^ subset2.ItemHash);
            if (discriminatorBit == -1)
            {
                throw new ArgumentException("Subsets have same item hash");
            }

            var discrimination = GetDiscrimination(discriminatorBit, subset1.ItemHash);

            _cache = cache;
            (_left, _right) = discrimination ? (subset1, subset2) : (subset2, subset1);
            Count = subset1.Count + subset2.Count;
            ItemHashMask = discriminatorBit == -1 ? ~0 : ((1 << discriminatorBit) - 1);
            ItemHash = subset1.ItemHash & ItemHashMask;
        }

        public static bool GetDiscrimination(int discriminatorBit, int hash)
        {
            var mask = 1 << discriminatorBit;
            return (hash & mask) == 0;
        }

        public bool Contains(T item)
        {
            if (_left.MayContain(item))
            {
                return _left.Contains(item);
            }
            else if (_right.MayContain(item))
            {
                return _right.Contains(item);
            }
            else
            {
                return false;
            }
        }

        public IShrinkingSet<T> GetDifference(IShrinkingSet<T> other)
        {
            if (other.Count == 0)
            {
                return this;
            }
            else if (Equals(other, this))
            {
                return _cache.GetEmpty();
            }
            else if (!other.MayOverlap(this))
            {
                return this;
            }
            else if (_differenceCache.TryGetValue(other, out var cachedDifference))
            {
                return cachedDifference;
            }
            else
            {
                var left = other.MayOverlap(_left) ? _left.GetDifference(other) : _left;
                var right = other.MayOverlap(_right) ? _right.GetDifference(other) : _right;
                if (Equals(_left, left) && Equals(_right, right))
                {
                    return _differenceCache[other] = this;
                }
                else if (left.Count == 0)
                {
                    return right;
                }
                else if (right.Count == 0)
                {
                    return left;
                }
                else
                {
                    return _differenceCache[other] = _cache.GetSplitSet(left, right);
                }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var item in _left)
            {
                yield return item;
            }
            foreach (var item in _right)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (var item in _left)
            {
                yield return item;
            }
            foreach (var item in _right)
            {
                yield return item;
            }
        }
    }
}
