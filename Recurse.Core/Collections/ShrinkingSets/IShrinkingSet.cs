﻿using System.Collections.Generic;

namespace Recurse.Core.Collections
{
    public interface IShrinkingSet
    {
        /// <summary>
        /// Mask for hash codes of items in this set indicating
        /// bits which must be set.
        /// </summary>
        int ItemHash { get; }

        /// <summary>
        /// Mask for hash codes of items in this set indicating
        /// which bits must match.
        /// </summary>
        int ItemHashMask { get; }
    }

    public interface IShrinkingSet<T> : IShrinkingSet, IReadOnlyCollection<T> where T : notnull
    {
        bool Contains(T item);

        IShrinkingSet<T> GetDifference(IShrinkingSet<T> other);

        bool MayOverlap(IShrinkingSet<T> other)
        {
            var commonHashMask = ItemHashMask & other.ItemHashMask;
            return (ItemHash & commonHashMask) == (other.ItemHash & commonHashMask);
        }

        bool MayContain(T item)
        {
            return (item.GetHashCode() & ItemHashMask) == ItemHash;
        }

        bool MayContain(int itemHash)
        {
            return (itemHash & ItemHashMask) == ItemHash;
        }
    }
}
