﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Recurse.Core.Collections
{
    public class ShrinkingSetCache<T> where T : notnull
    {
        private readonly ShrinkingEmptySet<T> _emptySet;

        private readonly Dictionary<T, ShrinkingSingletonSet<T>> _singletons = new();

        private readonly Dictionary<Tuple<IShrinkingSet<T>, IShrinkingSet<T>>, IShrinkingSet<T>> _binarySets = new(new TupleComparer());

        public ShrinkingSetCache()
        {
            _emptySet = new();
        }

        public IShrinkingSet<T> GetEmpty()
        {
            return _emptySet;
        }

        public IShrinkingSet<T> GetSingleton(T item)
        {
            if (!_singletons.TryGetValue(item, out var singleton))
            {
                singleton = new ShrinkingSingletonSet<T>(this, item);
                _singletons[item] = singleton;
            }
            return singleton;
        }

        public IShrinkingSet<T> GetSet(T item1, T item2)
        {
            if (Equals(item1, item2))
            {
                return GetSingleton(item1);
            }
            else
            {
                var items = new[] { item1, item2 };

                return GetSet(items, 0, 2);
            }
        }

        public IShrinkingSet<T> GetSet(IEnumerable<T> items)
        {
            var itemArray = items.ToHashSet().ToArray();

            return GetSet(itemArray, 0, itemArray.Length);
        }

        public IShrinkingSet<T> GetSplitSet(IShrinkingSet<T> subset1, IShrinkingSet<T> subset2)
        {
            var key = new Tuple<IShrinkingSet<T>, IShrinkingSet<T>>(subset1, subset2);
            if (_binarySets.TryGetValue(key, out var set))
            {
                return set;
            }
            else
            {
                return _binarySets[key] = new ShrinkingSplitSet<T>(this, subset1, subset2);
            }
        }

        public IShrinkingSet<T> GetLeafSet(HashSet<T> items)
        {
            if (items.Count == 0)
            {
                return _emptySet;
            }
            else if (items.Count == 1)
            {
                return GetSingleton(items.First());
            }
            else
            {
                return new ShrinkingFlatSet<T>(this, items);
            }
        }

        private IShrinkingSet<T> GetSet(T[] items, int startIndex, int count, int discriminatorIndex = 0)
        {
            if (count == 0)
            {
                return _emptySet;
            }
            else if (count == 1)
            {
                return GetSingleton(items[startIndex]);
            }
            else if (discriminatorIndex > ShrinkingSplitSet<T>.MaxDiscriminatorIndex)
            {
                return new ShrinkingFlatSet<T>(this, items.ToHashSet());
            }
            else
            {
                var i = startIndex;
                var rightStartIndex = startIndex + count;
                do
                {
                    var item = items[i];
                    if (ShrinkingSplitSet<T>.GetDiscrimination(discriminatorIndex, item.GetHashCode()))
                    {
                        rightStartIndex--;
                        items[i] = items[rightStartIndex];
                        items[rightStartIndex] = item;
                    }
                    else
                    {
                        i++;
                    }
                } while(i < rightStartIndex);

                var leftCount = rightStartIndex - startIndex;
                var rightCount = count - leftCount;
                var nextDiscriminatorIndex = discriminatorIndex + 1;
                if (leftCount == 0)
                {
                    return GetSet(items, rightStartIndex, rightCount, nextDiscriminatorIndex);
                }
                else if (rightCount == 0)
                {
                    return GetSet(items, startIndex, count, nextDiscriminatorIndex);
                }
                else
                {
                    var left = GetSet(items, startIndex, leftCount, nextDiscriminatorIndex);
                    var right = GetSet(items, rightStartIndex, rightCount, nextDiscriminatorIndex);

                    return GetSplitSet(left, right);
                }
            }
        }

        private class TupleComparer : IEqualityComparer<Tuple<IShrinkingSet<T>, IShrinkingSet<T>>>
        {
            public bool Equals(Tuple<IShrinkingSet<T>, IShrinkingSet<T>>? x, Tuple<IShrinkingSet<T>, IShrinkingSet<T>>? y)
            {
                if (Equals(x!.Item1, y!.Item1))
                {
                    return Equals(x.Item2, y.Item2);
                }
                else if (Equals(x.Item1, y.Item2))
                {
                    return Equals(x.Item2, y.Item1);
                }
                else
                {
                    return false;
                }
            }

            public int GetHashCode([DisallowNull] Tuple<IShrinkingSet<T>, IShrinkingSet<T>> obj)
            {
                return obj.Item1.GetHashCode() ^ obj.Item2.GetHashCode();
            }
        }
    }
}
