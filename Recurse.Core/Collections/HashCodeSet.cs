﻿using System;
using System.Collections.Generic;

namespace Recurse.Games.Explorer.Systems.WaveFunctions
{
    /// <summary>
    /// A set containing all objects with certain hash codes
    /// </summary>
    [Serializable]
    public struct HashCodeSet
    {
        /// <summary>
        /// Masked hash code of items in this set.
        /// </summary>
        private int ItemHashCode { get; set; }

        /// <summary>
        /// Mask for hash codes of items in this set (indicating which bits are set).
        /// </summary>
        private int ItemHashCodeMask { get; set; }

        public bool IsUniversal => ItemHashCodeMask == 0;

        private HashCodeSet(int itemHashCode, int itemHashCodeMask)
        {
            ItemHashCode = itemHashCode;
            ItemHashCodeMask = itemHashCodeMask;
        }

        /// <summary>
        /// Creates the smallest set which contains the given items
        /// </summary>
        public static HashCodeSet? IncludingAll<T>(IEnumerable<T> items) where T : notnull
        {
            HashCodeSet? result = null;
            foreach (var item in items)
            {
                if (result == null)
                {
                    result = Create(item);
                }
                else
                {
                    result.Value.Expand(item);
                    if (result.Value.IsUniversal)
                    {
                        return result;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Creates the smallest set which contains the given item
        /// </summary>
        public static HashCodeSet Create<T>(T item) where T : notnull
        {
            return new HashCodeSet(item.GetHashCode(), ~0);
        }

        public void Expand<T>(T item) where T : notnull
        {
            var difference = (item.GetHashCode() ^ ItemHashCode) & ItemHashCodeMask;

            ItemHashCodeMask &= ~difference;
        }

        public bool Contains<T>(T item)
        {
            return item != null && (item.GetHashCode() & ItemHashCodeMask) == ItemHashCode;
        }

        public bool Contains(HashCodeSet other)
        {
            return
                (ItemHashCodeMask & other.ItemHashCodeMask) == ItemHashCodeMask &&
                ItemHashCode == (other.ItemHashCode & ItemHashCodeMask);
        }

        public HashCodeSet Union(HashCodeSet other)
        {
            var itemHashCodeMask = ItemHashCodeMask & other.ItemHashCodeMask & ~(ItemHashCode ^ other.ItemHashCode);
            var itemHashCode = itemHashCodeMask & ItemHashCode;
            return new HashCodeSet(itemHashCode, itemHashCodeMask);
        }

        public bool Intersects(HashCodeSet other)
        {
            var commonHashMask = ItemHashCodeMask & other.ItemHashCodeMask;
            return (ItemHashCode & commonHashMask) == (other.ItemHashCode & commonHashMask);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ItemHashCodeMask, ItemHashCode);
        }
    }
}
