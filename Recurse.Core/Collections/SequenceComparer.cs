﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Recurse.Core.Collections
{
    public static class SequenceComparer<T>
    {
        public static readonly SequenceComparer<IEnumerable<T>, T> Default = new();
    }

    public class SequenceComparer<T, TItem> : IEqualityComparer<T>
        where T : IEnumerable<TItem>
    {
        public static readonly SequenceComparer<T, TItem> Default = new();

        public bool Equals(T? x, T? y)
        {
            if (x == null)
            {
                return y == null;
            }
            else if (y == null)
            {
                return x == null;
            }
            else
            {
                return x.SequenceEqual(y);
            }
        }

        public int GetHashCode([DisallowNull] T obj)
        {
            return obj.Aggregate(0x7f9ed6e6, HashCode.Combine);
        }
    }
}