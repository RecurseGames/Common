﻿using Recurse.Core.Messaging;
using System;

namespace Recurse.Core.Collections.Singletons
{
    public static class ReadOnlyField
    {
        public static IReadOnlyField<bool> True { get; } = new ReadOnlyField<bool>(true);

        public static IReadOnlyField<bool> False { get; } = new ReadOnlyField<bool>(false);

#warning See if I can get rid of these repetitive methods with c#9's new() operator
        public static IReadOnlyField<TValue> Create<TValue>(TValue value)
        {
            return new ReadOnlyField<TValue>(value);
        }
    }

    [Serializable]
    public class ReadOnlyField<TValue> : IReadOnlyField<TValue>
    {
        public TValue Value { get; }

        public ICallbackCollection ValueChanged => ICallbackCollection.NotInvoked;

        public ReadOnlyField(TValue value)
        {
            Value = value;
        }
    }
}
