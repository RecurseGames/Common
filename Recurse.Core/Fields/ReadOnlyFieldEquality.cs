﻿using Recurse.Core.Collections.Singletons;
using Recurse.Core.Messaging;
using System;

namespace Recurse.Amaranth.Mechanics.Logic.Generic
{
    [Serializable]
    public abstract class ReadOnlyFieldEquality : IReadOnlyField<bool>
    {
        public abstract ICallbackCollection ValueChanged { get; }

        public abstract bool Value { get; }

        public static IReadOnlyField<bool> Create<TValue>(IReadOnlyField<TValue> dependency, TValue requiredValue)
        {
            return new ReadOnlyFieldEquality<TValue>(dependency, requiredValue);
        }
    }

    [Serializable]
    public class ReadOnlyFieldEquality<TValue> : ReadOnlyFieldEquality
    {
        private readonly IReadOnlyField<TValue> _requirement;

        private readonly TValue _requiredValue;

        private readonly Action _requirementValueChanged;

        private bool _lastValue;

        private ICallbackInvoker _onChange = ICallbackInvoker.CreateExecutable();

        public override ICallbackCollection ValueChanged => _onChange;

        public override bool Value => Equals(_requirement.Value, _requiredValue);

        public ReadOnlyFieldEquality(IReadOnlyField<TValue> requirement, TValue requiredValue)
        {
            _requirement = requirement;
            _requiredValue = requiredValue;
            _requirementValueChanged = RequirementValueChanged;
            _lastValue = Value;

            _requirement.ValueChanged.AddWeak(_requirementValueChanged);
        }

        private void RequirementValueChanged()
        {
            if (!Equals(_lastValue, Value))
            {
                _lastValue = Value;
                _onChange.Execute();
            }
        }
    }
}
