﻿using Recurse.Core.Diagnostics;
using Recurse.Core.Messaging;
using System;

namespace Recurse.Core.Collections.Singletons
{
    [Serializable]
    public class Field<TValue> : IReadOnlyField<TValue>, IField<TValue>
    {
        private TValue _value;

        private ICallbackInvoker _onChange = ICallbackInvoker.CreateExecutable();

        public ICallbackCollection ValueChanged => _onChange;

        public bool IsReadOnly => !_onChange.IsOccurring;

        public TValue Value
        {
            get { return _value; }
            set
            {
                if (IsReadOnly)
                {
                    throw new InvalidOperationException("Value is read only.");
                }

                if (!Equals(Value, value))
                {
                    _value = value;
                    _onChange.Execute();
                }
            }
        }

        public Field(TValue initialValue)
        {
            _value = initialValue;
        }

        public void SetReadOnly()
        {
            _onChange = ICallbackInvoker.CreateNotExecutable();
        }

        public override string ToString()
        {
            return $"Changeable({new DebugString(Value)})";
        }
    }
}
