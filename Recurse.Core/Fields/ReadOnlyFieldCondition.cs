﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Core.Collections.Singletons
{
    /// <summary>
    /// A value which is true when a set of other values have the required value
    /// </summary>
    [Serializable]
    public class ReadOnlyFieldCondition : ReadOnlyFieldFunctional<bool>
    {
        private readonly Dependencies _dependencies;

#warning Update to C# 8, use init-only properties (here and in similar condition classes)
#warning Try to remove this, or move the logic into another component... doesn't really belong here
        /// <summary>
        /// Indicates an exception should be thrown when adding invalid conditions
        /// </summary>
        public bool IsValidRequired { get; init; }

        public ReadOnlyFieldCondition() : this(new Dependencies())
        {

        }

        private ReadOnlyFieldCondition(Dependencies dependencies) : base(dependencies.GetValue)
        {
            _dependencies = dependencies;
        }

        public void Add(IReadOnlyField<bool> condition)
        {
            if (IsValidRequired && !condition.Value)
            {
                throw new InvalidOperationException("Condition not satisfied.");
            }

            _dependencies.Add(condition);
            AddDependency(condition.ValueChanged);
        }

        [Serializable]
        private class Dependencies : List<IReadOnlyField<bool>>
        {
            public bool GetValue()
            {
                return this.All(e => e.Value);
            }
        }
    }
}
