﻿using Recurse.Core.Collections.Singletons;
using Recurse.Core.Messaging;
using System;

namespace Recurse.Amaranth.Mechanics.Logic.Generic
{
    [Serializable]
    public abstract class FieldEquality : IReadOnlyField<bool>
    {
        public abstract ICallbackCollection ValueChanged { get; }

        public abstract bool Value { get; }

        public abstract void SetTrue();

        public static FieldEquality Create<TValue>(IField<TValue> dependency, TValue requiredValue)
        {
            return new FieldEquality<TValue>(dependency, requiredValue);
        }
    }

    [Serializable]
    public class FieldEquality<TValue> : FieldEquality
    {
        private readonly IField<TValue> _requirement;

        private readonly TValue _requiredValue;

        private bool _lastValue;

        private Action _requirementValueChanged;

        private ICallbackInvoker _onChange = ICallbackInvoker.CreateExecutable();

        public override ICallbackCollection ValueChanged => _onChange;

        public override bool Value => Equals(_requirement.Value, _requiredValue);

        public FieldEquality(IField<TValue> requirement, TValue requiredValue)
        {
            _requirement = requirement;
            _requiredValue = requiredValue;
            _requirementValueChanged = RequirementValueChanged;
            _lastValue = Value;

            _requirement.ValueChanged.AddWeak(_requirementValueChanged);
        }

        public override void SetTrue()
        {
            _requirement.Value = _requiredValue;
        }

        private void RequirementValueChanged()
        {
            if (!Equals(_lastValue, Value))
            {
                _lastValue = Value;
                _onChange.Execute();
            }
        }
    }
}
