﻿using Recurse.Core.Messaging;
using System;

namespace Recurse.Core.Collections.Singletons
{
    [Serializable]
    public class ReadOnlyFieldFunctional<TValue> : IReadOnlyField<TValue>
    {
        private readonly ICallbackInvoker _onChange = ICallbackInvoker.CreateExecutable();

        private readonly Func<TValue> _getValue;

        private TValue _lastValue;

        private Action? _handleDependencyChange;

        public ICallbackCollection ValueChanged => _onChange;

        public TValue Value
        {
            get
            {
                var currentValue = _getValue();
                if (!Equals(currentValue, _lastValue))
                {
                    throw new InvalidOperationException("Invalid state. Value has not been updated correctly.");
                }
                return _lastValue;
            }
        }

        public ReadOnlyFieldFunctional(Func<TValue> getValue)
        {
            _getValue = getValue;
            _lastValue = getValue();
        }

        public void AddDependency(ICallbackCollection dependencyChanged)
        {
            _handleDependencyChange ??= OnDependencyChange;
            dependencyChanged.AddWeak(_handleDependencyChange);
        }

        public void RemoveDependency(ICallbackCollection dependencyChanged)
        {
            if (_handleDependencyChange != null)
            {
                dependencyChanged.Remove(_handleDependencyChange);
            }
        }

        private void OnDependencyChange()
        {
            var newValue = _getValue();
            if (!Equals(_lastValue, newValue))
            {
                _lastValue = newValue;
                _onChange.Execute();
            }
        }
    }
}
