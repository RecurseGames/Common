﻿using Recurse.Amaranth.Mechanics.Logic.Generic;

namespace Recurse.Core.Collections.Singletons
{
    public static class FieldsExtensions
    {
#warning Rename CreateEquality, same as similar methods in this class
        public static IReadOnlyField<bool> IsEqual<T>(this IReadOnlyField<T> field, T value)
        {
            return ReadOnlyFieldEquality.Create(field, value);
        }

        public static FieldEquality IsEqual<T>(this IField<T> field, T value)
        {
            return FieldEquality.Create(field, value);
        }
    }
}
