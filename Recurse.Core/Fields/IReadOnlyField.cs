﻿using Recurse.Core.Messaging;

namespace Recurse.Core.Collections.Singletons
{
    public interface IReadOnlyField
    {
        /*
         * ValueChanged.Add
         * OnChange.AddCall
         * ChangeHandlers.Add
         */
#warning Rename and rename callback collection methods to support OnChange.AddCall
        ICallbackCollection ValueChanged { get; }
    }

    public interface IReadOnlyField<out TValue> : IReadOnlyField
    {
        TValue Value { get; }
    }
}
