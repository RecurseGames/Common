﻿using Recurse.Core.Messaging;

namespace Recurse.Core.Collections.Singletons
{
    public interface IField<TValue>
    {
        ICallbackCollection ValueChanged { get; }

        TValue Value { get; set; }
    }
}
