﻿using Recurse.Core.Messaging;
using System;

namespace Recurse.Core.Collections.Singletons
{
    [Serializable]
    public class FieldWrapper<TValue> : IReadOnlyField<TValue>, IField<TValue>
    {
        private readonly ICallbackInvoker _onChange = ICallbackInvoker.CreateExecutable();

        private Action? _handleDependencyChange;

        private IReadOnlyField<TValue> _source;

        private TValue _lastValue;

        public ICallbackCollection ValueChanged => _onChange;

        public IReadOnlyField<TValue> Source
        {
            get { return _source; }
            set
            {
                _handleDependencyChange ??= OnDependencyChange;
                _source.ValueChanged.Remove(_handleDependencyChange);
                _source = value;
                _source.ValueChanged.AddWeak(_handleDependencyChange);

                OnDependencyChange();
            }
        }

        public TValue Value
        {
            get
            {
                var currentValue = _source.Value;
                if (!Equals(currentValue, _lastValue))
                {
                    throw new InvalidOperationException("Invalid state. Value has not been updated correctly.");
                }
                return _lastValue;
            }
            set
            {
                _source.ValueChanged.Remove(_handleDependencyChange);
                _source = ReadOnlyField.Create(value);

                OnDependencyChange();
            }
        }

        public FieldWrapper(TValue initialValue)
        {
            _lastValue = initialValue;
            _source = ReadOnlyField.Create(initialValue);
        }

        public FieldWrapper(IReadOnlyField<TValue> source)
        {
            _lastValue = source.Value;
            _source = source;
            _handleDependencyChange = OnDependencyChange;

            source.ValueChanged.AddWeak(_handleDependencyChange);
        }

        private void OnDependencyChange()
        {
            var newValue = _source.Value;
            if (!Equals(_lastValue, newValue))
            {
                _lastValue = newValue;
                _onChange.Execute();
            }
        }
    }
}
