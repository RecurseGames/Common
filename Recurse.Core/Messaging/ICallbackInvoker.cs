﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Recurse.Core.Messaging
{
#warning Rename ICallbackEvent, IReadOnlyCallbackEvent
    public interface ICallbackInvoker : ICallbackCollection
    {
        public static ICallbackInvoker CreateExecutable()
        {
            return new ExecutableImplementation();
        }

        public static ICallbackInvoker CreateNotExecutable()
        {
            return new NotExecutableImplementation();
        }

#warning Rename InvokeCallbacks
        void Execute();

        [Serializable]
        private class NotExecutableImplementation : ICallbackInvoker
        {
            public bool IsOccurring => false;

            public void Add(Action callback)
            {

            }

            public void AddWeak(Action callback)
            {

            }

            public void Execute()
            {
                throw new NotSupportedException();
            }

            public bool Remove(Action? callback)
            {
                return false;
            }
        }

        [Serializable]
        private class ExecutableImplementation : ReferenceCollection<Action>, ICallbackInvoker
        {
            public bool IsOccurring => true;

            public void Execute()
            {
                var callbacks = CreateEnumerable();
                foreach (var callback in callbacks)
                {
                    try
                    {
                        callback();
                    }
                    catch (Exception e)
                    {
                        throw new CallbackException(e);
                    }
                }
            }
        }
    }
}