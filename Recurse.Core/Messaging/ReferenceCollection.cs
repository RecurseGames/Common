﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;

namespace Recurse.Core.Messaging
{
    [Serializable]
    public class ReferenceCollection<T> : List<Reference<T>>, IReferenceCollection<T> where T : class
    {
        public void Add(T element)
        {
            Add(Reference.CreateStrong(element));
        }

        public void AddWeak(T element)
        {
            Add(Reference.CreateWeak(element));
        }

        public bool Remove(T? element)
        {
            if (element == null)
            {
                return false;
            }

            var index = FindIndex(reference => element.Equals(reference.Target));
            if (index != -1)
            {
                RemoveAt(index);
                return true;
            }

            return false;
        }

        public IReadOnlyCollection<T> CreateEnumerable()
        {
            var result = new List<T>(Count);
            var i = 0;
            while (i < Count)
            {
                var element = this[i].Target;
                if (element != null)
                {
                    result.Add(element);
                    i++;
                }
                else
                {
                    RemoveAt(i);
                }
            }

            return result;
        }
    }
}