﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Recurse.Core.Messaging
{
    public interface ICallbackCollection
    {
        public static ICallbackCollection NotInvoked { get; } = new NotInvokedImplementation();

        bool IsOccurring { get; }

        /// <summary>
        /// Stores a strong reference to the given callback for as long as it
        /// may be invoked.
        /// </summary>
        void Add(Action callback);

        void AddWeak(Action callback);

        bool Remove(Action? callback);

        [Serializable]
        private class NotInvokedImplementation : ICallbackCollection
        {
            public bool IsOccurring => false;

            public void Add(Action callback)
            {

            }

            public void AddWeak(Action callback)
            {

            }

            public bool Remove(Action? callback)
            {
                return false;
            }
        }
    }
}