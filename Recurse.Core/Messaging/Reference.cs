﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Recurse.Core.Messaging
{
    public static class Reference
    {
        public static Reference<T> CreateWeak<T>(T element) where T : class
        {
            return Reference<T>.CreateWeak(element);
        }

        public static Reference<T> CreateStrong<T>(T element) where T : class
        {
            return Reference<T>.CreateStrong(element);
        }
    }

    public struct Reference<T> where T : class
    {
        private readonly object _reference;

        public T? Target
        {
            get
            {
                if (_reference is WeakReference<T> weakReference)
                {
                    weakReference.TryGetTarget(out var target);
                    return target;
                }
                else
                {
                    return (T?)_reference;
                }
            }
        }

        private Reference(object reference)
        {
            _reference = reference;
        }

        public static Reference<T> CreateWeak(T element)
        {
            return new Reference<T>(new WeakReference<T>(element));
        }

        public static Reference<T> CreateStrong(T element)
        {
            return new Reference<T>(element);
        }
    }
}