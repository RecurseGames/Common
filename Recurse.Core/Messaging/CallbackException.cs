﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Recurse.Core.Messaging
{
    /// <summary>
    /// Thrown when a callback, event handler or other
    /// similar object throws an Exception.
    /// </summary>
    /// <remarks>
    /// If an event handler throws an Exception, the code responsible
    /// for invoking event handlers has no sensible way to handle it.
    /// Execution must immediately stop, but this means some event
    /// handlers may fail to be invoked. The program may thus be left
    /// in an invalid state. The
    /// Exception is wrapped in a <see cref="CallbackException"/> to make sure it is
    /// not mistaken for a regular Exception (ie, whatever was thrown
    /// by the event handler)
    /// </remarks>
    public class CallbackException : Exception
    {
        public CallbackException(Exception innerException) : base(
            "A callback, event handler or similar has thrown an Exception.",
            innerException)
        {
        }
    }
}