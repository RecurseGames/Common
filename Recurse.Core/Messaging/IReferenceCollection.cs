﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;

namespace Recurse.Core.Messaging
{
    public static class IReferenceCollection
    {
        public static IReferenceCollection<T> Create<T>() where T : class
        {
            return new ReferenceCollection<T>();
        }
    }

    public interface IReferenceCollection<T> where T : class
    {
        /// <summary>
        /// Adds a strong reference to the given element.
        /// </summary>
        void Add(T element);

        /// <summary>
        /// Adds a weak reference to the given element.
        /// </summary>
        void AddWeak(T element);

        /// <summary>
        /// Removes a reference to the given element, if any.
        /// </summary>
        /// <returns>
        /// Whether a reference was removed.
        /// </returns>
        bool Remove(T? element);

        /// <summary>
        /// Creates a list of all elements currently in the collection.
        /// </summary>
        IReadOnlyCollection<T> CreateEnumerable();
    }
}