﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Recurse.Core.Templates
{
    public partial class OffsetTemplate
    {
        public string Namespace { get; }

        public string Name { get; }

        public string ValueType { get; }

        public OffsetTemplate(string @namespace, string name, string valueType)
        {
            Namespace = @namespace;
            Name = name;
            ValueType = valueType;
        }
    }
}
