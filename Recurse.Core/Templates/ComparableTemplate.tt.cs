﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Recurse.Core.Templates
{
#warning Rename ComparableMethods, only include method declarations, use C#5 Records for equality
    public partial class ComparableTemplate
    {
        public string Namespace { get; }

        public string Name { get; }

        public ComparableTemplate(string @namespace, string name)
        {
            Namespace = @namespace;
            Name = name;
        }
    }
}
