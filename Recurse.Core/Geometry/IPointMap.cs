﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Recurse.Core.Collections.Planes
{
    public interface IPointMap<TPoint, TValue> : IPointArea<TPoint>
    {
        IReadOnlyCollection<TValue> Values { get; }

        TValue this[TPoint point] { get; }

        bool TryGet(TPoint point, [MaybeNullWhen(false)] out TValue value)
        {
            if (Contains(point))
            {
                value = this[point];
                return true;
            }
            else
            {
                value = default;
                return false;
            }
        }
    }
}