﻿namespace Recurse.Games.Explorer.Systems.WaveFunctions3.Solvers
{
    public interface IPoint<TPoint>
    {
        TPoint Add(TPoint point);
    }
}
