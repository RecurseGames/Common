﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Recurse.Core.Collections.Planes
{
    public static class AreaComparer
    {
        public static bool Equals<TPoint, TValue>(IPointMap<TPoint, TValue>? x, IPointMap<TPoint, TValue>? y)
        {
            return AreaComparer<TPoint, TValue>.Instance.Equals(x, y);
        }

        public static int GetHashCode<TPoint, TValue>([DisallowNull] IPointMap<TPoint, TValue> obj)
        {
            return AreaComparer<TPoint, TValue>.Instance.GetHashCode(obj);
        }
    }

    public class AreaComparer<TPoint, TValue> : IEqualityComparer<IPointMap<TPoint, TValue>>
    {
        public readonly static AreaComparer<TPoint, TValue> Instance = new();

        public bool Equals(IPointMap<TPoint, TValue>? x, IPointMap<TPoint, TValue>? y)
        {
            if (x == y)
            {
                return true;
            }
            else if (x == null || y == null)
            {
                return false;
            }
            else
            {
                return y.Points.All(x.Contains) && x.Points.All(point => y.Contains(point) && Equals(y[point], x[point]));
            }
        }

        public int GetHashCode([DisallowNull] IPointMap<TPoint, TValue> obj)
        {
            return obj.Points.Select(point => obj[point]).Aggregate(0, HashCode.Combine);
        }
    }
}