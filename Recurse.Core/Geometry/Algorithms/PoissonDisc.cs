﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Recurse.Core.Geometry.Algorithms
{
    public class PoissonDisc<T>
    {
        private readonly Random _random;

        private readonly List<T> _frontier = new();

        private readonly List<T> _items = new();

        public IReadOnlyCollection<T> Items => _items;

        public PoissonDisc(Random random)
        {
            _random = random;
        }

        public IEnumerable<T> GetNeighbours(Sampling sampling, T item)
        {
            return _items.Where(candidate => sampling.AreNeighbours(candidate, item));
        }

        public void Add(T item)
        {
            _items.Add(item);
            _frontier.Add(item);
        }

        public void Expand(Sampling sampling, int maximumAddAttempts)
        {
            if (_items.Count == 0)
            {
                throw new InvalidOperationException("Must provide a starting point.");
            }

            while (_frontier.Count != 0)
            {
                var originIndex = _random.Next(_frontier.Count);
                var origin = _frontier[originIndex];

                var attempts = 0;
                while (true)
                {
                    if (TryAdd(origin, sampling, out _))
                    {
                        break;
                    }
                    else if (++attempts > maximumAddAttempts)
                    {
                        _frontier.UnstableRemoveAt(originIndex);
                        break;
                    }
                }
            }
        }

        private bool TryAdd(
            T origin,
            Sampling sampling,
            [MaybeNullWhen(false)] out T item)
        {
            var candidate = sampling.CreateNeighbour(origin, _random);
            if (_items.All(item => sampling.AreValid(item, candidate)))
            {
                item = candidate;
                _frontier.Add(item);
                _items.Add(item);
                return true;
            }

            item = default;
            return false;
        }

        public abstract class Sampling
        {
            public abstract bool AreValid(T a, T b);

            public abstract bool AreNeighbours(T a, T b);

            public abstract T CreateNeighbour(T origin, Random random);
        }
    }
}
