﻿using System;
using System.Collections.Generic;

namespace Recurse.Core.Collections.Planes.Algorithms
{
    public abstract class AreaFiller<TPoint>
    {
        private readonly INeighbourhood<TPoint> _neighbourhood;

        private readonly List<TPoint> _frontier = new();

        public AreaFiller(INeighbourhood<TPoint> neighbourhood)
        {
            _neighbourhood = neighbourhood;
        }

        public void AddPoint(TPoint origin)
        {
            OnExpand(origin);
            _frontier.Add(origin);
        }

        public bool AddPoint(Random rng)
        {
            if (_frontier.Count != 0)
            {
                var positionIndex = rng.Next(_frontier.Count);
                var position = _frontier[positionIndex];
                _frontier.RemoveAt(positionIndex);

                foreach (var neighbour in _neighbourhood.GetNeighbours(position))
                {
                    if (CanExpand(position, neighbour))
                    {
                        OnExpand(position, neighbour);
                        _frontier.Add(neighbour);
                    }
                }

                return true;
            }
            return false;
        }

        protected abstract void OnExpand(TPoint point);

        protected abstract void OnExpand(TPoint source, TPoint point);

        protected abstract bool CanExpand(TPoint source, TPoint point);
    }
}
