﻿using Recurse.Core.Maths;
using System;

namespace Recurse.Core.Geometry2D.Spheres
{
    public readonly partial struct LatLong
    {
        private const double LatitudeMaximum = Math.PI / 2;

        private const double LatitudeMinimum = -Math.PI / 2;

        public static LatLong Default => default;

        public static LatLong NorthPole => new(LatitudeMaximum, 0);

        public static LatLong SouthPole => new(LatitudeMinimum, 0);

        public double Latitude { get; }

        public double Longitude { get; }

        private LatLong(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public static LatLong Next(Random random)
        {
            return new(random.Next(180) - 90, random.Next(360) - 180);
        }

        public static LatLong Create(double latitude, double longitude)
        {
            if (!IsLatitude(latitude))
            {
                throw new ArgumentOutOfRangeException(nameof(latitude), "Latitude must be between -90 and 90 degrees (inclusive)");
            }
            else if (!IsLongitude(longitude))
            {
                throw new ArgumentOutOfRangeException(nameof(longitude), "Longitude must be between -180 and 180 degrees (inclusive)");
            }

            return new(latitude, longitude);
        }

        /// <summary>
        /// Gets the point a given distance and bearing from an origin
        /// </summary>
        /// <param name="distance">The distance, relative to the radius of the sphere</param>
        /// <param name="bearing">The bearing, in radians</param>
        public static LatLong Create(LatLong origin, Degrees distance, Degrees bearing)
        {
            var latitude = Math.Asin(
                Math.Sin(origin.Latitude) * Math.Cos(distance.Value)
                + Math.Cos(origin.Latitude) * Math.Sin(distance.Value) * Math.Cos(bearing.Value));
            var longitude = origin.Longitude + Math.Atan2(
                Math.Sin(bearing.Value) * Math.Sin(distance.Value) * Math.Cos(origin.Latitude),
                Math.Cos(distance.Value) - Math.Sin(origin.Latitude) * Math.Sin(latitude)
            );

            if (latitude > LatitudeMaximum)
            {
                latitude = Math.PI - latitude;
            }
            else if (latitude < LatitudeMinimum)
            {
                latitude = Math.PI + latitude;
            }

            longitude = Number.Wrap(longitude, -2 * Math.PI, 2 * Math.PI);

            return new(latitude, longitude);
        }

        /// <summary>
        /// Gets the distance to a destination, relative to radius of the sphere
        /// </summary>
        public static Radians GetDistance(LatLong a, LatLong b)
        {
            var latitudeDelta = a.Latitude - b.Latitude;
            var longitudeDelta = a.Longitude - b.Longitude;
            var h = Math.Sin(latitudeDelta / 2) * Math.Sin(latitudeDelta / 2)
                + Math.Cos(a.Latitude) * Math.Cos(b.Latitude)
                * Math.Sin(longitudeDelta / 2) * Math.Sin(longitudeDelta / 2);
            return new(2 * Math.Atan2(Math.Sqrt(h), Math.Sqrt(1 - h)));
        }

        /// <summary>
        /// Gets the bearing to the given destination from this point, in radians
        /// </summary>
        public double GetBearing(LatLong destination)
        {
            var destinationRadian = destination.Longitude - Longitude;
            var destinationPhi = Math.Log(Math.Tan(destination.Latitude / 2 + Math.PI / 4) / Math.Tan(Latitude / 2 + Math.PI / 4));

            if (Math.Abs(destinationRadian) > Math.PI)
            {
                destinationRadian = destinationRadian > 0
                    ? -(2 * Math.PI - destinationRadian)
                    : (2 * Math.PI + destinationRadian);
            }

            return Math.Atan2(destinationRadian, destinationPhi);
        }

        /// <summary>
        /// Gets a point a given distance and bearing from this one.
        /// </summary>
        /// <param name="distance">The distance, relative to the radius of the sphere</param>
        /// <param name="bearing">The bearing, in radians</param>
        public LatLong GetOffset(Degrees distance, Degrees bearing)
        {
            return Create(this, distance, bearing);
        }

        /// <summary>
        /// Gets the distance to a destination, relative to radius of the sphere
        /// </summary>
        public Radians GetDistance(LatLong destination)
        {
            return GetDistance(this, destination);
        }

        public override string ToString()
        {
            var latitude = new Degrees(Latitude);
            var longitude = new Degrees(Longitude);

            return $"{latitude:F2}, {longitude:F2}";
        }

        private static bool IsLatitude(double latitude)
        {
            return latitude >= -90 && latitude <= 90;
        }

        private static bool IsLongitude(double longitude)
        {
            return longitude >= -180 && longitude <= 180;
        }
    }
}
