﻿using System;

namespace Recurse.Core.Geometry2D.Spheres
{
    public class OrthographicProjection
    {
        private readonly LatLong _origin;

        public OrthographicProjection(LatLong origin)
        {
            _origin = origin;
        }

        public void Project(LatLong point, out double x, out double y, out bool isClipped)
        {
            var radius = 1;
            var longitudeDelta = point.Longitude - _origin.Longitude;

            isClipped = _origin.GetDistance(point) > Radians.HalfTurn;
            x = radius * Math.Cos(point.Latitude) * Math.Sin(longitudeDelta);
            y = radius * (
                Math.Cos(_origin.Latitude) * Math.Sin(point.Latitude) 
                - Math.Sin(_origin.Latitude) * Math.Cos(point.Latitude) * Math.Cos(longitudeDelta)
            );
        }
    }
}
