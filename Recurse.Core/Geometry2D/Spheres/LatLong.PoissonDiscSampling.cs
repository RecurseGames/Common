﻿using Recurse.Core.Geometry.Algorithms;
using System;

namespace Recurse.Core.Geometry2D.Spheres
{
    public readonly partial struct LatLong
    {
        public class PoissonDiscSampling : PoissonDisc<LatLong>.Sampling
        {
            private readonly Radians _minimumDistance;

            private readonly Radians _maximumDistance;

            private PoissonDiscSampling(Radians minimumDistance, Radians maximumDistance)
            {
                _minimumDistance = minimumDistance;
                _maximumDistance = maximumDistance;
            }

            public static PoissonDiscSampling FromDistance(Radians minimumDistance, Radians maximumDistance)
            {
                if (!(maximumDistance > minimumDistance))
                {
                    throw new ArgumentException("Maximum distance must be greater than minimum.");
                }

                return new(minimumDistance, maximumDistance);
            }

            public static PoissonDiscSampling FromExpectedCount(int count)
            {
                if (count <= 1)
                {
                    throw new ArgumentException("Expected samples must be at least 2.", nameof(count));
                }

                var averageDistance = Math.PI * Math.Sin(Math.PI / count);
                var minimumDistance = new Radians(2 * averageDistance / 3);
                var maximumDistance = new Radians(4 * averageDistance / 3);
                return new(minimumDistance, maximumDistance);
            }

            public override bool AreValid(LatLong a, LatLong b)
            {
                return a.GetDistance(b) >= _minimumDistance;
            }

            public override bool AreNeighbours(LatLong a, LatLong b)
            {
                return a.GetDistance(b) < _maximumDistance;
            }

            public override LatLong CreateNeighbour(LatLong origin, Random random)
            {
                var distance = new Radians(random.NextDouble(_minimumDistance.Value, _maximumDistance.Value));
                var bearing = new Radians(random.NextDouble() * Radians.MaximumValue);

                return origin.GetOffset(distance, bearing);
            }
        }
    }
}
