﻿using Recurse.Core.Maths;
using System;

namespace Recurse.Core.Geometry2D
{
    public readonly struct Degrees : IEquatable<Degrees>
    {
        private const double RadiansPerDegree = Math.PI / 180;

        private const double MaximumValue = 360;

        public static readonly Degrees HalfTurn = new(180);

        public double Value { get; }

        public double Radians => Value * RadiansPerDegree;

        public Degrees(double value)
        {
            if (!double.IsFinite(value))
            {
                throw new ArgumentException("Degrees must be finite.", nameof(value));
            }

            Value = Modulus.Get(value, MaximumValue);
        }

        public Degrees GetDistance(Degrees other)
        {
            var difference = Math.Abs(Value - other.Value);

            return new(Math.Min(difference, MaximumValue - difference));
        }

        public Degrees GetDifference(Degrees other)
        {
            return new(Value - other.Value);
        }

        public static implicit operator Radians(Degrees degrees)
        {
            return new(degrees.Value * RadiansPerDegree);
        }

        public static bool operator ==(Degrees a, Degrees b)
        {
            return a.Value == b.Value;
        }

        public static bool operator !=(Degrees a, Degrees b)
        {
            return a.Value == b.Value;
        }

        public static bool operator >=(Degrees a, Degrees b)
        {
            return a.Value >= b.Value;
        }

        public static bool operator <=(Degrees a, Degrees b)
        {
            return a.Value <= b.Value;
        }

        public static bool operator >(Degrees a, Degrees b)
        {
            return a.Value > b.Value;
        }

        public static bool operator <(Degrees a, Degrees b)
        {
            return a.Value < b.Value;
        }

        public override bool Equals(object? obj)
        {
            return obj is Degrees other && Equals(other);
        }

        public bool Equals(Degrees other)
        {
            return Value == other.Value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString()
        {
            return $"{Value}°";
        }
    }
}
