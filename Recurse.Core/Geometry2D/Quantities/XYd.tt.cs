﻿using System;
using Recurse.Core.Randomness;

namespace Recurse.Core.Maths.Space.Points
{
    [Serializable]
    public partial struct XYd : IEquatable<XYd>
    {
        public readonly double X;
        
        public readonly double Y;

        public XYd(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double GetDistance(XYd other)
        {
            return Math.Sqrt(Math.Pow(X - other.X, 2) + Math.Pow(Y - other.Y, 2));
        }

        public double GetOrthogonalDistance(XYd other)
        {
            return Math.Abs(X - other.X) + Math.Abs(Y - other.Y);
        }

        public static XYd operator+(XYd a, XYd b)
        {
            return new XYd(a.X + b.X, a.Y + b.Y);
        }

        public static XYd operator -(XYd a, XYd b)
        {
            return new XYd(a.X - b.X, a.Y - b.Y);
        }

        public static XYd operator *(XYd a, XYd b)
        {
            return new XYd(a.X * b.X, a.Y * b.Y);
        }

        public static XYd operator *(XYd point, double factor)
        {
            return new XYd(point.X * factor, point.Y * factor);
        }

        public static XYd operator -(XYd point)
        {
            return new XYd(-point.X, -point.Y);
        }

        public static bool operator ==(XYd a, XYd b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(XYd a, XYd b)
        {
            return a.X != b.X || a.Y != b.Y;
        }

        public bool Equals(XYd other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object? obj)
        {
            return obj is XYd other && Equals(other);
        }

        public override int GetHashCode()
        {
            return RandomElement.FromCallingFile(X, Y).ToInt();
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }
    }
}
