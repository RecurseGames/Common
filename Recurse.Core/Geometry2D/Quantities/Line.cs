﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Numerics;

namespace Recurse.Core.Collections.Planes
{
    public struct Line
    {
        private readonly float _offset;

        public float Slope { get; }

        public Line(float x1, float y1, float x2, float y2)
        {
            if (x1 == x2)
            {
                Slope = float.PositiveInfinity;
                _offset = x1;
            }
            else
            {
                Slope = (y2 - y1) / (x2 - x1);
                _offset = y1 - Slope * x1;
            }
        }

        public Line(Vector2 point1, Vector2 point2) : this(point1.X, point1.Y, point2.X, point2.Y)
        {

        }

        public float GetY(float x)
        {
            if (float.IsInfinity(Slope))
            {
                return float.NaN;
            }
            else
            {
                return Slope * x + _offset;
            }
        }

        public float GetX(float y)
        {
            if (float.IsInfinity(Slope))
            {
                return _offset;
            }
            else if (Slope == 0)
            {
                return float.NaN;
            }
            else
            {
                return (y - _offset) / Slope;
            }
        }

        public override string ToString()
        {
            if (float.IsNaN(Slope))
            {
                return $"x = y = NaN";
            }
            else if (Slope == 0)
            {
                return $"y = {_offset}";
            }
            else if (float.IsInfinity(Slope))
            {
                return $"x = {_offset}";
            }
            else
            {
                return $"{Slope:0.##}x + {_offset:0.##} = y";
            }
        }
    }
}