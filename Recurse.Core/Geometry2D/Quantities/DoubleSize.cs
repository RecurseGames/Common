﻿using System;
using Recurse.Core.Maths.Space.Points;
using Recurse.Core.Randomness;

namespace Recurse.Core.Maths.Space.Podoubles
{
#warning Rename XYd to DoublePoint, get rid of stupid T4 template (actually they're fine, can share between assemblies using relative paths)
    [Serializable]
    public struct DoubleSize : IEquatable<DoubleSize>
    {
        public readonly double X;

        public readonly double Y;

        public DoubleSize(double size)
        {
            X = size;
            Y = size;
        }

        public DoubleSize(double x, double y)
        {
            X = x;
            Y = y;
        }

        public IntVector Round()
        {
            return new IntVector((int)Math.Round(X), (int)Math.Round(Y));
        }

        public static DoubleSize operator +(DoubleSize a, DoubleSize b)
        {
            return new DoubleSize(a.X + b.X, a.Y + b.Y);
        }

        public static DoubleSize operator -(DoubleSize a, DoubleSize b)
        {
            return new DoubleSize(a.X - b.X, a.Y - b.Y);
        }

        public static DoubleSize operator *(DoubleSize a, DoubleSize b)
        {
            return new DoubleSize(a.X * b.X, a.Y * b.Y);
        }

        public static DoubleSize operator *(DoubleSize area, double factor)
        {
            return new DoubleSize(area.X * factor, area.Y * factor);
        }

        public static DoubleSize operator -(DoubleSize area)
        {
            return new DoubleSize(-area.X, -area.Y);
        }

        public static bool operator ==(DoubleSize a, DoubleSize b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(DoubleSize a, DoubleSize b)
        {
            return a.X != b.X || a.Y != b.Y;
        }

        public bool Equals(DoubleSize other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object? obj)
        {
            return obj is DoubleSize other && Equals(other);
        }

        public override int GetHashCode()
        {
            return RandomElement.FromCallingFile(X, Y).ToInt();
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }
    }
}
