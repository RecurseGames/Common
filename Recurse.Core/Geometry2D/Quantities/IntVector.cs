﻿using System;
using System.Collections.Generic;

namespace Recurse.Core.Maths.Space.Points
{
    public partial struct IntVector : IEquatable<IntVector>
    {
        public IEnumerable<XY> Points
        {
            get
            {
                for (var x = 0; x < X; x++)
                {
                    for (var y = 0; y < Y; y++)
                    {
                        yield return new XY(x, y);
                    }
                }
            }
        }

        public IntVector(int size)
        {
            X = size;
            Y = size;
        }

        public bool ContainsX(int x)
        {
            return x >= 0 && x < X;
        }

        public bool ContainsY(int y)
        {
            return y >= 0 && y < Y;
        }

        public bool Contains(XY point)
        {
            return point.X >= 0 && point.Y >= 0 && point.X < X && point.Y < Y;
        }
    }
}
