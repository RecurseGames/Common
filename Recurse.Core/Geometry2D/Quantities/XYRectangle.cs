﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using Recurse.Core.Maths.Space.Points;

namespace Recurse.Core.Collections.Planes
{
    /// <summary>
    /// A rectangle containing one or more points
    /// </summary>
    [Serializable]
    public struct XYRectangle : IRectangleArea, IEquatable<XYRectangle>
    {
        public XY Min { get; }

        public int Width { get; }

        public int Height { get; }

        public XY Max => new(Min.X + Width - 1, Min.Y + Height - 1);

        public XY ExclusiveMax => new(Min.X + Width, Min.Y + Height);

        public IEnumerable<XY> Points
        {
            get
            {
                for (var x = 0; x < Width; x++)
                {
                    for (var y = 0; y < Height; y++)
                    {
                        yield return new(Min.X + x, Min.Y + y);
                    }
                }
            }
        }

        public XYRectangle(int width, int height)
        {
            if (width <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(width), "Width must be greater than 0.");
            }
            else if (height <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(height), "Height must be greater than 0.");
            }

            Min = default;
            Width = width;
            Height = height;
        }

        public XYRectangle(XY min, int width, int height)
        {
            if (width <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(width), "Width must be greater than 0.");
            }
            else if (height <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(height), "Height must be greater than 0.");
            }

            Min = min;
            Width = width;
            Height = height;
        }

        public XYRectangle(int x, int y, int width, int height)
        {
            if (width <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(width), "Width must be greater than 0.");
            }
            else if (height <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(height), "Height must be greater than 0.");
            }

            Min = new(x, y);
            Width = width;
            Height = height;
        }

        public static XYRectangle FromRadius(int x, int y, int radius)
        {
            if (radius < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(radius), "Radius must not be less than 0.");
            }

            return new XYRectangle(x - radius, y - radius, 2 * radius + 1, 2 * radius + 1);
        }

        public XYRectangle? GetIntersect(XYRectangle? other)
        {
            if (!other.HasValue)
            {
                return default;
            }
            else
            {
                var minX = Math.Max(Min.X, other.Value.Min.X);
                var minY = Math.Max(Min.Y, other.Value.Min.Y);
                var maxX = Math.Min(Max.X, other.Value.Max.X);
                var maxY = Math.Min(Max.Y, other.Value.Max.Y);
                if (minX <= maxX && minY <= maxY)
                {
                    return new XYRectangle(minX, minY, maxX - minX + 1, maxY - minY + 1);
                }
                else
                {
                    return null;
                }
            }
        }
        
        public XYRectangle? GetIntersect(IRectangleArea? other)
        {
            if (other == null)
            {
                return default;
            }
            else
            {
                var minX = Math.Max(Min.X, 0);
                var minY = Math.Max(Min.Y, 0);
                var maxX = Math.Min(Max.X, other.Width);
                var maxY = Math.Min(Max.Y, other.Height);
                if (minX <= maxX && minY <= maxY)
                {
                    return new XYRectangle(minX, minY, maxX - minX + 1, maxY - minY + 1);
                }
                else
                {
                    return null;
                }
            }
        }

        public bool ContainsX(int x)
        {
            return x >= Min.X && x <= Max.X;
        }

        public bool ContainsY(int y)
        {
            return y >= Min.Y && y <= Max.Y;
        }

        public bool Contains(int x, int y)
        {
            return x >= Min.X && x <= Max.X && y >= Min.Y && y <= Max.Y;
        }

        public bool Contains(XY point)
        {
            return point.X >= Min.X && point.X <= Max.X && point.Y >= Min.Y && point.Y <= Max.Y;
        }

        public XYRectangle GetOffset(int x, int y)
        {
            return new XYRectangle(Min + new XY(x, y), Width, Height);
        }

        public XYRectangle GetOffset(XY offset)
        {
            return new XYRectangle(Min + offset, Width, Height);
        }

        public IEnumerable<XY> GetPoints()
        {
            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    yield return new(Min.X + x, Min.Y + y);
                }
            }
        }

        public bool Equals(XYRectangle other)
        {
            return EqualityComparer<XYRectangle>.Default.Equals(this, other);
        }

        public override string ToString()
        {
            return $"{Min} - {Max}";
        }
    }
}