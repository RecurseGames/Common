﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using Recurse.Core.Maths.Space.Points;

namespace Recurse.Core.Collections.Planes
{
    [Serializable]
    public struct Ellipse : IPointArea<XY>, IIntArea
    {
        public float MinX { get; }

        public float MinY { get; }

        public float Width { get; }

        public float Height { get; }

        public IEnumerable<XY> Points => GetPoints(new XYRectangle(
            (int)Math.Floor(MinX), 
            (int)Math.Floor(MinY),
            (int)Math.Ceiling(Width),
            (int)Math.Ceiling(Height)));

        public Ellipse(float width, float height)
        {
            MinX = 0;
            MinY = 0;
            Width = width;
            Height = height;
        }

        public Ellipse(float minX, float minY, float width, float height)
        {
            MinX = minX;
            MinY = minY;
            Width = width;
            Height = height;
        }

        public Ellipse GetOffset(float x, float y)
        {
            return new Ellipse(MinX + x, MinY + y, Width, Height);
        }

        public IEnumerable<XY> GetPoints(XYRectangle bounds)
        {
            var minY = Math.Max(bounds.Min.Y, (int)MinY);
            var maxY = minY + Height;
            for (var y = minY; y <= maxY; y++)
            {
                var horizontalChordLength = GetHorizontalChordLength(y);
                var minX = Math.Max(
                    (int)Math.Round((Width - horizontalChordLength) / 2 + MinX),
                    bounds.Min.X);
                var maxX = Math.Min(
                    (int)Math.Round(minX + horizontalChordLength),
                    bounds.Max.X);
                for (var x = minX; x <= maxX; x++)
                {
                    yield return new XY(x, y);
                }
            }
        }

        public bool Contains(int x, int y)
        {
            var horizontalChordLength = GetHorizontalChordLength(y);

            var minX = (Width - horizontalChordLength) / 2;

            return x >= minX && x <= minX + horizontalChordLength;
        }

        public bool Contains(XY point)
        {
            return Contains(point.X, point.Y);
        }

        private double GetHorizontalChordLength(int y)
        {
            var normalY = (y - MinY) / Height;

            return Math.Sqrt(4 * normalY * (1 - normalY)) * Width;
        }

        public override string ToString()
        {
            return $"({MinX}, {MinY}, {Width}, {Height})";
        }
    }
}