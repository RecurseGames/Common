﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Numerics;
using Recurse.Core.Maths.Space.Points;

namespace Recurse.Core.Collections.Planes
{
    [Serializable]
    public struct Triangle : IIntArea
    {
        private readonly Vector2 _a, _b, _c;

        public Triangle(IEnumerable<Vector2> vertices)
        {
            var i = 0;
            var vertexArray = new Vector2[3];
            foreach (var vertex in vertices)
            {
                if (i == 3)
                {
                    throw new ArgumentException("Cannot create triangle with more than three points.", nameof(vertexArray));
                }
                vertexArray[i++] = vertex;
            }

            if (i != 3)
            {
                throw new ArgumentException("Cannot create triangle with less than three points.", nameof(vertexArray));
            }

            Array.Sort(vertexArray, (x, y) => x.X.CompareTo(y.X));
            _a = vertexArray[0];
            _b = vertexArray[1];
            _c = vertexArray[2];
        }

        public Triangle(Vector2 vertex1, Vector2 vertex2, Vector2 vertex3)
        {
            var vertexArray = new[] { vertex1, vertex2, vertex3 };
            Array.Sort(vertexArray, (x, y) => x.X.CompareTo(y.X));
            _a = vertexArray[0];
            _b = vertexArray[1];
            _c = vertexArray[2];
        }

        public Triangle(float x1, float y1, float x2, float y2, float x3, float y3) : this(
            new Vector2(x1, y1),
            new Vector2(x2, y2),
            new Vector2(x3, y3))
        {

        }

        public IEnumerable<XY> GetPoints()
        {
            return GetPoints(default, false);
        }

        public IEnumerable<XY> GetPoints(XYRectangle bounds)
        {
            return GetPoints(bounds, true);
        }

        public bool Contains(XY point)
        {
            if (point.X < _a.X || point.X >= _c.X)
            {
                return false;
            }

            var bound1 = new Line(_a, _c);
            var bound2 = point.X < _b.X ? new Line(_a, _b) : new Line(_b, _c);
            var yBound1 = bound1.GetY(point.X);
            var yBound2 = bound2.GetY(point.X);
            var minY = MathF.Min(yBound1, yBound2);
            var maxY = MathF.Max(yBound1, yBound2);
            return point.Y >= minY && point.Y < maxY;
        }

        private IEnumerable<XY> GetPoints(XYRectangle bounds, bool useBounds)
        {
            var xMin = (int)MathF.Ceiling(useBounds ? MathF.Max(bounds.Min.X, _a.X) : _a.X);
            var xMax = (int)MathF.Floor(useBounds ? MathF.Min(bounds.Max.X, _c.X) : _c.X);
            var xMiddle = (int)MathF.Ceiling(_b.X);
            var yBound1 = new Line(_a, _c);
            var yBound2 = xMiddle <= xMin ? new Line(_b, _c) : new Line(_a, _b);
            for (var x = xMin; x <= xMax; x++)
            {
                if (x == xMiddle)
                {
                    yBound2 = new Line(_b, _c);
                }

                var yLimit1 = yBound1.GetY(x);
                var yLimit2 = yBound2.GetY(x);
                var yMin = (int)MathF.Ceiling(MathF.Min(yLimit1, yLimit2));
                var yMax = (int)MathF.Floor(MathF.Max(yLimit1, yLimit2));
                var yMinBounded = useBounds ? Math.Max(bounds.Min.Y, yMin) : yMin;
                var yMaxBounded = useBounds ? Math.Min(bounds.Max.Y, yMax) : yMax;

                for (var y = yMinBounded; y <= yMaxBounded; y++)
                {
                    yield return new XY(x, y);
                }
            }
        }

        public override string ToString()
        {
            return $"{_a}, {_b}, {_c}";
        }
    }
}