﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Core.Collections.Planes;
using Recurse.Games.Explorer.Systems.WaveFunctions3.Solvers;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace Recurse.Core.Maths.Space.Points
{
    [Serializable]
    public readonly struct XY : IEquatable<XY>, IPoint<XY>
    {
        public static ImmutableArray<XY> Offsets => ImmutableArray.Create<XY>(
            new(0, -1), new(1, -1), new(1, 0), new(1, 1), new(0, 1), new(-1, 1), new(-1, 0), new(-1, -1));

        public static ImmutableArray<XY> HalfOffsets => ImmutableArray.Create<XY>(
            new(0, -1), new(1, -1), new(1, 0), new(1, 1));

        public static ImmutableArray<XY> OrthogonalOffsets => ImmutableArray.Create<XY>(
            new(0, -1), new(1, 0), new(0, 1), new(-1, 0));

        public static INeighbourhood<XY> OrthogonalAdjacency => new OrthogonalNeighbourhood();

        public static INeighbourhood<XY> Adjacency => new Neighbourhood();

        public readonly int X;

        public readonly int Y;

        public XY(int x, int y)
        {
            X = x;
            Y = y;
        }

        public XYd ToDouble()
        {
            return this;
        }

        public IReadOnlyList<XY> GetNeighbours()
        {
            return Adjacency.GetNeighbours(this);
        }

        public IReadOnlyList<XY> GetOrthogonalNeighbours()
        {
            return OrthogonalAdjacency.GetNeighbours(this);
        }

        public static implicit operator XYd(XY point)
        {
            return new XYd(point.X, point.Y);
        }

        public double GetDistance(XY other)
        {
            return Math.Sqrt(Math.Pow(X - other.X, 2) + Math.Pow(Y - other.Y, 2));
        }

        public double GetOrthogonalDistance(XY other)
        {
            return Math.Abs(X - other.X) + Math.Abs(Y - other.Y);
        }

        public XY Add(XY offset)
        {
            return this + offset;
        }

        public static XY operator +(XY a, XY b)
        {
            return new XY(a.X + b.X, a.Y + b.Y);
        }

        public static XY operator -(XY a, XY b)
        {
            return new XY(a.X - b.X, a.Y - b.Y);
        }

        public static XY operator *(XY a, XY b)
        {
            return new XY(a.X * b.X, a.Y * b.Y);
        }

        public static XY operator *(XY point, int factor)
        {
            return new XY(point.X * factor, point.Y * factor);
        }

        public static XY operator -(XY point)
        {
            return new XY(-point.X, -point.Y);
        }

        public static bool operator ==(XY a, XY b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(XY a, XY b)
        {
            return a.X != b.X || a.Y != b.Y;
        }

        public bool Equals(XY other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object? obj)
        {
            return obj is XY other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }

        [Serializable]
        private class OrthogonalNeighbourhood : INeighbourhood<XY>
        {
            public IReadOnlyList<XY> GetNeighbours(XY point)
            {
                return new[] 
                {
                    point + new XY(1, 0),
                    point + new XY(0, 1),
                    point + new XY(-1, 0),
                    point + new XY(0, -1),
                };
            }
        }

        [Serializable]
        private class Neighbourhood : INeighbourhood<XY>
        {
            public IReadOnlyList<XY> GetNeighbours(XY point)
            {
                return new[]
                {
                    point + new XY(1, 0),
                    point + new XY(1, 1),
                    point + new XY(0, 1),
                    point + new XY(-1, 1),
                    point + new XY(-1, 0),
                    point + new XY(-1, -1),
                    point + new XY(0, -1),
                    point + new XY(1, -1) 
                };
            }
        }
    }
}
