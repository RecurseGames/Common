﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Recurse.Core.Maths.Space.Points
{
    public partial struct XYd
    {
        public XY Round()
        {
            return new XY((int)Math.Round(X), (int)Math.Round(Y));
        }

        public XY Floor()
        {
            return new XY((int)Math.Floor(X), (int)Math.Floor(Y));
        }

        public XY Ceiling()
        {
            return new XY((int)Math.Ceiling(X), (int)Math.Ceiling(Y));
        }

        public static XYd operator /(XYd point, int divisor)
        {
            return new XYd(point.X / divisor, point.Y / divisor);
        }

        public static XYd operator /(XYd a, XYd b)
        {
            return new XYd(a.X / b.X, a.Y / b.Y);
        }
    }
}
