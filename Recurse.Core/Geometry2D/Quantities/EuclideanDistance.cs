﻿using System;
using Recurse.Core.Collections.Planes;
using Recurse.Core.Geometry2D;
using Recurse.Core.Maths.Space.Points;

namespace Recurse.Core.Maths.Space.Podoubles
{
    [Serializable]
    public class EuclideanDistance : IDistanceMetric<XYd>
    {
        public double GetDistance(XYd point1, XYd point2)
        {
            return point1.GetDistance(point2);
        }

        public XYd GetRandomPoint(XYd origin, Random rng, double distance)
        {
            var angle = new Degrees(rng.NextDouble() * 360);

            return ScreenCoordinates.Get(angle) * distance + origin;
        }
    }
}
