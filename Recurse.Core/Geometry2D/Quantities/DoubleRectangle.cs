﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Recurse.Core.Maths.Space.Podoubles;
using Recurse.Core.Maths.Space.Points;

namespace Recurse.Core.Collections.Planes
{
    [Serializable]
    public struct DoubleRectangle
    {
        private readonly double _exclusiveWidth;

        private readonly double _height;

        private readonly XYd _positionIfAny;

        public bool HasPosition => _exclusiveWidth != 0;

        public XYd Position => HasPosition ? _positionIfAny : throw new InvalidOperationException("Area has no position");

        public DoubleSize Size => new DoubleSize(_exclusiveWidth == 0 ? 0 : Math.BitDecrement(_exclusiveWidth), _height);

        public IEnumerable<XY> Points
        {
            get
            {
                if (HasPosition)
                {
                    var minimum = Position.Ceiling();
                    var maximum = Position + new XYd(Size.X, Size.Y);
                    for (var x = minimum.X; x <= maximum.X; x++)
                    {
                        for (var y = minimum.Y; y <= maximum.Y; y++)
                        {
                            yield return new XY(x, y);
                        }
                    }
                }
            }
        }

        public DoubleRectangle(XYd position, double width, double height)
        {
            if (width < 0 || double.IsNaN(width))
            {
                throw new ArgumentOutOfRangeException(nameof(width), "Size must be non-negative.");
            }
            else if (height < 0 || double.IsNaN(height))
            {
                throw new ArgumentOutOfRangeException(nameof(height), "Size must be non-negative.");
            }

            _positionIfAny = position;
            _exclusiveWidth = Math.BitIncrement(width);
            _height = height;
        }

        public static DoubleRectangle CreateBounds(IEnumerable<XY> points)
        {
            return CreateBounds(points.Select(point => point.ToDouble()));
        }

        public static DoubleRectangle CreateBounds(IEnumerable<XYd> points)
        {
            var pointArray = points.ToArray();
            if (pointArray.Length == 0)
            {
                return default;
            }

            var minX = pointArray[0].X;
            var minY = pointArray[0].Y;
            var maxX = pointArray[0].X;
            var maxY = pointArray[0].Y;
            foreach (var point in pointArray.Skip(1))
            {
                minX = Math.Min(point.X, minX);
                maxX = Math.Max(point.X, maxX);
                minY = Math.Min(point.Y, minY);
                maxY = Math.Max(point.Y, maxY);
            }

            return new DoubleRectangle(new XYd(minX, minY), maxX - minX, maxY - minY);
        }

        public bool Contains(XYd point)
        {
            if (!HasPosition)
            {
                return false;
            }

            var relativePoint = point - Position;

            return relativePoint.X >= 0 && relativePoint.Y >= 0 && relativePoint.X <= Size.X && relativePoint.Y <= Size.Y;
        }

        public bool Contains(XY point)
        {
            if (!HasPosition)
            {
                return false;
            }

            var relativePoint = point - Position;

            return relativePoint.X >= 0 && relativePoint.Y >= 0 && relativePoint.X <= Size.X && relativePoint.Y <= Size.Y;
        }
    }
}