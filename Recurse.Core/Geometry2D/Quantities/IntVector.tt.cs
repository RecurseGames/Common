﻿using System;
using Recurse.Core.Randomness;

namespace Recurse.Core.Maths.Space.Points
{
    [Serializable]
    public partial struct IntVector : IEquatable<IntVector>
    {
        public readonly int X;
        
        public readonly int Y;

        public IntVector(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static IntVector operator+(IntVector a, IntVector b)
        {
            return new IntVector(a.X + b.X, a.Y + b.Y);
        }

        public static IntVector operator -(IntVector a, IntVector b)
        {
            return new IntVector(a.X - b.X, a.Y - b.Y);
        }

        public static IntVector operator *(IntVector a, IntVector b)
        {
            return new IntVector(a.X * b.X, a.Y * b.Y);
        }

        public static IntVector operator *(IntVector point, int factor)
        {
            return new IntVector(point.X * factor, point.Y * factor);
        }

        public static IntVector operator -(IntVector point)
        {
            return new IntVector(-point.X, -point.Y);
        }

        public static bool operator ==(IntVector a, IntVector b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(IntVector a, IntVector b)
        {
            return a.X != b.X || a.Y != b.Y;
        }

        public bool Equals(IntVector other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object? obj)
        {
            return obj is IntVector other && Equals(other);
        }

        public override int GetHashCode()
        {
            return RandomElement.FromCallingFile(X, Y).ToInt();
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }
    }
}
