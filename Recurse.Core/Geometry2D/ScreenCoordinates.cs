﻿using Recurse.Core.Maths.Space.Points;
using System;

namespace Recurse.Core.Geometry2D
{
    public static class ScreenCoordinates
    {
        public static readonly XY
            N = new(0, -1),
            NE = new(1, -1),
            E = new(1, 0),
            SE = new(1, 1),
            S = new(0, 1),
            SW = new(-1, 1),
            W = new(-1, 0),
            NW = new(-1, -1);

        public static XYd Get(Radians radians)
        {
            var x = Math.Cos(radians.Value);
            var y = -Math.Sin(radians.Value);

            return new(x, y);
        }

        public static Radians GetAngle(XYd point)
        {
            return new Radians(Math.Atan2(-point.Y, point.X));
        }
    }
}
