﻿using Recurse.Core.Maths;
using System;

namespace Recurse.Core.Geometry2D
{
    public readonly struct Radians : IEquatable<Radians>
    {
        private const double DegreesPerRadian = 180 / Math.PI;

        public const double MaximumValue = 2 * Math.PI;

        public const double MaximumDistanceValue = Math.PI;

        public static readonly Radians HalfTurn = new(Math.PI);

        public double Value { get; }

        public double Degrees => Value * DegreesPerRadian;

        public Radians(double value)
        {
            if (!double.IsFinite(value))
            {
                throw new ArgumentException("Radians must be finite.", nameof(value));
            }

            Value = Modulus.Get(value, MaximumValue);
        }

        public Radians GetDistance(Radians other)
        {
            var difference = Math.Abs(Value - other.Value);

            return new(Math.Min(difference, MaximumValue - difference));
        }

        public Degrees GetDifference(Degrees other)
        {
            return new(Value - other.Value);
        }

        public static implicit operator Degrees(Radians radians)
        {
            return new(radians.Value * DegreesPerRadian);
        }

        public static bool operator ==(Radians a, Radians b)
        {
            return a.Value == b.Value;
        }

        public static bool operator !=(Radians a, Radians b)
        {
            return a.Value == b.Value;
        }

        public static bool operator >=(Radians a, Radians b)
        {
            return a.Value >= b.Value;
        }

        public static bool operator <=(Radians a, Radians b)
        {
            return a.Value <= b.Value;
        }

        public static bool operator >(Radians a, Radians b)
        {
            return a.Value > b.Value;
        }

        public static bool operator <(Radians a, Radians b)
        {
            return a.Value < b.Value;
        }

        public override bool Equals(object? obj)
        {
            return obj is Radians other && Equals(other);
        }

        public bool Equals(Radians other)
        {
            return Value == other.Value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString()
        {
            return $"{Value}rad";
        }
    }
}
