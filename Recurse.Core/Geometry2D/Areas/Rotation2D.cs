﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Core.Maths.Space.Points;

namespace Recurse.Core.Collections.Planes
{
    public readonly struct Rotation2D
    {
        public static Rotation2D Default => default;

        public static Rotation2D Left => Default.LeftRotation;

        public static Rotation2D Right => Default.RightRotation;

        public static Rotation2D Full => Default.FullRotation;

        public static Rotation2D Horizontal => Default.HorizontalMirror;

        public static Rotation2D Vertical => Default.VerticalMirror;

        private readonly bool _flipHorizontal, _flipVertical, _flipDiagonal;

        public Rotation2D HorizontalMirror => new(!_flipHorizontal, _flipVertical, _flipDiagonal);

        public Rotation2D VerticalMirror => new(_flipHorizontal, !_flipVertical, _flipDiagonal);

        public Rotation2D LeftRotation => new(!_flipHorizontal, _flipVertical, !_flipDiagonal);

        public Rotation2D RightRotation => new(_flipHorizontal, !_flipVertical, !_flipDiagonal);

        public Rotation2D FullRotation => new(!_flipHorizontal, !_flipVertical, _flipDiagonal);

        public Rotation2D(bool mirrorHorizontal, bool mirrorVertical)
        {
            _flipHorizontal = mirrorHorizontal;
            _flipVertical = mirrorVertical;
            _flipDiagonal = false;
        }

        private Rotation2D(bool flipHorizontal, bool flipVertical, bool flipDiagonal)
        {
            _flipHorizontal = flipHorizontal;
            _flipVertical = flipVertical;
            _flipDiagonal = flipDiagonal;
        }

        public Array2D<T> CreateArray<T>(IRectangleMap<T> source)
        {
            var result = _flipDiagonal ? new Array2D<T>(source.Height, source.Width) : new Array2D<T>(source.Width, source.Height);
            foreach (var point in source.Points)
            {
                result[Translate(point, 0, 0, source.Width, source.Height)] = source[point];
            }
            return result;
        }

        public Array2D<T> CreateArray<T>(IPointMap<XY, T> source, int sourceX, int sourceY, int sourceWidth, int sourceHeight)
        {
            var result = _flipDiagonal ? new Array2D<T>(sourceHeight, sourceWidth) : new Array2D<T>(sourceWidth, sourceHeight);
            foreach (var point in result.Points)
            {
                if (result.Contains(point))
                {
                    result[point] = source[Translate(point, sourceX, sourceY, sourceWidth, sourceHeight)];
                }
            }
            return result;
        }

        private XY Translate(XY point, int sourceX, int sourceY, int sourceWidth, int sourceHeight)
        {
            if (_flipDiagonal)
            {
                point = new(point.Y, point.X);
            }

            var x = _flipHorizontal ? sourceX + sourceWidth - point.X - 1 : sourceX + point.X;
            var y = _flipVertical ? sourceY + sourceHeight - point.Y - 1 : sourceY + point.Y;
            return new(x, y);
        }
    }
}