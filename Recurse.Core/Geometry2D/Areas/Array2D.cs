﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Recurse.Core.Maths.Space.Points;

namespace Recurse.Core.Collections.Planes
{
    public static class Array2D
    {
        public static Array2D<T> Create<T>(T[,] elements)
        {
            return Array2D<T>.Create(elements);
        }

        public static Array2D<T> Create<T>(int width, int height) where T : new()
        {
            var result = new Array2D<T>(width, height);
            for (var i = 0; i < result.Elements.Length; i++)
            {
                result.Elements[i] = new T();
            }
            return result;
        }

        public static Array2D<T> Create<T>(int width, int height, Func<XY, T> createElement)
        {
            var result = new Array2D<T>(width, height);
            foreach (var point in result.Points)
            {
                result[point] = createElement(point);
            }
            return result;
        }
    }

    [Serializable]
    public class Array2D<T> : IRectangleMap<T>, IReadOnlyPlane<T>, IPointMap<XY, T>
    {
        public static readonly Array2D<T> Empty = new();

        public int Width { get; }

        public int Height { get; }

        public T[] Elements { get; }

        public IReadOnlyCollection<T> Values => Elements;

        public IEnumerable<XY> Points
        {
            get
            {
                for (var x = 0; x < Width; x++)
                {
                    for (var y = 0; y < Height; y++)
                    {
                        yield return new(x, y);
                    }
                }
            }
        }

        public T this[int x, int y]
        {
            get
            {
                if (!ContainsX(x)) 
                    throw new ArgumentOutOfRangeException(nameof(x));
                if (!ContainsY(y)) 
                    throw new ArgumentOutOfRangeException(nameof(y));
                return Elements[x + y * Width];
            }
            set
            {
                if (!ContainsX(x))
                    throw new ArgumentOutOfRangeException(nameof(x));
                if (!ContainsY(y))
                    throw new ArgumentOutOfRangeException(nameof(y));
                Elements[x + y * Width] = value;
            }
        }

        public T this[XY point]
        {
            get
            {
                if (!Contains(point))
                {
                    throw new ArgumentOutOfRangeException(nameof(point));
                }
                else
                {
                    return Elements[point.X + point.Y * Width];
                }
            }
            set
            {
                if (!Contains(point))
                {
                    throw new ArgumentOutOfRangeException(nameof(point));
                }
                else
                {
                    Elements[point.X + point.Y * Width] = value;
                }
            }
        }

        private Array2D()
        {
            Elements = Array.Empty<T>();
            Width = 0;
            Height = 0;
        }

        public Array2D(IRectangleMap<T> area) : this(area.Width, area.Height)
        {
            foreach (var point in area.Points)
            {
                this[point] = area[point];
            }
        }

        public Array2D(int width, int height)
        {
            if (width < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(width), "Width cannot be negative.");
            }
            else if (height < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(height), "Height cannot be negative.");
            }
            else if (width == 0 || height == 0)
            {
                // This fails-fast for nonsensical code like new Array2D(0, 10);
                throw new InvalidOperationException("Cannot specify zero width or height, use static instance instead.");
            }

            Elements = new T[width * height];
            Width = width;
            Height = height;
        }

        public Array2D(XYRectangle bounds)
        {
            Elements = new T[bounds.Width * bounds.Height];
        }

        public Array2D(IntVector area)
        {
            if (area.X < 0 || area.Y < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(area), "Width and height must be greater than 0.");
            }

            Elements = new T[area.X * area.Y];
        }

        private Array2D(T[] elements, int width, int height)
        {
            Elements = elements;
            Width = width;
            Height = height;
        }

        public static Array2D<T> Create(T[,] elements)
        {
            if (elements.GetLength(0) == 0 || elements.GetLength(1) == 0)
            {
                throw new ArgumentException("Elements cannot be empty.", nameof(elements));
            }

            var result = new Array2D<T>(elements.GetLength(1), elements.GetLength(0));

            for (var x = 0; x < result.Width; x++)
            {
                for (var y = 0; y < result.Height; y++)
                {
                    result[new(x, y)] = elements[y, x];
                }
            }

            return result;
        }

        public static Array2D<T> FromBounds(IRectangleArea other)
        {
            return new Array2D<T>(other.Width, other.Height);
        }

        public bool ContainsX(int x)
        {
            return x >= 0 && x < Width;
        }

        public bool ContainsY(int y)
        {
            return y >= 0 && y < Height;
        }

        public bool Contains(int x, int y)
        {
            return x >= 0 && y >= 0 && x < Width && y < Height;
        }

        public bool Contains(XY point)
        {
            return point.X >= 0 && point.Y >= 0 && point.X < Width && point.Y < Height;
        }

        public bool TryGet(XY point, [MaybeNullWhen(false)] out T value)
        {
            if (Contains(point))
            {
                value = Elements[GetElementIndex(point)];
                return true;
            }
            else
            {
                value = default;
                return false;
            }
        }

        public int GetElementIndex(int x, int y)
        {
            if (x < 0 || x >= Width)
            {
                throw new ArgumentOutOfRangeException(nameof(x));
            }
            else if (y < 0 || y >= Height)
            {
                throw new ArgumentOutOfRangeException(nameof(y));
            }
            else
            {
                return x + y * Width;
            }
        }

        public int GetElementIndex(XY point)
        {
            if (!Contains(point))
            {
                throw new ArgumentOutOfRangeException(nameof(point));
            }
            else
            {
                return point.X + point.Y * Width;
            }
        }

        public Array2D<T> Clone()
        {
            var clonedElements = new T[Elements.Length];
            Array.Copy(Elements, clonedElements, Elements.Length);
            return new Array2D<T>(Elements, Width, Height);
        }

        public void Fill(Func<T> createElement)
        {
            for (var i = 0; i < Elements.Length; i++)
            {
                Elements[i] = createElement();
            }
        }

        public void Fill(Func<XY, T> createElement)
        {
            foreach (var point in Points)
            {
                this[point] = createElement(point);
            }
        }
    }
}