﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Core.Maths.Space.Points;
using System.Collections.Generic;

namespace Recurse.Core.Collections.Planes
{
    public class RectangleArea : IRectangleArea
    {
        public int Width { get; }

        public int Height { get; }

        public IEnumerable<XY> Points
        {
            get
            {
                for (var x = 0; x < Width; x++)
                {
                    for (var y = 0; y < Height; y++)
                    {
                        yield return new(x, y);
                    }
                }
            }
        }

        public RectangleArea(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public bool Contains(XY point)
        {
            return point.X >= 0 && point.Y >= 0 && point.X < Width && point.Y < Height;
        }
    }
}