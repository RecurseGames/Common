﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using Recurse.Core.Maths;
using Recurse.Core.Maths.Space.Points;

namespace Recurse.Core.Collections.Planes
{
    public static class WrappedPlane
    {
        public static WrappedPlane<T> Create<T>(IRectangleMap<T> source)
        {
            return new(source);
        }

        public static WrappedPlane<T> Create<T>(
            IRectangleMap<T> source,
            bool isWrappedHorizontally,
            bool isWrappedVertically)
        {
            return new(source)
            {
                IsWrappedHorizontally = isWrappedHorizontally,
                IsWrappedVertically = isWrappedVertically,
            };
        }
    }

    public class WrappedPlane<T>
    {
        public IRectangleMap<T> Source { get; set; }

        public bool IsWrappedHorizontally { get; set; }

        public bool IsWrappedVertically { get; set; }

        public T this[XY point] => Source[GetSourcePoint(point)];

        public WrappedPlane(IRectangleMap<T> source)
        {
            Source = source;
        }

        public bool TryGet(XY point, [MaybeNullWhen(false)] out T item)
        {
            var sourcePoint = GetSourcePoint(point);

            return Source.TryGet(sourcePoint, out item);
        }

        public bool Contains(XY point)
        {
            return
                (IsWrappedHorizontally || point.X >= 0 && point.X < Source.Width) &&
                (IsWrappedVertically || point.Y >= 0 && point.Y < Source.Height);
        }

        public double GetDistance(XY a, XY b)
        {
            return Math.Sqrt(
                Math.Pow(GetHorizontalDistance(a.X, b.X), 2) +
                Math.Pow(GetVerticalDistance(a.Y, b.Y), 2));
        }

        private int GetHorizontalDistance(int x1, int x2)
        {
            var absoluteDifference = Math.Abs(x1 - x2);

            return IsWrappedHorizontally
                ? Math.Min(absoluteDifference, Source.Width - absoluteDifference)
                : absoluteDifference;
        }

        private int GetVerticalDistance(int y1, int y2)
        {
            var absoluteDifference = Math.Abs(y1 - y2);

            return IsWrappedVertically
                ? Math.Min(absoluteDifference, Source.Width - absoluteDifference)
                : absoluteDifference;
        }

        private XY GetSourcePoint(XY point)
        {
            return new(
                IsWrappedHorizontally ? Modulus.Get(point.X, Source.Width) : point.X,
                IsWrappedVertically ? Modulus.Get(point.Y, Source.Height) : point.Y);
        }
    }
}